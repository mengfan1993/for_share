\---

title: Hello World

\---

Welcome to [Hexo](https://hexo.io/)! This is your very first post. Check [documentation](https://hexo.io/docs/) for more info. If you get any problems when using Hexo, you can find the answer in [troubleshooting](https://hexo.io/docs/troubleshooting.html) or you can ask me on [GitHub](https://github.com/hexojs/hexo/issues).

# hexo学习记录

20230929-时间标签

> 启动方式：
> hexo server

## 001-框架介绍

Hexo 是一个快速、简洁且高效的博客框架。Hexo 使用 Markdown（或其他标记语言）解析文章，在几秒内，即可利用靓丽的主题生成静态网页。

## 002-安装方式

```cmd
npm install hexo-cli -g
hexo init blog
cd blog
npm install
hexo server
```

## 003-框架的基本结构与打包后的文件介绍

- 目录结构的概念
- 生成静态问网站的命令：hexo generate
- 生成好的public文件夹就可以直接作为静态网站进行部署

## 004-Hexo博客主题的安装与推荐

https://hexo.io/themes/

找到自己喜欢的主题，使用git clone命令将主题文件夹下载到自己的`themes`目录之下，下面是自己感觉还可以的目录：

1. mustom：https://github.com/jinyaoMa/hexo-theme-mustom

- 使用指南：https://ma-jinyao.cn/post/cd0e94fa87bd88209ee0bc5be2d6b5eb/#hexo%E6%A0%B9%E7%9B%AE%E5%BD%95%E7%BB%93%E6%9E%84

- 配置参考: https://blog.ma-jinyao.cn/posts/49651/

1. Light: https://github.com/hexojs/hexo-theme-light
2. Next: https://github.com/next-theme/hexo-theme-next
3. Butterfly: https://github.com/jerryc127/hexo-theme-butterfly

- 使用教程：https://haojen.github.io/2017/05/09/Anisina-%E4%B8%AD%E6%96%87%E4%BD%BF%E7%94%A8%E6%95%99%E7%A8%8B/#undefined

## 005-如何自定义博客结构

主题：Light

大概说明了一下文件夹下的逻辑

## 006-如何开始写博客文章以及文章分类与标签



文章分类及标签：https://hexo.io/zh-cn/docs/front-matter

## 007-如何让代码在Hexo中更加的美观

https://hexo.io/zh-cn/docs/tag-plugins

## 008-本地写作时图床的最佳方案

使用七牛提供的对象储存服务，这个内容与整体的技术关系不大,感觉和图床的理念是类似。

![](images/苍穹轮廓pc (1).jpg)

## 009-插件的安装与必备插件介绍

字数统计插件

http://ibruce.info/2015/04/04/busuanzi/

如果你是用的hexo，打开**themes/你的主题/layout/_partial/footer.ejs**添加上述脚本即可

## 010-如何让你的博客中增加评论系统

disqus：针对于国外的博客页面

livere.com：国内的网站

changyan.kuaizhan.com：国内的网站

## 011-如何给你的博客增加搜索功能

swiftype.com: 目前好像是收费的。

site:bolg.parryqiu.com: 自定义site语法进行跳转即可。

## 012-多个博客统计系统的接入方案分析

统计的重要性

有盟统计：https://www.umeng.com/

百度统计：https://tongji.baidu.com/

## 013-部署博客到github的技巧

github page：https://pages.github.com/

域名：
CNAME的解析：https://www.iteye.com/blog/justcoding-1959736

## 014-Hexo其他资源的介绍与分享

中文文档：https://hexo.io/zh-cn/docs/

CSDN文档：https://blog.csdn.net/gdutxiaoxu/article/details/53576018

好看的Hexo主题：https://www.zhihu.com/question/24422335