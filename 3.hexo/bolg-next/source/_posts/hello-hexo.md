---
title: hello hexo
categories:
  - - 教程
tags:
  - 教程
mathjax: false
date: 2023-10-14 11:56:59
updated: 2023-10-14 11:56:59
---


Welcome to [Hexo](https://hexo.io/)! This is your very first post. Check [documentation](https://hexo.io/docs/) for more info. If you get any problems when using Hexo, you can find the answer in [troubleshooting](https://hexo.io/docs/troubleshooting.html) or you can ask me on [GitHub](https://github.com/hexojs/hexo/issues).

## Quick Start - 快速开始

### Create a new post - 创建一个新的博客

``` bash
$ hexo new "My New Post"
```

More info: [Writing](https://hexo.io/docs/writing.html)

### Run server - 开启服务

``` bash
$ hexo server
```

More info: [Server](https://hexo.io/docs/server.html)

### Generate static files - 生成静态文件

``` bash
$ hexo generate
```

More info: [Generating](https://hexo.io/docs/generating.html)

### Deploy to remote sites - 部署远程网页

``` bash
$ hexo deploy
```

More info: [Deployment](https://hexo.io/docs/one-command-deployment.html)



