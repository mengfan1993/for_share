# -*- coding: utf-8 -*-
# @project: for_share
# @Time    : 2023/10/30 11:39
# @Author  : Dreamstar
# @File    : 2.Dash.py
# @Desc    :


import plotly.graph_objects as go # or plotly.express as px
fig = go.Figure() # or any Plotly Express function e.g. px.bar(...)
# fig.add_trace( ... )
# fig.update_layout( ... )

from dash import Dash, dcc, html

app = Dash()
app.layout = html.Div([
    dcc.Graph(figure=fig)
])

app.run_server(debug=True, use_reloader=False)  # Turn off reloader if inside Jupyter