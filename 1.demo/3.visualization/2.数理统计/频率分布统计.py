# -*- coding: utf-8 -*-
# @project: for_share
# @Time    : 2023/11/21 11:05
# @Author  : Dreamstar
# @File    : 频率分布统计.py
# @Desc    :

import numpy as np
import pandas as pd

weights = np.array([ 48, 54, 47, 50, 53, 43, 45, 43, 44, 47,
                     58, 46, 46, 63, 49, 50, 48, 43, 46, 45,
                     50, 53, 51, 58, 52, 53, 47, 49, 45, 42,
                     51, 49, 58, 54, 45, 53, 50, 69, 44, 50,
                     58, 64, 40, 57, 51, 69, 58, 47, 62, 47,
                     40, 60, 48, 47, 53, 47, 52, 61, 55, 55,
                     48, 48, 46, 52, 45, 38, 62, 47, 55, 50,
                     46, 47, 55, 48, 50, 50, 54, 55, 48, 50])

sections = [35,40,45,50,55,60,65,70]
group_names = ['36~40','41~45','46~50', '51~55','56~60','61~65','66~70']
cuts = pd.cut(weights,sections, labels=group_names)

s_counts = pd.Series(cuts).value_counts()  # 前缀s代表了Series的数据类型
print(s_counts)
print(dict(s_counts))