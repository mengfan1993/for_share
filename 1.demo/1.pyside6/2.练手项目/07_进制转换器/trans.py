from PySide6.QtWidgets import QApplication, QWidget, QLineEdit
from Ui_trans import Ui_Form


class MyWindow(QWidget, Ui_Form):
    def __init__(self):
        super().__init__()
        self.setupUi(self)

        # 用于储存数据类型的字典
        self.lengthVar = {'米': 100, '千米': 100000, '厘米': 1, '分米': 10}
        self.weightVar = {'克': 1, '千克': 1000, '斤': 500}

        self.TypeDict = {'长度': self.lengthVar, '质量': self.weightVar}

        self.dataTypeCombox.addItems(self.TypeDict.keys())
        self.comboBox_input_1.addItems(self.lengthVar.keys())
        self.comboBox_input_2.addItems(self.lengthVar.keys())
        self.bind()

    def bind(self):
        self.dataTypeCombox.currentTextChanged.connect(self.typeChanged)
        self.PushButton_cal.clicked.connect(self.calc)

    def calc(self):
        bigType = self.dataTypeCombox.currentText()
        # 获取第一个输入框的值
        value = self.lineEdit_input_1.text()
        if value == '':
            return

        currentType = self.comboBox_input_1.currentText()
        transType = self.comboBox_input_2.currentText()

        standardization = float(value) * self.TypeDict[bigType][currentType]
        result = standardization / self.TypeDict[bigType][transType]

        self.originDataLabel.setText(f'{value} {currentType} =')
        self.transDataLabel.setText(f'{result} {transType}')
        self.lineEdit_input_2.setText(str(result))

    def typeChanged(self, text):
        self.comboBox_input_1.clear()
        self.comboBox_input_2.clear()


if __name__ == '__main__':
    app = QApplication([])
    window = MyWindow()
    window.show()
    app.exec()
