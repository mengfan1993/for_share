# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'trans.ui'
##
## Created by: Qt User Interface Compiler version 6.3.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QComboBox, QHBoxLayout, QLabel,
    QLayout, QLineEdit, QPushButton, QSizePolicy,
    QVBoxLayout, QWidget)

class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(277, 287)
        self.widget = QWidget(Form)
        self.widget.setObjectName(u"widget")
        self.widget.setGeometry(QRect(1, 5, 273, 256))
        self.verticalLayout = QVBoxLayout(self.widget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setSizeConstraint(QLayout.SetDefaultConstraint)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.originDataLabel = QLabel(self.widget)
        self.originDataLabel.setObjectName(u"originDataLabel")
        self.originDataLabel.setMaximumSize(QSize(16777215, 30))
        font = QFont()
        font.setFamilies([u"\u963f\u91cc\u5df4\u5df4\u666e\u60e0\u4f53"])
        font.setPointSize(16)
        self.originDataLabel.setFont(font)
        self.originDataLabel.setStyleSheet(u"color: rgb(103, 103, 103);")

        self.verticalLayout.addWidget(self.originDataLabel)

        self.transDataLabel = QLabel(self.widget)
        self.transDataLabel.setObjectName(u"transDataLabel")
        self.transDataLabel.setMinimumSize(QSize(0, 40))
        self.transDataLabel.setMaximumSize(QSize(16777215, 40))
        font1 = QFont()
        font1.setFamilies([u"\u963f\u91cc\u5df4\u5df4\u666e\u60e0\u4f53"])
        font1.setPointSize(30)
        self.transDataLabel.setFont(font1)

        self.verticalLayout.addWidget(self.transDataLabel)

        self.dataTypeCombox = QComboBox(self.widget)
        self.dataTypeCombox.setObjectName(u"dataTypeCombox")
        self.dataTypeCombox.setMinimumSize(QSize(0, 30))

        self.verticalLayout.addWidget(self.dataTypeCombox)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.lineEdit_input_1 = QLineEdit(self.widget)
        self.lineEdit_input_1.setObjectName(u"lineEdit_input_1")
        self.lineEdit_input_1.setMinimumSize(QSize(0, 30))

        self.horizontalLayout.addWidget(self.lineEdit_input_1)

        self.comboBox_input_1 = QComboBox(self.widget)
        self.comboBox_input_1.setObjectName(u"comboBox_input_1")
        self.comboBox_input_1.setMinimumSize(QSize(130, 30))
        self.comboBox_input_1.setMaximumSize(QSize(16777215, 16777215))

        self.horizontalLayout.addWidget(self.comboBox_input_1)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.lineEdit_input_2 = QLineEdit(self.widget)
        self.lineEdit_input_2.setObjectName(u"lineEdit_input_2")
        self.lineEdit_input_2.setMinimumSize(QSize(0, 30))

        self.horizontalLayout_2.addWidget(self.lineEdit_input_2)

        self.comboBox_input_2 = QComboBox(self.widget)
        self.comboBox_input_2.setObjectName(u"comboBox_input_2")
        self.comboBox_input_2.setMinimumSize(QSize(130, 30))

        self.horizontalLayout_2.addWidget(self.comboBox_input_2)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.PushButton_cal = QPushButton(self.widget)
        self.PushButton_cal.setObjectName(u"PushButton_cal")

        self.verticalLayout.addWidget(self.PushButton_cal)


        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"\u8fdb\u5236\u8f6c\u6362\u5668", None))
        self.originDataLabel.setText(QCoreApplication.translate("Form", u"TextLabel", None))
        self.transDataLabel.setText(QCoreApplication.translate("Form", u"TextLabel", None))
        self.PushButton_cal.setText(QCoreApplication.translate("Form", u"\u8ba1\u7b97", None))
    # retranslateUi

