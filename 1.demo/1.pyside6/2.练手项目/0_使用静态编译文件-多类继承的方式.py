from PySide6.QtWidgets import QApplication, QWidget
from loginui import Ui_Form


class MyWindow(QWidget, Ui_Form):
    def __init__(self):
        super().__init__()

        # self.ui = Ui_Form()  # 多类继承的好处就是不需要再定义self.ui
        self.ui.setupUi(self)


if __name__ == '__main__':
    app = QApplication([])
    window = MyWindow()
    window.show()
    app.exec()
