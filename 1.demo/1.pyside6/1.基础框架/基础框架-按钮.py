from PySide6.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QLineEdit


class MyWindow(QWidget):
    def __init__(self):
        super().__init__()
        btn = QPushButton("点击按钮", self)
        btn.setGeometry(100, 100, 200, 100)  # 修改按钮大小
        btn.setToolTip('按钮提示框')
        btn.setText('重新设置文字')

        lb = QLabel("点击按钮", self)


if __name__ == '__main__':
    app = QApplication([])
    window = MyWindow()
    window.show()
    app.exec()
