---
title: hexo-theme-Butterfly
date: 2023-12-07 22:50:53
updated:
tags: theme
categories:
keywords:
description:
top_img:
comments:
cover: /img/default_cover-3.png
toc:
toc_number:
toc_style_simple:
copyright:
copyright_author:
copyright_author_href:
copyright_url:
copyright_info:
mathjax:
katex:
aplayer:
highlight_shrink:
aside:
abcjs:
---

# Butterfly 主题文档-安装与配置

> 内容来源于github所指向的[官方博客网址](https://butterfly.js.org/posts/21cfbf15/)
>
>  Butterfly 版本为 [4.10](https://github.com/jerryc127/hexo-theme-butterfly/releases/tag/4.10)

[TOC]

# 快速开始

`hexo-theme-butterfly` 是基于 [hexo-theme-melody](https://github.com/Molunerfinn/hexo-theme-melody) 的基础上进行开发的。

## 安装 - gitee

稳定版【建议】，在你的 Hexo 根目录里

```powershell
git clone -b master https://gitee.com/immyw/hexo-theme-butterfly.git themes/butterfly
```

测试版(dev分支)，可能存在Bugs

```powershell
git clone -b dev https://gitee.com/immyw/hexo-theme-butterfly.git themes/butterfly
```

> 升级方法：在主题目录下，运行 git pull

## 应用主题

修改 Hexo 根目录下的 `_config.yml`，把主题改为 `butterfly`

```yaml
theme: butterfly
```

## 安装插件

如果你没有 pug 以及 stylus 的渲染器，请下载安装：

```powershell
npm install hexo-renderer-pug hexo-renderer-stylus --save
```

## 升级建议

> 升级完成后，请到 Github 的 [Releases](https://github.com/jerryc127/hexo-theme-butterfly/releases) 界面 或者 [文档七](https://butterfly.js.org/posts/198a4240/) 查看新版的更新内容。
>
> 里面有标注 `_config` 文件的变更内容（如有），请根据实际情况更新你的配置内容。

为了减少升级主题后带来的不便，请使用以下方法（建议，可以不做）。

在 hexo 的根目录创建一个文件 `_config.butterfly.yml`，并把主题目录的 `_config.yml` 内容复制到 `_config.butterfly.yml` 去。( 注意: 复制的是主题的 `_config.yml` ，而不是 hexo 的 `_config.yml`)

> 注意： 不要把主题目录的 `_config.yml` 删掉

> 注意： 以后只需要在 `_config.butterfly.yml `进行配置就行。
> 如果使用了 `_config.butterfly.yml`， 配置主题的 `_config.yml` 将不会有效果。

Hexo会自动合并主题中的 `_config.yml` 和 `_config.butterfly.yml` 里的配置，如果存在同名配置，会使用 `_config.butterfly.yml` 的配置，其优先度较高。

![image-20200805191531090](images/butterfly-docs-install-suggestion-1.png)

# 主题页面

## Front-matter

Front-matter 是 markdown 文件最上方以 `--- `分隔的区域，用于指定个别档案的变数。

- Page Front-matter 用于页面配置
- Post Front-matter 用于文章页配置

> 注意：如果标注`可选`的参数，可根据自己需要添加，不用全部都写在 markdown 里

### Page  Front-matter

```markdown
---
title:  
date:
updated:
type:
comments:
description:
keywords:
top_img:
mathjax:
katex:
aside:
aplayer:
highlight_shrink:
random:
---
```

| 写法             | 解释                                                                           |
| ---------------- | ------------------------------------------------------------------------------ |
| title            | 【必需】页面标题                                                               |
| date             | 【必需】页面创建日期                                                           |
| type             | 【必需】标签、分类和友情链接三个页面需要配置                                   |
| updated          | 【可选】页面更新日期                                                           |
| description      | 【可选】页面描述                                                               |
| keywords         | 【可选】页面关键字                                                             |
| comments         | 【可选】显示页面评论模块 (默认 true)                                           |
| top_img          | 【可选】页面顶部图片                                                           |
| mathjax          | 【可选】显示mathjax (当设置mathjax的per_page: false时，才需要配置，默认 false) |
| katex            | 【可选】显示katex (当设置katex的per_page: false时，才需要配置，默认 false)     |
| aside            | 【可选】显示侧边栏 (默认 true)                                                 |
| aplayer          | 【可选】在需要的页面加载aplayer的js和css,请参考文章下面的`音乐`配置            |
| highlight_shrink | 【可选】配置代码框是否展开 (true/false) (默认为设置中highlight_shrink的配置)   |
| random           | 【可选】配置友情链接是否随机排序（默认为 false）                               |

### Post  Front-matter

```markdown
---
title:
date:
updated:
tags:
categories:
keywords:
description:
top_img:
comments:
cover:
toc:
toc_number:
toc_style_simple:
copyright:
copyright_author:
copyright_author_href:
copyright_url:
copyright_info:
mathjax:
katex:
aplayer:
highlight_shrink:
aside:
abcjs:
---
```
| 写法                  | 解释                                                                                      |
| --------------------- | ----------------------------------------------------------------------------------------- |
| title                 | 【必需】文章标题                                                                          |
| date                  | 【必需】文章创建日期                                                                      |
| updated               | 【可选】文章更新日期                                                                      |
| tags                  | 【可选】文章标签                                                                          |
| categories            | 【可选】文章分类                                                                          |
| keywords              | 【可选】文章关键字                                                                        |
| description           | 【可选】文章描述                                                                          |
| top_img               | 【可选】文章顶部图片                                                                      |
| cover                 | 【可选】文章缩略图(如果没有设置top_img,文章页顶部将显示缩略图，可设为false/图片地址/留空) |
| comments              | 【可选】显示文章评论模块(默认 true)                                                       |
| toc                   | 【可选】显示文章TOC(默认为设置中toc的enable配置)                                          |
| toc_number            | 【可选】显示toc_number(默认为设置中toc的number配置)                                       |
| toc_style_simple      | 【可选】显示 toc 简洁模式                                                                 |
| copyright             | 【可选】显示文章版权模块(默认为设置中post_copyright的enable配置)                          |
| copyright_author      | 【可选】文章版权模块的文章作者                                                            |
| copyright_author_href | 【可选】文章版权模块的文章作者链接                                                        |
| copyright_url         | 【可选】文章版权模块的文章连结链接                                                        |
| copyright_info        | 【可选】文章版权模块的版权声明文字                                                        |
| mathjax               | 【可选】显示mathjax(当设置 mathjax 的 per_page: false 时，才需要配置，默认 false )        |
| katex                 | 【可选】显示 katex (当设置 katex 的 per_page: false 时，才需要配置，默认 false )          |
| aplayer               | 【可选】在需要的页面加载 aplayer 的 js 和 css,请参考文章下面的音乐 配置                   |
| highlight_shrink      | 【可选】配置代码框是否展开(true/false)(默认为设置中 highlight_shrink 的配置)              |
| aside                 | 【可选】显示侧边栏 (默认 true)                                                            |
| abcjs                 | 【可选】加载 abcjs (当设置 abcjs 的 per_page: false 时，才需要配置，默认 false )          |

### 标签页

1. 前往你的 Hexo 博客的根目录
2. 输入 hexo new page tags
3. 你会找到 source/tags/index.md 这个文件
4. 修改这个文件：

记得添加 `type: "tags"`

```markdown
---
title: 标签
date: 2018-01-05 00:00:00
type: "tags"
orderby: random
order: 1
---
```

| 参数    | 解释                                                             |
| ------- | ---------------------------------------------------------------- |
| type    | 【必须】页面类型，必须为 tags                                    |
| orderby | 【可选】排序方式 ：random/name/length                            |
| order   | 【可选】排序次序： 1, asc for ascending; -1, desc for descending |

### 分类页

1. 前往你的 Hexo 博客的根目录
2. 输入 hexo new page categories
3. 你会找到 source/categories/index.md 这个文件
4. 修改这个文件：

记得添加` type: "categories"`

```markdown
---
title: 分类
date: 2018-01-05 00:00:00
type: "categories"
---
```

### 友情链接

为你的博客创建一个友情链接！

#### 创建友情链接页面

1. 前往你的 Hexo 博客的根目录

2. 输入 hexo new page link
3. 你会找到 source/link/index.md 这个文件
4. 修改这个文件：

记得添加 `type: "link"`

```markdown
---
title: 友情链接
date: 2018-06-07 22:17:49
type: "link"
---
```

#### 友情链接添加

##### 本地生成

在Hexo博客目录中的 `source/_data`（如果没有 _data 文件夹，请自行创建），创建一个文件 `link.yml`

```yml
- class_name: 友情链接
  class_desc: 那些人，那些事
  link_list:
    - name: Hexo
      link: https://hexo.io/zh-tw/
      avatar: https://d33wubrfki0l68.cloudfront.net/6657ba50e702d84afb32fe846bed54fba1a77add/827ae/logo.svg
      descr: 快速、简单且强大的网志框架

- class_name: 网站
  class_desc: 值得推荐的网站
  link_list:
    - name: Youtube
      link: https://www.youtube.com/
      avatar: https://i.loli.net/2020/05/14/9ZkGg8v3azHJfM1.png
      descr: 视频网站
    - name: Weibo
      link: https://www.weibo.com/
      avatar: https://i.loli.net/2020/05/14/TLJBum386vcnI1P.png
      descr: 中国最大社交分享平台
    - name: Twitter
      link: https://twitter.com/
      avatar: https://i.loli.net/2020/05/14/5VyHPQqR6LWF39a.png
      descr: 社交分享平台
```

`class_name `和 `class_desc `支持 html 格式书写，如不需要，也可以留空。

##### 远程拉取

从 `4.0.0 `开始，支持从远程加载友情链接，远程拉取只支持` json`。

注意： 选择远程加载后，本地生成的方法会无效。

在 `source/link/index.md `这个文件的 front-matter 添加远程链接

```markdown
flink_url: xxxxx
```

Json 的格式

```json
[
  {
    "class_name": "友情链接",
    "class_desc": "那些人，那些事",
    "link_list": [
      {
        "name": "Hexo",
        "link": "https://hexo.io/zh-tw/",
        "avatar": "https://d33wubrfki0l68.cloudfront.net/6657ba50e702d84afb32fe846bed54fba1a77add/827ae/logo.svg",
        "descr": "快速、简单且强大的网志框架"
      }
    ]
  },
  {
    "class_name": "网站",
    "class_desc": "值得推荐的网站",
    "link_list": [
      {
        "name": "Youtube",
        "link": "https://www.youtube.com/",
        "avatar": "https://i.loli.net/2020/05/14/9ZkGg8v3azHJfM1.png",
        "descr": "视频网站"
      },
      {
        "name": "Weibo",
        "link": "https://www.weibo.com/",
        "avatar": "https://i.loli.net/2020/05/14/TLJBum386vcnI1P.png",
        "descr": "中国最大社交分享平台"
      },
      {
        "name": "Twitter",
        "link": "https://twitter.com/",
        "avatar": "https://i.loli.net/2020/05/14/5VyHPQqR6LWF39a.png",
        "descr": "社交分享平台"
      }
    ]
  }
]
```

#### 友情链接随机排序

主题支持友情链接随机排序，只需要在顶部 `front-matter` 添加 `random: true`

#### 友情链接界面设置

由 2.2.0 起，友情链接界面可以由用户自己自定义，只需要在友情链接的md档设置就行，以普通的Markdown格式书写。

### 图库 - todo 这里的内容应该后置

图库页面只是普通的页面，你只需要 `hexo n page xxxxx` 创建你的页面就行

然后使用标签外挂 [galleryGroup](https://butterfly.js.org/posts/4aa8abbe/#Gallery%E7%9B%B8%E5%86%8A%E5%9C%96%E5%BA%AB)，具体用法请查看对应的内容。

```yaml
<div class="gallery-group-main">
{% galleryGroup '壁纸' '收藏的一些壁纸' '/Gallery/wallpaper' https://i.loli.net/2019/11/10/T7Mu8Aod3egmC4Q.png %}
{% galleryGroup '漫威' '关于漫威的图片' '/Gallery/marvel' https://i.loli.net/2019/12/25/8t97aVlp4hgyBGu.jpg %}
{% galleryGroup 'OH MY GIRL' '关于OH MY GIRL的图片' '/Gallery/ohmygirl' https://i.loli.net/2019/12/25/hOqbQ3BIwa6KWpo.jpg %}
</div>
```

### 子页面

子页面也是普通的页面，你只需要 `hexo n page xxxxx` 创建你的页面就行

然后使用标签外挂 [gallery](https://butterfly.js.org/posts/4aa8abbe/#Gallery%E7%9B%B8%E5%86%8A)，具体用法请查看对应的内容。

```yaml
{% gallery %}
![](https://i.loli.net/2019/12/25/Fze9jchtnyJXMHN.jpg)
![](https://i.loli.net/2019/12/25/ryLVePaqkYm4TEK.jpg)
![](https://i.loli.net/2019/12/25/gEy5Zc1Ai6VuO4N.jpg)
![](https://i.loli.net/2019/12/25/d6QHbytlSYO4FBG.jpg)
![](https://i.loli.net/2019/12/25/6nepIJ1xTgufatZ.jpg)
![](https://i.loli.net/2019/12/25/E7Jvr4eIPwUNmzq.jpg)
![](https://i.loli.net/2019/12/25/mh19anwBSWIkGlH.jpg)
![](https://i.loli.net/2019/12/25/2tu9JC8ewpBFagv.jpg)
{% endgallery %}
```

> 如果你想要使用 /photo/ohmygirl 这样的链接显示你的图片内容
>
> 你可以把创建好的 ohmygirl 整个文件夹移到 photo 文件夹里去

### 404页面

主题内置了一个简单的 404 页面，可在设置中开启

> 本地预览时，访问出错的网站是不会跳到 404 页面的。
>
> 如需本地预览，请访问 http://localhost:4000/404.html

```yaml
# A simple 404 page
error_404:
  enable: true
  subtitle: "页面没有找到"
  background: 
```

# 主题配置

## 语言

修改站点配置文件 _config.yml

默认语言是 en

主题支持三种语言

- default(en)
- zh-CN (简体中文)
- zh-TW (繁体中文)

## 网站资料

修改网站各种资料，例如标题、副标题和邮箱等个人资料，请修改博客根目录的_config.yml

![网站资料](images/theme-Butterfly-config.png)

## 导航栏设置(Navigation bar settings)

### 参数设置

```yaml
nav:
  logo: #image
  display_title: true
  fixed: false # fixed navigation bar
```

| 参数          | 解释                                    |
| ------------- | --------------------------------------- |
| logo          | 网站的 logo，支持图片，直接填入图片链接 |
| display_title | 是否显示网站标题，填写 true 或者 false  |
| fixed         | 是否固定状态栏，填写 true 或者 false    |

### 菜单/目录

修改 `主题配置文件`

```yaml
Home: / || fas fa-home
Archives: /archives/ || fas fa-archive
Tags: /tags/ || fas fa-tags
Categories: /categories/ || fas fa-folder-open
List||fas fa-list:
  Music: /music/ || fas fa-music
  Movie: /movies/ || fas fa-video
Link: /link/ || fas fa-link
About: /about/ || fas fa-heart
```

必须是 `/xxx/`，后面`||`分开，然后写图标名。

如果不希望显示图标，图标名可不写。

默认子目录是展开的，如果你想要隐藏，在子目录里添加 `hide` 。

```yaml
List||fas fa-list||hide:
  Music: /music/ || fas fa-music
  Movie: /movies/ || fas fa-video
```

注意： 导航的文字可自行更改

例如：

```markdown
menu:
  首页: / || fas fa-home
  时间轴: /archives/ || fas fa-archive
  标签: /tags/ || fas fa-tags
  分类: /categories/ || fas fa-folder-open
  清单||fa fa-heartbeat:
    音乐: /music/ || fas fa-music
    照片: /Gallery/ || fas fa-images
    电影: /movies/ || fas fa-video
  友链: /link/ || fas fa-link
  关于: /about/ || fas fa-heart
```

![导航栏](images/theme-Butterfly-Navigation_bar.png)

## 代码(code Blocks)

> 代码块中的所有功能只适用于 Hexo 自带的代码渲染
>
> 如果使用第三方的渲染器，不一定会有效

### 代码高亮主题

Butterfly 支持6种代码高亮样式：

- darker
- pale night
- light
- ocean
- mac
- mac light

修改 `主题配置文件`

```yaml
highlight_theme: light
```

==具体效果：略==

### 代码复制

主题支持代码复制功能

修改 `主题配置文件`

```yaml
highlight_copy: true
```

### 代码框展开/关闭

在默认情况下，代码框自动展开，可设置是否所有代码框都关闭状态，点击`>`可展开代码

- true 全部代码框不展开，需点击`>`打开
- false 代码框展开，有`>`点击按钮
- none 不显示`>`按钮

修改 `主题配置文件`

```yaml
highlight_shrink: true #代码框不展开，需点击 '>' 打开
```

> 你也可以在post/page页对应的markdown文件front-matter添加highlight_shrink来独立配置。
>
> 当主题配置文件中的 highlight_shrink 设为true时，可在front-matter添加highlight_shrink: false来单独配置文章展开代码框。
>
> 当主题配置文件中的 highlight_shrink 设为false时，可在front-matter添加highlight_shrink: true来单独配置文章收缩代码框。
>

### 代码换行

在默认情况下，Hexo 在编译的时候不会实现代码自动换行。如果你不希望在代码块的区域里有横向滚动条的话，那么你可以考虑开启这个功能。

修改` 主题配置文件`

```yaml
code_word_wrap: true
```

如果你是使用 highlight 渲染，需要找到你站点的 Hexo 配置文件`_config.yml`，将`line_number`改成`false`:

```YAML
highlight:
  enable: true
  line_number: false # <- 改这里
  auto_detect: false
  tab_replace:
```

如果你是使用 prismjs 渲染，需要找到你站点的 Hexo 配置文件`_config.yml`，将`line_number`改成`false`:

```yaml
prismjs:
  enable: false
  preprocess: true
  line_number: false # <- 改这里
  tab_replace: ''
```

###  代码高度限制

可配置代码高度限制，超出的部分会隐藏，并显示展开按钮。

```yaml
highlight_height_limit: false # unit: px
```

注意：

1. 单位是 `px`，直接添加数字，如 200
2. 实际限制高度为 `highlight_height_limit + 30 px `，多增加 30px 限制，目的是避免代码高度只超出highlight_height_limit 一点时，出现展开按钮，展开没内容。
3. 不适用于隐藏后的代码块（ css 设置 display: none）

![](https://jsd.012700.xyz/gh/jerryc127/CDN@m2/img/hexo-theme-butterfly-docs-highlight-heigh-limit.gif)

## 社交图标(Social Settings)

Butterfly支持 [font-awesome v6](https://fontawesome.com/icons?from=io) 图标.

书写格式 图标名：`url || 描述性文字 || color`

```yaml
social:
  fab fa-github: https://github.com/xxxxx || Github || "#hdhfbb"
  fas fa-envelope: mailto:xxxxxx@gmail.com || Email || "#000000"
```

图标名可在这寻找

![社交图标](https://jsd.012700.xyz/gh/jerryc127/CDN/img/hexo-theme-butterfly-doc-fontawesome.png)

## 头像

修改 `主题配置文件`

```yaml
avatar:
  img: /img/avatar.png
  effect: true # 头像会一直转圈
```

## 顶部图

> 如果不要显示顶部图，可直接配置 disable_top_img: true

> 顶部图的获取顺序，如果都没有配置，则不显示顶部图。
>
> 1. 页面顶部图的获取顺序：各自配置的 top_img > 配置文件的 default_top_img
> 2. 文章页顶部图的获取顺序：各自配置的 top_img > cover > 配置文件的 default_top_img

配置中的值：

| 配置             | 解释                                                                |
| ---------------- | ------------------------------------------------------------------- |
| index_img        | 主页的 top_img                                                      |
| default_top_img  | 默认的 top_img，当页面的 top_img 没有配置时，会显示 default_top_img |
| archive_img      | 归档页面的 top_img                                                  |
| tag_img          | tag 子页面 的 默认 top_img                                          |
| tag_per_img      | tag 子页面的 top_img，可配置每个 tag 的 top_img                     |
| category_img     | category 子页面 的 默认 top_img                                     |
| category_per_img | category 子页面的 top_img，可配置每个 category 的 top_img           |

其它页面 （tags/categories/自建页面）和 文章页 的 `top_img `，请到对应的 md 页面设置`front-matter`中的`top_img`

以上所有的 top_img 可配置以下值

| 配置的值                                                                                                                                         | 效果                                                                                            |
| ------------------------------------------------------------------------------------------------------------------------------------------------ | ----------------------------------------------------------------------------------------------- |
| 留空                                                                                                                                             | 显示默认的 top_img（如有），否则显示默认的顔色<br/>（文章页top_img留空的话，会显示 cover 的值） |
| img链接                                                                                                                                          | 图片的链接，显示所配置的图片                                                                    |
| 顔色(<br/>HEX值 - #0000FF<br/>RGB值 - rgb(0,0,255)<br/>顔色单词 - orange<br/>渐变色 - linear-gradient( 135deg, #E2B0FF 10%, #9F44D3 100%)<br/>） | 对应的顔色                                                                                      |
| transparent                                                                                                                                      | 透明                                                                                            |
| false                                                                                                                                            | 不显示 top_img                                                                                  |

`tag_per_img `和 `category_per_img` 是 3.2.0 新增的内容，可对 tag 和 category 进行单独的配置

并不推荐为每个 tag 和每个 category 都配置不同的顶部图，因为配置太多会拖慢生成速度

==后续部分内容：略==

## 文章封面

文章的 markdown 文档上,在` Front-matter` 添加 `cover `,并填上要显示的图片地址。

如果不配置 `cover`,可以设置显示默认的 cover。

如果不想在首页显示 cover, 可以设置为 `false`。

> 文章封面的获取顺序 Front-matter 的 cover > 配置文件的 default_cover > false

```yaml
cover:
  # 是否显示文章封面
  index_enable: true
  aside_enable: true
  archives_enable: true
  # 封面显示的位置
  # 三个值可配置 left , right , both
  position: both
  # 当没有设置cover时，默认的封面显示
  default_cover: 
```

| 参数            | 解释                                                                                                                     |
| --------------- | ------------------------------------------------------------------------------------------------------------------------ |
| index_enable    | 主页是否显示文章封面图                                                                                                   |
| aside_enable    | 侧栏是否显示文章封面图                                                                                                   |
| archives_enable | 归档页面是否显示文章封面图                                                                                               |
| position        | 主页卡片文章封面的显示位置<br/>- left：全部显示在左边<br/>- right：全部显示在右边<br/>- both：封面位置以左右左右轮流显示 |
| default_cover   | 默认的 cover, 可配置图片链接/顔色/渐变色等                                                                               |

==后续图片展示：略==

## 页面meta显示

这个选项是用来显示文章的相关信息

修改 `主题配置文件`

```yaml
post_meta:
  page:
    date_type: both # created or updated or both 主页文章日期是创建日或者更新日或都显示
    date_format: relative # date/relative 显示日期还是相对日期
    categories: true # true or false 主页是否显示分类
    tags: true # true or false 主页是否显示标签
    label: true # true or false 显示描述性文字
  post:
    date_type: both # created or updated or both 文章页日期是创建日或者更新日或都显示
    date_format: relative # date/relative 显示日期还是相对日期
    categories: true # true or false 文章页是否显示分类
    tags: true # true or false 文章页是否显示标签
    label: true # true or false 显示描述性文字
```

`date_format` 是 3.2.0 新增的内容，配置时间显示明确时间还是相对时间

## 主页文章节选(自动节选和文章页description)

因为主题UI的关係，`主页文章节选`只支持`自动节选`和`文章页description`。

在`butterfly`里，有四种可供选择

1. description： 只显示description
2. both： 优先选择description，如果没有配置description，则显示自动节选的内容
3. auto_excerpt：只显示自动节选
4. false： 不显示文章内容

修改 `主题配置文件`

```yaml
index_post_content:
  method: 3
  length: 500 # if you set method to 2 or 3, the length need to config
```

## 页面锚点

开启页面锚点后，当你在进行滚动时，页面链接会根据标题ID进行替换
(注意: 每替换一次，会留下一个历史记录。所以如果一篇文章有很多锚点的话，网页的历史记录会很多。)

修改 `主题配置文件`

```yaml
# anchor
anchor:
  # when you scroll, the URL will update according to header id.
  auto_update: false
  # Click the headline to scroll and update the anchor
  click_to_scroll: false
```

## 图片描述

可开启图片Figcaption描述文字显示

优先显示图片的 title 属性，然后是 alt 属性

修改 `主题配置文件`

```yaml
photofigcaption: true
```

## 复制相关配置

可配置网站是否可以复制、复制的内容是否添加版权信息

```yaml
# copy settings
# copyright: Add the copyright information after copied content (复制的内容后面加上版权信息)
copy:
  enable: true
  copyright:
    enable: true
    limit_count: 50
```

| 配置        | 解释                                                                   |
| ----------- | ---------------------------------------------------------------------- |
| enable      | 是否开启网站复制权限                                                   |
| copyright   | 复制的内容后面加上版权信息                                             |
| enable      | 是否开启复制版权信息添加                                               |
| limit_count | 字数限制，当复制文字大于这个字数限制时，将在复制的内容后面加上版权信息 |

添加版权信息后

```plaintext
Lorem ipsum dolor sit amet, test link consectetur adipiscing elit. Strong text pellentesque ligula commodo viverra vehicula. Italic text at ullamcorper enim. Morbi a euismod nibh. Underline text non elit nisl. Deleted text tristique, sem id condimentum tempus, metus lectus venenatis mauris, sit amet semper lorem felis a eros. Fusce egestas nibh at sagittis auctor. Sed ultricies ac arcu quis molestie. Donec dapibus nunc in nibh egestas, vitae volutpat sem iaculis. Curabitur sem tellus, elementum nec quam id, fermentum laoreet mi. Ut mollis ullamcorper turpis, vitae facilisis velit ultricies sit amet. Etiam laoreet dui odio, id tempus justo tincidunt id. Phasellus scelerisque nunc sed nunc ultricies accumsan.


作者: Jerry
連結: https://butterfly.js.org/posts/4aa8abbe/#%E8%A4%87%E8%A3%BD%E7%9B%B8%E9%97%9C%E9%85%8D%E7%BD%AE
來源: Butterfly
著作權歸作者所有。商業轉載請聯繫作者獲得授權，非商業轉載請註明出處。
```

## 文章页相关配置

### 文章版权

为你的博客文章展示文章版权和许可协议。

修改 `主题配置文件`

```yaml
post_copyright:
  enable: true
  decode: false
  author_href:
  license: CC BY-NC-SA 4.0
  license_url: https://creativecommons.org/licenses/by-nc-sa/4.0/
```

由于Hexo 4.1开始，默认对网址进行解码，以至于如果是中文网址，会被解码，可设置decode: true来显示中文网址。

如果有文章（例如：转载文章）不需要显示版权，可以在文章Front-matter单独设置

```yaml
copyright: false
```

从`3.0.0`开始，支持对单独文章设置版权信息，可以在文章Front-matter单独设置

```markdown
copyright_author: xxxx
copyright_author_href: https://xxxxxx.com
copyright_url: https://xxxxxx.com
copyright_info: 此文章版权归xxxxx所有，如有转载，请注明来自原作者
```

### 文章打赏

在你每篇文章的结尾，可以添加打赏按钮。相关二维码可以自行配置。

对于没有提供二维码的，可配置一张软件的icon图片，然后在link上添加相应的打赏链接。用户点击图片就会跳转到链接去。

link可以不写，会默认为图片的链接。

修改 `主题配置文件`

```yaml
reward:
  enable: true
  text:
  QR_code:
    - img: /img/wechat.jpg
      link:
      text: 微信
    - img: /img/alipay.jpg
      link:
      text: 支付宝
```

### TOC-目录

在文章页，会有一个目录，用于显示TOC。

修改 `主题配置文件`

```yaml
toc:
  post: true  # 文章页是否显示 TOC
  page: true  # 普通页面是否显示 TOC
  number: true  # 是否显示章节数
  expand: false  # 是否展开 TOC
  style_simple: false # 简洁模式（侧边栏只显示 TOC, 只对文章页post有效 ）
  scroll_percent: true  # 是否显示滚动进度百分比
```

#### 为特定的文章配置

在你的文章`md`文件的头部，加入`toc_number`和`toc`，并配置`true`或者`false`即可。

主题会优先判断文章Markdown的Front-matter是否有配置，如有，则以Front-matter的配置为准。否则，以**主题配置文件**中的配置为准

### 相关文章

> 当文章封面设置为 false 时，或者没有获取到封面配置，相关文章背景将会显示主题色。

相关文章推荐的原理是根据文章tags的比重来推荐

修改 `主题配置文件`

```yaml
related_post:
  enable: true
  limit: 6 # 显示推荐文章数目
  date_type: created # or created or updated 文章日期显示创建日或者更新日
```

![img](https://jsd.012700.xyz/gh/jerryc127/CDN/img/hexo-theme-butterfly-docs-releatedpost.png)

### 文章过期提醒

可设置是否显示文章过期提醒，以更新时间为基准。

```markdown
# Displays outdated notice for a post (文章过期提醒)
noticeOutdate:
  enable: true
  style: flat # style: simple/flat
  limit_day: 365 # When will it be shown
  position: top # position: top/bottom
  message_prev: It has been
  message_next: days since the last update, the content of the article may be outdated.
```

`limit_day`： 距离更新时间多少天才显示文章过期提醒

`message_prev `： 天数之前的文字

`message_next`：天数之后的文字

### 文章编辑按钮

在文章标题旁边显示一个编辑按钮，点击会跳转到对应的链接去。

```yaml
# Post edit
# Easily browse and edit blog source code online.
post_edit:
  enable: false
  # url: https://github.com/user-name/repo-name/edit/branch-name/subdirectory-name/
  # For example: https://github.com/jerryc127/butterfly.js.org/edit/main/source/
  url:
```

### 文章分页按钮

> 当文章封面设置为 false 时，或者没有获取到封面配置，分页背景将会显示主题色。

可设置分页的逻辑，也可以关闭分页显示

```yaml
# post_pagination (分页)
# value: 1 || 2 || false
# 1: The 'next post' will link to old post
# 2: The 'next post' will link to new post
# false: disable pagination
post_pagination: false
```

| 参数                   | 解释                 |
| ---------------------- | -------------------- |
| post_pagination: false | 关闭分页按钮         |
| post_pagination: 1     | 下一篇显示的是旧文章 |
| post_pagination: 2     | 下一篇显示的是新文章 |

## Footer设置

### 博客年份

`since`是一个来展示你站点起始时间的选项。它位于页面的最底部。

修改 `主题配置文件`

```yaml
footer:
  owner:
    enable: true
    since: 2018
```

### 页脚自定义文本

`custom_text`是一个给你用来在页脚自定义文本的选项。通常你可以在这里写声明文本等。支持 HTML。

修改 `主题配置文件`

```yaml
custom_text: Hi, welcome to my <a href="https://butterfly.js.org/">blog</a>!
```

对于部分人需要写 ICP 的，也可以写在 `custom_text`里

```yaml
custom_text: <a href="icp链接"><img class="icp-icon" src="icp图片"><span>备案号：xxxxxx</span></a>
```

## 侧边栏设置

### 侧边排版

可自行决定哪个项目需要显示，可决定位置，也可以设置不显示侧边栏。

修改 `主题配置文件`

```yaml
aside:
  enable: true
  hide: false
  button: true
  mobile: true # display on mobile
  position: right # left or right
  display:
    archive: true
    tag: true
    category: true
  card_author:
    enable: true
    description:
    button:
      enable: true
      icon: fab fa-github
      text: Follow Me
      link: https://github.com/xxxxxx
  card_announcement:
    enable: true
    content: This is my Blog
  card_recent_post:
    enable: true
    limit: 5 # if set 0 will show all
    sort: date # date or updated
    sort_order: # Don't modify the setting unless you know how it works
  card_categories:
    enable: true
    limit: 8 # if set 0 will show all
    expand: none # none/true/false
    sort_order: # Don't modify the setting unless you know how it works
  card_tags:
    enable: true
    limit: 40 # if set 0 will show all
    color: false
    orderby: random # Order of tags, random/name/length
    order: 1 # Sort of order. 1, asc for ascending; -1, desc for descending
    sort_order: # Don't modify the setting unless you know how it works
  card_archives:
    enable: true
    type: monthly # yearly or monthly
    format: MMMM YYYY # eg: YYYY年MM月
    order: -1 # Sort of order. 1, asc for ascending; -1, desc for descending
    limit: 8 # if set 0 will show all
    sort_order: # Don't modify the setting unless you know how it works
  card_webinfo:
    enable: true
    post_count: true
    last_push_date: true
    sort_order: # Don't modify the setting unless you know how it works
  card_post_series:
    enable: true
    orderBy: 'date' # Order by title or date
    order: -1 # Sort of order. 1, asc for ascending; -1, desc for descending
```

### 访问人数busuanzi(UV和PV)

访问 busuanzi 的[官方网站](http://busuanzi.ibruce.info/)查看更多的介绍。

修改 `主题配置文件`

```yaml
busuanzi:
  site_uv: true
  site_pv: true
  page_pv: true
```

> 如果需要修改 busuanzi 的 CDN 链接，可通过 `主题配置文件` 的 `CDN` 中的 `option `进行修改

```yaml
CDN:
  option:
  	busuanzi: xxxxxxxxx
```

![](https://jsd.012700.xyz/gh/jerryc127/CDN/img/hexo-theme-butterfly-doc-busuanzi-site-pv.png)

![](https://jsd.012700.xyz/gh/jerryc127/CDN/img/hexo-theme-butterfly-doc-pv.png)

### 运行时间

网页已运行时间

修改 `主题配置文件`

```
runtimeshow:
  enable: true
  publish_date: 6/7/2018 00:00:00  
  ##网页开通时间
  #格式: 月/日/年 时间
  #也可以写成 年/月/日 时间
```

![](https://jsd.012700.xyz/gh/jerryc127/CDN/img/hexo-theme-butterfly-doc-runtime.png)

###  最新评论?

> 3.1.0 起支持
>
> 最新评论只会在刷新时才会去读取，并不会实时变化
>
> 由于 API 有 访问次数限制，为了避免调用太多，主题默认存取期限为 10 分钟。也就是説，调用后资料会存在 localStorage 里，10分钟内刷新网站只会去 localStorage 读取资料。 10 分钟期限一过，刷新页面时才会去调取 API 读取新的数据。（ 3.6.0 新增了 `storage `配置，可自行配置缓存时间）
>

在侧边栏显示最新评论板块

修改 `主题配置文件`

```JS
# Aside widget - Newest Comments
newest_comments:
  enable: true
  sort_order: # Don't modify the setting unless you know how it works
  limit: 6
  storage: 10 # unit: mins, save data to localStorage
  avatar: true
```

部分配置解释：

| 配置    | 解释                    |
| ------- | ----------------------- |
| limit   | 显示的数量              |
| storage | 设置缓存时间，单位 分钟 |
| avatar  | 是否显示头像            |

![](https://jsd.012700.xyz/gh/jerryc127/CDN/img/hexo-theme-butterfly-docs-newest-comments.png)

### 自定义侧边栏

侧边栏可以添加自己喜欢的 widget，也可以对现有的 widget 进行排序（博客资料和 公告这两个固定，其它的能排序）

#### widegt排序

只需要配置 `sort_order`就行。（使用了 `Flex `佈局的 `order` 属性，具体可查看 [mozilla 文档](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Ordering_Flex_Items)。简单来讲，就是配置数字来实现排序，如果不配置，则默认为 0。数字越小，排序越靠前。

```yaml
aside:
 ...
  card_recent_post:
    sort_order: # Don't modify the setting unless you know how it works
  card_categories:
    sort_order: # Don't modify the setting unless you know how it works
  card_tags:
    sort_order: # Don't modify the setting unless you know how it works
  card_archives:
    sort_order: # Don't modify the setting unless you know how it works
  card_webinfo:
    sort_order: # Don't modify the setting unless you know how it works

newest_comments:
  enable: true
  sort_order: # Don't modify the setting unless you know how it works
```

#### 自定义widget

如果你想添加自己的内容到侧边栏去，你可以自定义

##### 创建widget.yml

在Hexo博客目录中的`source/_data`（如果没有 _data 文件夹，请自行创建），创建一个文件 `widget.yml`

```yaml
top:
  - class_name:
    id_name:
    name:
    icon:
    html:

bottom:
  - class_name:
    id_name:
    name:
    icon:
    order:
    html:
```

**top**: 创建的 widget 会出现在非 sticky 区域（即所有页面都会显示)

**bottom**: 创建的 widget 会出现在 sticky 区域（除了文章页都会显示)

| 参数       | 解释                                   |
| ---------- | -------------------------------------- |
| class_name | 所创建的 widget 父类 class 名 （可选） |
| id_name    | 所创建的 widget 父类 id 名（可选）     |
| name       | 所创建的 widget 标题                   |
| icon       | 所创建的 widget 图标                   |
| order      | 所创建的 widget 排序 （可选）          |
| html       | 所创建的 widget 相关代码               |

![](https://jsd.012700.xyz/gh/jerryc127/CDN/img/adside-diy-parameter.png)

生成的 代码 为

```html
<div class="card-widget 所写的 class_name" id="所写的 id_name" style="order: 所写的 order">
    <div class="item-headline">
        <i class="所写的 icon"></i>
        <span>所写的 name</span>
    </div>
    <div class="item-content">
        所写的 html
    </div>
</div>
```

如果你需要对添加的 widget 进行 UI 调整，请自行添加 css 到 inject 去。

##### 例子

以 [访客地图](https://clustrmaps.com/profile/1b7ep/widget/code/globe) 为例子

1. 获取访客地图的 html 代码

   ```html
   <script type="text/javascript" id="clstr_globe" src="//clustrmaps.com/globe.js?d=5V2tOKp8qAdRM-i8eu7ETTO9ugt5uKbbG-U7Yj8uMl8"></script>
   ```

2. 创建 widget.yml

   ```yaml
   bottom:
       - class_name: user-map
         id_name: user-map
         name: 访客地图
         icon: fas fa-heartbeat
         order:
         html: '<script type="text/javascript" id="clstr_globe" src="//clustrmaps.com/globe.js?d=5V2tOKp8qAdRM-i8eu7ETTO9ugt5uKbbG-U7Yj8uMl8"></script>'
   ```

3. 运行hexo

![](https://jsd.012700.xyz/gh/jerryc127/CDN/img/aside-diy-sample.png)

## 左下角按钮(Button right button)

### 简繁转换

简体繁体互换，右下角会有简繁转换按钮。

修改 `主题配置文件`

```
translate:
  enable: true
  # 默认按钮显示文字(网站是简体，应设置为'default: 繁')
  default: 简
  #网站默认语言，1: 繁体中文, 2: 简体中文
  defaultEncoding: 1
  #延迟时间,若不在前, 要设定延迟翻译时间, 如100表示100ms,默认为0
  translateDelay: 0
  #当文字是简体时，按钮显示的文字
  msgToTraditionalChinese: "繁"
  #当文字是繁体时，按钮显示的文字
  msgToSimplifiedChinese: "简"
```

### 阅读模型

閲读模式下会去掉除文章外的内容，避免干扰閲读。只会出现在文章页面，右下角会有閲读模式按钮。

修改 `主题配置文件`

```yaml
readmode: true
```

![](https://jsd.012700.xyz/gh/jerryc127/CDN/img/hexo-theme-butterfly-doc-read-mode.png)

### 夜间模型

右下角会有夜间模式按钮

修改 `主题配置文件`

```yaml
# dark mode
darkmode:
  enable: true
  # dark mode和 light mode切换按钮
  button: true
  autoChangeMode: false
  # Set the light mode time. The value is between 0 and 24. If not set, the default value is 6 and 18
  start: # 8
  end: # 22
```

| 参数           | 解释                                                                                                                                                                                                                                                   |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| button         | 是否在右下角显示日夜模式切换按钮                                                                                                                                                                                                                       |
| autoChangeMode | 自动切换的模式<br/>autoChangeMode: 1 跟随系统而变化，不支持的浏览器/系统将按照时间 start 到 end 之间切换为 light mode<br/>autoChangeMode: 2 只按照时间 start 到 end 之间切换为 light mode ,其余时间为 dark mode<br/>autoChangeMode: false 取消自动切换 |
| start          | light mode 的开始时间                                                                                                                                                                                                                                  |
| end            | light mode 的结束时间                                                                                                                                                                                                                                  |

![](https://jsd.012700.xyz/gh/jerryc127/CDN/img/hexo-theme-butterfly-doc-dark-mode-1.png)

### 滚动状态百分比

主题配置文件中

```yaml
# show scroll percent in scroll-to-top button
rightside_scroll_percent: true
```

![img](https://cdn.jsdelivr.net/gh/jerryc127/CDN@m2/img/hexo-butterfly-docs-scroll-percent-right-btn.gif)

### 按钮排序

```yaml
# Don't modify the following settings unless you know how they work (非必要请不要修改 )
# Choose: readmode,translate,darkmode,hideAside,toc,chat,comment
# Don't repeat 不要重复
rightside_item_order:
  enable: false
  hide: # readmode,translate,darkmode,hideAside
  show: # toc,chat,comment
```

## 标签外挂(Tag Plugins)

{% note info simple %}

标签外挂是Hexo独有的功能，并不是标准的Markdown格式。

以下的写法，只适用于Butterfly主题，用在其它主题上不会有效果，甚至可能会报错。使用前请留意

{% endnote %}



{% note warning simple %}
标签外挂虽然能为主题带来一些额外的功能和UI方面的强化，但是，标签外挂也有明显的限制，使用时请留意。
{% endnote %}



### Note(Bootstrap Callout)

{% tabs  test %}
<!-- tab 通用设置 -->
移植于next主题，并进行修改。

修改 `主题配置文件`

```yaml
note:
  # Note tag style values:
  #  - simple    bs-callout old alert style. Default.
  #  - modern    bs-callout new (v2-v3) alert style.
  #  - flat      flat callout style with background, like on Mozilla or StackOverflow.
  #  - disabled  disable all CSS styles import of note tag.
  style: simple
  icons: false
  border_radius: 3
  # Offset lighter of background in % for modern and flat styles (modern: -12 | 12; flat: -18 | 6).
  # Offset also applied to label tag variables. This option can work with disabled note tag.
  light_bg_offset: 0
```

`icons`和`light_bg_offset`只对*方法一*生效

Note 标签外挂有两种用法

<!-- endtab -->

<!-- tab 用法1 -->

```markdown
{% note [class] [no-icon] [style] %}
Any content (support inline tags too.io).
{% endnote %}
```

| 名称    | 用法                                                                                                 |
| ------- | ---------------------------------------------------------------------------------------------------- |
| class   | 【可选】标识，不同的标识有不同的配色<br/>（ default / primary / success / info / warning / danger ） |
| no-icon | 【可选】不显示 icon                                                                                  |
| style   | 【可选】可以覆盖配置中的 style<br/>（simple/modern/flat/disabled）                                   |

> simple

```markdown
{% note simple %}
默认 提示块标签
{% endnote %}

{% note default simple %}
default 提示块标签
{% endnote %}

{% note primary simple %}
primary 提示块标签
{% endnote %}

{% note success simple %}
success 提示块标签
{% endnote %}

{% note info simple %}
info 提示块标签
{% endnote %}

{% note warning simple %}
warning 提示块标签
{% endnote %}

{% note danger simple %}
danger 提示块标签
{% endnote %}
```

{% note simple %}
默认 提示块标签
{% endnote %}

{% note default simple %}
default 提示块标签
{% endnote %}

{% note primary simple %}
primary 提示块标签
{% endnote %}

{% note success simple %}
success 提示块标签
{% endnote %}

{% note info simple %}
info 提示块标签
{% endnote %}

{% note warning simple %}
warning 提示块标签
{% endnote %}

{% note danger simple %}
danger 提示块标签
{% endnote %}

> modern

```markdown
{% note modern %}
默认 提示块标签
{% endnote %}

{% note default modern %}
default 提示块标签
{% endnote %}

{% note primary modern %}
primary 提示块标签
{% endnote %}

{% note success modern %}
success 提示块标签
{% endnote %}

{% note info modern %}
info 提示块标签
{% endnote %}

{% note warning modern %}
warning 提示块标签
{% endnote %}

{% note danger modern %}
danger 提示块标签
{% endnote %}
```

{% note modern %}
默认 提示块标签
{% endnote %}

{% note default modern %}
default 提示块标签
{% endnote %}

{% note primary modern %}
primary 提示块标签
{% endnote %}

{% note success modern %}
success 提示块标签
{% endnote %}

{% note info modern %}
info 提示块标签
{% endnote %}

{% note warning modern %}
warning 提示块标签
{% endnote %}

{% note danger modern %}
danger 提示块标签
{% endnote %}

> flat

```markdown
{% note flat %}
默认 提示块标签
{% endnote %}

{% note default flat %}
default 提示块标签
{% endnote %}

{% note primary flat %}
primary 提示块标签
{% endnote %}

{% note success flat %}
success 提示块标签
{% endnote %}

{% note info flat %}
info 提示块标签
{% endnote %}

{% note warning flat %}
warning 提示块标签
{% endnote %}

{% note danger flat %}
danger 提示块标签
{% endnote %}
```

> disabled

```markdown
{% note disabled %}
默认 提示块标签
{% endnote %}

{% note default disabled %}
default 提示块标签
{% endnote %}

{% note primary disabled %}
primary 提示块标签
{% endnote %}

{% note success disabled %}
success 提示块标签
{% endnote %}

{% note info disabled %}
info 提示块标签
{% endnote %}

{% note warning disabled %}
warning 提示块标签
{% endnote %}

{% note danger disabled %}
danger 提示块标签
{% endnote %}
```

{% note disabled %}
默认 提示块标签
{% endnote %}

{% note default disabled %}
default 提示块标签
{% endnote %}

{% note primary disabled %}
primary 提示块标签
{% endnote %}

{% note success disabled %}
success 提示块标签
{% endnote %}

{% note info disabled %}
info 提示块标签
{% endnote %}

{% note warning disabled %}
warning 提示块标签
{% endnote %}

{% note danger disabled %}
danger 提示块标签
{% endnote %}

> no-icon

```markdown
{% note no-icon %}
默认 提示块标签
{% endnote %}

{% note default no-icon %}
default 提示块标签
{% endnote %}

{% note primary no-icon %}
primary 提示块标签
{% endnote %}

{% note success no-icon %}
success 提示块标签
{% endnote %}

{% note info no-icon %}
info 提示块标签
{% endnote %}

{% note warning no-icon %}
warning 提示块标签
{% endnote %}

{% note danger no-icon %}
danger 提示块标签
{% endnote %}
```

{% note no-icon %}
默认 提示块标签
{% endnote %}

{% note default no-icon %}
default 提示块标签
{% endnote %}

{% note primary no-icon %}
primary 提示块标签
{% endnote %}

{% note success no-icon %}
success 提示块标签
{% endnote %}

{% note info no-icon %}
info 提示块标签
{% endnote %}

{% note warning no-icon %}
warning 提示块标签
{% endnote %}

{% note danger no-icon %}
danger 提示块标签
{% endnote %}

<!-- endtab -->

<!-- tab 用法2(自定义icon) -->

> 3.2.0 以上版本支持

```markdown
{% note [color] [icon] [style] %}
Any content (support inline tags too.io).
{% endnote %}
```

| 名称  | 用法                                                                     |
| ----- | ------------------------------------------------------------------------ |
| color | 【可选】顔色<br/>(default / blue / pink / red / purple / orange / green) |
| icon  | 【可选】可配置自定义 icon (只支持 fontawesome 图标, 也可以配置 no-icon ) |
| style | 【可选】可以覆盖配置中的 style<br/>（simple/modern/flat/disabled）       |

> simple

```markdown
{% note 'fab fa-cc-visa' simple %}
你是刷 Visa 还是 UnionPay
{% endnote %}
{% note blue 'fas fa-bullhorn' simple %}
2021年快到了....
{% endnote %}
{% note pink 'fas fa-car-crash' simple %}
小心开车 安全至上
{% endnote %}
{% note red 'fas fa-fan' simple%}
这是三片呢？还是四片？
{% endnote %}
{% note orange 'fas fa-battery-half' simple %}
你是刷 Visa 还是 UnionPay
{% endnote %}
{% note purple 'far fa-hand-scissors' simple %}
剪刀石头布
{% endnote %}
{% note green 'fab fa-internet-explorer' simple %}
前端最讨厌的浏览器
{% endnote %}
```

{% note 'fab fa-cc-visa' simple %}
你是刷 Visa 还是 UnionPay
{% endnote %}
{% note blue 'fas fa-bullhorn' simple %}
2021年快到了....
{% endnote %}
{% note pink 'fas fa-car-crash' simple %}
小心开车 安全至上
{% endnote %}
{% note red 'fas fa-fan' simple%}
这是三片呢？还是四片？
{% endnote %}
{% note orange 'fas fa-battery-half' simple %}
你是刷 Visa 还是 UnionPay
{% endnote %}
{% note purple 'far fa-hand-scissors' simple %}
剪刀石头布
{% endnote %}
{% note green 'fab fa-internet-explorer' simple %}
前端最讨厌的浏览器
{% endnote %}

> modern

```markdown
{% note 'fab fa-cc-visa' modern %}
你是刷 Visa 还是 UnionPay
{% endnote %}
{% note blue 'fas fa-bullhorn' modern %}
2021年快到了....
{% endnote %}
{% note pink 'fas fa-car-crash' modern %}
小心开车 安全至上
{% endnote %}
{% note red 'fas fa-fan' modern%}
这是三片呢？还是四片？
{% endnote %}
{% note orange 'fas fa-battery-half' modern %}
你是刷 Visa 还是 UnionPay
{% endnote %}
{% note purple 'far fa-hand-scissors' modern %}
剪刀石头布
{% endnote %}
{% note green 'fab fa-internet-explorer' modern %}
前端最讨厌的浏览器
{% endnote %}
```

{% note 'fab fa-cc-visa' modern %}
你是刷 Visa 还是 UnionPay
{% endnote %}
{% note blue 'fas fa-bullhorn' modern %}
2021年快到了....
{% endnote %}
{% note pink 'fas fa-car-crash' modern %}
小心开车 安全至上
{% endnote %}
{% note red 'fas fa-fan' modern%}
这是三片呢？还是四片？
{% endnote %}
{% note orange 'fas fa-battery-half' modern %}
你是刷 Visa 还是 UnionPay
{% endnote %}
{% note purple 'far fa-hand-scissors' modern %}
剪刀石头布
{% endnote %}
{% note green 'fab fa-internet-explorer' modern %}
前端最讨厌的浏览器
{% endnote %}

> flat

```markdown
{% note 'fab fa-cc-visa' flat %}
你是刷 Visa 还是 UnionPay
{% endnote %}
{% note blue 'fas fa-bullhorn' flat %}
2021年快到了....
{% endnote %}
{% note pink 'fas fa-car-crash' flat %}
小心开车 安全至上
{% endnote %}
{% note red 'fas fa-fan' flat%}
这是三片呢？还是四片？
{% endnote %}
{% note orange 'fas fa-battery-half' flat %}
你是刷 Visa 还是 UnionPay
{% endnote %}
{% note purple 'far fa-hand-scissors' flat %}
剪刀石头布
{% endnote %}
{% note green 'fab fa-internet-explorer' flat %}
前端最讨厌的浏览器
{% endnote %}
```

{% note 'fab fa-cc-visa' flat %}
你是刷 Visa 还是 UnionPay
{% endnote %}
{% note blue 'fas fa-bullhorn' flat %}
2021年快到了....
{% endnote %}
{% note pink 'fas fa-car-crash' flat %}
小心开车 安全至上
{% endnote %}
{% note red 'fas fa-fan' flat%}
这是三片呢？还是四片？
{% endnote %}
{% note orange 'fas fa-battery-half' flat %}
你是刷 Visa 还是 UnionPay
{% endnote %}
{% note purple 'far fa-hand-scissors' flat %}
剪刀石头布
{% endnote %}
{% note green 'fab fa-internet-explorer' flat %}
前端最讨厌的浏览器
{% endnote %}

> disabled

```markdown
{% note 'fab fa-cc-visa' disabled %}
你是刷 Visa 还是 UnionPay
{% endnote %}
{% note blue 'fas fa-bullhorn' disabled %}
2021年快到了....
{% endnote %}
{% note pink 'fas fa-car-crash' disabled %}
小心开车 安全至上
{% endnote %}
{% note red 'fas fa-fan' disabled %}
这是三片呢？还是四片？
{% endnote %}
{% note orange 'fas fa-battery-half' disabled %}
你是刷 Visa 还是 UnionPay
{% endnote %}
{% note purple 'far fa-hand-scissors' disabled %}
剪刀石头布
{% endnote %}
{% note green 'fab fa-internet-explorer' disabled %}
前端最讨厌的浏览器
{% endnote %}
```

{% note 'fab fa-cc-visa' disabled %}
你是刷 Visa 还是 UnionPay
{% endnote %}
{% note blue 'fas fa-bullhorn' disabled %}
2021年快到了....
{% endnote %}
{% note pink 'fas fa-car-crash' disabled %}
小心开车 安全至上
{% endnote %}
{% note red 'fas fa-fan' disabled %}
这是三片呢？还是四片？
{% endnote %}
{% note orange 'fas fa-battery-half' disabled %}
你是刷 Visa 还是 UnionPay
{% endnote %}
{% note purple 'far fa-hand-scissors' disabled %}
剪刀石头布
{% endnote %}
{% note green 'fab fa-internet-explorer' disabled %}
前端最讨厌的浏览器
{% endnote %}

> no-icon

```markdown
{% note no-icon %}
你是刷 Visa 还是 UnionPay
{% endnote %}
{% note blue no-icon %}
2021年快到了....
{% endnote %}
{% note pink no-icon %}
小心开车 安全至上
{% endnote %}
{% note red no-icon %}
这是三片呢？还是四片？
{% endnote %}
{% note orange no-icon %}
你是刷 Visa 还是 UnionPay
{% endnote %}
{% note purple no-icon %}
剪刀石头布
{% endnote %}
{% note green no-icon %}
前端最讨厌的浏览器
{% endnote %}
```

{% note no-icon %}
你是刷 Visa 还是 UnionPay
{% endnote %}
{% note blue no-icon %}
2021年快到了....
{% endnote %}
{% note pink no-icon %}
小心开车 安全至上
{% endnote %}
{% note red no-icon %}
这是三片呢？还是四片？
{% endnote %}
{% note orange no-icon %}
你是刷 Visa 还是 UnionPay
{% endnote %}
{% note purple no-icon %}
剪刀石头布
{% endnote %}
{% note green no-icon %}
前端最讨厌的浏览器
{% endnote %}

<!-- endtab -->
{% endtabs %}

### Gallery相册图库

> 2.2.0以上提供

一个图库集合。写法如下

```plaintext
<div class="gallery-group-main">
{% galleryGroup name description link img-url %}
{% galleryGroup name description link img-url %}
{% galleryGroup name description link img-url %}
</div>
```

  - name：图库名字
  - description：图库描述
  - link：连接到对应相册的地址
  - img-url：图库封面的地址

例如：

```
<div class="gallery-group-main">
{% galleryGroup '壁纸' '收藏的一些壁纸' '/Gallery/wallpaper' https://i.loli.net/2019/11/10/T7Mu8Aod3egmC4Q.png %}
{% galleryGroup '漫威' '关于漫威的图片' '/Gallery/marvel' https://i.loli.net/2019/12/25/8t97aVlp4hgyBGu.jpg %}
{% galleryGroup 'OH MY GIRL' '关于OH MY GIRL的图片' '/Gallery/ohmygirl' https://i.loli.net/2019/12/25/hOqbQ3BIwa6KWpo.jpg %}
</div>
```

<div class="gallery-group-main">
{% galleryGroup '壁纸' '收藏的一些壁纸' '/Gallery/wallpaper' https://i.loli.net/2019/11/10/T7Mu8Aod3egmC4Q.png %}
{% galleryGroup '漫威' '关于漫威的图片' '/Gallery/marvel' https://i.loli.net/2019/12/25/8t97aVlp4hgyBGu.jpg %}
{% galleryGroup 'OH MY GIRL' '关于OH MY GIRL的图片' '/Gallery/ohmygirl' https://i.loli.net/2019/12/25/hOqbQ3BIwa6KWpo.jpg %}
</div>

### Gallery相册

> 2.0.0 以上提供

区别于旧版的Gallery相册,新的 Gallery 相册会自动根据图片长度进行排版，书写也更加方便，与 markdown 格式一样。可根据需要插入到相应的 md。

{% tabs test1 %}
<!-- tab 本地 -->
写法：

```markdown
{% gallery [lazyload],[rowHeight],[limit] %}
markdown 图片格式
{% endgallery %}
```

| 参数      | 解释                                                                               |
| --------- | ---------------------------------------------------------------------------------- |
| lazyload  | 【可选】点击按钮加载更多图片，填写 true/false。默认为 false。                      |
| rowHeight | 【可选】图片显示的高度，如果需要一行显示更多的图片，可设置更小的数字。默认为 220。 |
| limit     | 【可选】每次加载多少张照片。默认为 10。                                            |

> 示例

```markdown
{% gallery %}
markdown 图片格式
{% endgallery %}

{% gallery true,220,10 %}
markdown 图片格式
{% endgallery %}

{% gallery true,,10 %}
markdown 图片格式
{% endgallery %}
```

例如：

```
{% gallery %}
![](https://i.loli.net/2019/12/25/Fze9jchtnyJXMHN.jpg)
![](https://i.loli.net/2019/12/25/ryLVePaqkYm4TEK.jpg)
![](https://i.loli.net/2019/12/25/gEy5Zc1Ai6VuO4N.jpg)
![](https://i.loli.net/2019/12/25/d6QHbytlSYO4FBG.jpg)
![](https://i.loli.net/2019/12/25/6nepIJ1xTgufatZ.jpg)
![](https://i.loli.net/2019/12/25/E7Jvr4eIPwUNmzq.jpg)
![](https://i.loli.net/2019/12/25/mh19anwBSWIkGlH.jpg)
![](https://i.loli.net/2019/12/25/2tu9JC8ewpBFagv.jpg)
{% endgallery %}
```

{% gallery %}
![](https://i.loli.net/2019/12/25/Fze9jchtnyJXMHN.jpg)
![](https://i.loli.net/2019/12/25/ryLVePaqkYm4TEK.jpg)
![](https://i.loli.net/2019/12/25/gEy5Zc1Ai6VuO4N.jpg)
![](https://i.loli.net/2019/12/25/d6QHbytlSYO4FBG.jpg)
![](https://i.loli.net/2019/12/25/6nepIJ1xTgufatZ.jpg)
![](https://i.loli.net/2019/12/25/E7Jvr4eIPwUNmzq.jpg)
![](https://i.loli.net/2019/12/25/mh19anwBSWIkGlH.jpg)
![](https://i.loli.net/2019/12/25/2tu9JC8ewpBFagv.jpg)
{% endgallery %}

<!-- endtab -->

<!-- tab 远程拉取 -->

写法：

```markdown
{% gallery url,[link],[lazyload],[rowHeight],[limit] %}
{% endgallery %}
```

| 参数      | 解释                                                                                 |
| --------- | ------------------------------------------------------------------------------------ |
| url       | 【必须】 识别词                                                                      |
| link      | 【必须】远程的 json 链接                                                             |
| lazyload  | 【可选】点击按钮加载更多图片，填写 true/false。默认为 `false`。                      |
| rowHeight | 【可选】图片显示的高度，如果需要一行显示更多的图片，可设置更小的数字。默认为 `220`。 |
| limit     | 【可选】每次加载多少张照片。默认为 `10`。                                            |

> 远程链接 Json 的例子

有三个参数，`url`是必须存在的，`alt `和 `title `可有，也可没有。

```json
[
  {
    "url": "https://cdn.jsdelivr.net/gh/jerryc127/CDN/img/IMG_0556.jpg",
    "alt": "IMG_0556.jpg",
     "title": "这是title"
  },
  {
    "url": "https://cdn.jsdelivr.net/gh/jerryc127/CDN/img/IMG_0472.jpg",
    "alt": "IMG_0472.jpg"
  },
  {
    "url": "https://cdn.jsdelivr.net/gh/jerryc127/CDN/img/IMG_0453.jpg",
    "alt": ""
  },
  {
    "url": "https://cdn.jsdelivr.net/gh/jerryc127/CDN/img/IMG_0931.jpg",
    "alt": ""
  }
]
```

> 示例

```markdown
{% gallery url,https://xxxx.com/sss.json %}
{% endgallery %}

{% gallery url,https://xxxx.com/sss.json,true,220,10 %}
{% endgallery %}

{% gallery url,https://xxxx.com/sss.json,true,,10 %}
{% endgallery %}
```

<!-- endtab -->

{% endtabs %}

### tag-hide - 隐藏标签

{% note warning simple %}

2.2.0以上提供

请注意，tag-hide内的标签外挂content内都不建议有h1 - h6 等标题。因为Toc会把隐藏内容标题也显示出来，而且当滚动屏幕时，如果隐藏内容没有显示出来，会导致Toc的滚动出现异常。

{% endnote %}

如果你想把一些文字、内容隐藏起来，并提供按钮让用户点击显示。可以使用这个标签外挂。

{% tabs test1 %}
<!-- tab Inline -->
`inline `在文本里面添加按钮隐藏内容，只限文字

( content不能包含英文逗号，可用 `&sbquo;`)

```markdown
{% hideInline content,display,bg,color %}
```

- content: 文本内容

- display: 按钮显示的文字(可选)

- bg: 按钮的背景颜色(可选)

- color: 按钮文字的颜色(可选)

> Demo

```markdown
哪个英文字母最酷？ {% hideInline 因为西装裤(C装酷),查看答案,#FF7242,#fff %}

门里站着一个人? {% hideInline 闪 %}
```

哪个英文字母最酷？ {% hideInline 因为西装裤(C装酷),查看答案,#FF7242,#fff %}

门里站着一个人? {% hideInline 闪 %}

<!-- endtab -->

<!-- tab Block -->

`block`独立的block隐藏内容，可以隐藏很多内容，包括图片，代码块等等

( display 不能包含英文逗号，可用`&sbquo;`)

```markdown
{% hideBlock display,bg,color %}
content
{% endhideBlock %}
```

- content: 文本内容
- display: 按钮显示的文字(可选)
- bg: 按钮的背景颜色(可选)
- color: 按钮文字的颜色(可选)

> demo

```plaintext
查看答案
{% hideBlock 查看答案 %}
傻子，怎么可能有答案
{% endhideBlock %}
```

查看答案
{% hideBlock 查看答案 %}
傻子，怎么可能有答案
{% endhideBlock %}

<!-- endtab -->

<!-- tab Toggle -->

> 2.3.0以上支持 

如果你需要展示的内容太多，可以把它隐藏在收缩框里，需要时再把它展开。

( display 不能包含英文逗号，可用`&sbquo;`)

```markdown
{% hideToggle display,bg,color %}
content
{% endhideToggle %}
```

> Demo

```markdown
{% hideToggle Butterfly安装方法 %}
在你的博客根目录里

git clone -b master https://github.com/jerryc127/hexo-theme-butterfly.git themes/Butterfly

如果想要安装比较新的dev分支，可以

git clone -b dev https://github.com/jerryc127/hexo-theme-butterfly.git themes/Butterfly

{% endhideToggle %}
```

{% hideToggle Butterfly安装方法 %}
在你的博客根目录里

git clone -b master https://github.com/jerryc127/hexo-theme-butterfly.git themes/Butterfly

如果想要安装比较新的dev分支，可以

git clone -b dev https://github.com/jerryc127/hexo-theme-butterfly.git themes/Butterfly

{% endhideToggle %}

<!-- endtab -->
{% endtabs %}

### mermaid - 绘图标签

使用mermaid标签可以绘制Flowchart（流程图）、Sequence diagram（时序图 ）、Class Diagram（类别图）、State Diagram（状态图）、Gantt（甘特图）和Pie Chart（圆形图），具体可以查看[mermaid文档](https://mermaid-js.github.io/mermaid/#/)

修改 `主题配置文件`

```yaml
# mermaid
# see https://github.com/mermaid-js/mermaid
mermaid:
  enable: true
  # built-in themes: default/forest/dark/neutral
  theme:
    light: default
    dark: dark
```

写法：

```markdown
{% mermaid %}
内容
{% endmermaid %}
```

例如：

```markdown
{% mermaid %}
pie
    title Key elements in Product X
    "Calcium" : 42.96
    "Potassium" : 50.05
    "Magnesium" : 10.01
    "Iron" :  5
{% endmermaid %}
```

{% mermaid %}
pie
    title Key elements in Product X
    "Calcium" : 42.96
    "Potassium" : 50.05
    "Magnesium" : 10.01
    "Iron" :  5
{% endmermaid %}

### Tabs - 标签

移植于next主题

使用方法

```markdown
{% tabs Unique name, [index] %}
<!-- tab [Tab caption] [@icon] -->
Any content (support inline tags too).
<!-- endtab -->
{% endtabs %}

Unique name   : Unique name of tabs block tag without comma.
                Will be used in #id's as prefix for each tab with their index numbers.
                If there are whitespaces in name, for generate #id all whitespaces will replaced by dashes.
                Only for current url of post/page must be unique!
[index]       : Index number of active tab.
                If not specified, first tab (1) will be selected.
                If index is -1, no tab will be selected. It's will be something like spoiler.
                Optional parameter.
[Tab caption] : Caption of current tab.
                If not caption specified, unique name with tab index suffix will be used as caption of tab.
                If not caption specified, but specified icon, caption will empty.
                Optional parameter.
[@icon]       : FontAwesome icon name (full-name, look like 'fas fa-font')
                Can be specified with or without space; e.g. 'Tab caption @icon' similar to 'Tab caption@icon'.
                Optional parameter.
```

> Demo 1 - 预设选择第一个【默认】

```markdown
{% tabs test1 %}
<!-- tab -->
**This is Tab 1.**
<!-- endtab -->

<!-- tab -->
**This is Tab 2.**
<!-- endtab -->

<!-- tab -->
**This is Tab 3.**
<!-- endtab -->
{% endtabs %}
```

{% tabs test1 %}
<!-- tab -->
**This is Tab 1.**
<!-- endtab -->

<!-- tab -->
**This is Tab 2.**
<!-- endtab -->

<!-- tab -->
**This is Tab 3.**
<!-- endtab -->
{% endtabs %}

> Demo 2 - 预设选择tabs

```markdown
{% tabs test2, 3 %}
<!-- tab -->
**This is Tab 1.**
<!-- endtab -->

<!-- tab -->
**This is Tab 2.**
<!-- endtab -->

<!-- tab -->
**This is Tab 3.**
<!-- endtab -->
{% endtabs %}
```

{% tabs test2, 3 %}
<!-- tab -->
**This is Tab 1.**
<!-- endtab -->

<!-- tab -->
**This is Tab 2.**
<!-- endtab -->

<!-- tab -->
**This is Tab 3.**
<!-- endtab -->
{% endtabs %}

> Demo 3 - 没有预设值

```markdown
{% tabs test3, -1 %}
<!-- tab -->
**This is Tab 1.**
<!-- endtab -->

<!-- tab -->
**This is Tab 2.**
<!-- endtab -->

<!-- tab -->
**This is Tab 3.**
<!-- endtab -->
{% endtabs %}
```

{% tabs test3, -1 %}
<!-- tab -->
**This is Tab 1.**
<!-- endtab -->

<!-- tab -->
**This is Tab 2.**
<!-- endtab -->

<!-- tab -->
**This is Tab 3.**
<!-- endtab -->
{% endtabs %}

> Demo 4 - 自定义Tab名 + 只有icon + icon和Tab名

```markdown
{% tabs test4 %}
<!-- tab 第一个Tab -->
**tab名字为第一个Tab**
<!-- endtab -->

<!-- tab @fab fa-apple-pay -->
**只有图标 没有Tab名字**
<!-- endtab -->

<!-- tab 炸弹@fas fa-bomb -->
**名字+icon**
<!-- endtab -->
{% endtabs %}
```

{% tabs test4 %}
<!-- tab 第一个Tab -->
**tab名字为第一个Tab**
<!-- endtab -->

<!-- tab @fab fa-apple-pay -->
**只有图标 没有Tab名字**
<!-- endtab -->

<!-- tab 炸弹@fas fa-bomb -->
**名字+icon**
<!-- endtab -->
{% endtabs %}

### Button - 按钮

> 3.0以上适用

使用方法：

```markdown
{% btn [url],[text],[icon],[color] [style] [layout] [position] [size] %}

[url]         : 链接
[text]        : 按钮文字
[icon]        : [可选] 图标
[color]       : [可选] 按钮背景顔色(默认style时）
                      按钮字体和边框顔色(outline时)                      default/blue/pink/red/purple/orange/green
[style]       : [可选] 按钮样式 默认实心
                      outline/留空
[layout]      : [可选] 按钮佈局 默认为line
                      block/留空
[position]    : [可选] 按钮位置 前提是设置了layout为block 默认为左边
                      center/right/留空
[size]        : [可选] 按钮大小
                      larger/留空
```

> Demo

```markdown
This is my website, click the button {% btn 'https://butterfly.js.org/',Butterfly %}
This is my website, click the button {% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right %}
This is my website, click the button {% btn 'https://butterfly.js.org/',Butterfly,,outline %}
This is my website, click the button {% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,outline %}
This is my website, click the button {% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,larger %}
```

This is my website, click the button {% btn 'https://butterfly.js.org/',Butterfly %}
This is my website, click the button {% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right %}
This is my website, click the button {% btn 'https://butterfly.js.org/',Butterfly,,outline %}
This is my website, click the button {% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,outline %}
This is my website, click the button {% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,larger %}

```markdown
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,block %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,block center larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,block right outline larger %}
```

{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,block %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,block center larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,block right outline larger %}

more than one button in center

```markdown
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,blue larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,pink larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,red larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,purple larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,orange larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,green larger %}
```

{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,blue larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,pink larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,red larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,purple larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,orange larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,green larger %}

```markdown
<div class="btn-center">
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,outline larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,outline blue larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,outline pink larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,outline red larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,outline purple larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,outline orange larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,outline green larger %}
</div>
```

<div class="btn-center">
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,outline larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,outline blue larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,outline pink larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,outline red larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,outline purple larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,outline orange larger %}
{% btn 'https://butterfly.js.org/',Butterfly,far fa-hand-point-right,outline green larger %
</div>

### inlineImg - 行内图片

主题中的图片都是默认以`块级元素`显示，如果你想以`内联元素`显示，可以使用这个标签外挂。

```markdown
{% inlineImg [src] [height] %}

[src]      :    图片链接
[height]   ：   图片高度限制【可选】
```

> Demo

```markdown
你看我长得漂亮不

![](https://i.loli.net/2021/03/19/2P6ivUGsdaEXSFI.png)

我觉得很漂亮 {% inlineImg https://i.loli.net/2021/03/19/5M4jUB3ynq7ePgw.png 150px %}
```

你看我长得漂亮不

![](https://i.loli.net/2021/03/19/2P6ivUGsdaEXSFI.png)

我觉得很漂亮 {% inlineImg https://i.loli.net/2021/03/19/5M4jUB3ynq7ePgw.png 150px %}

### label - 标签

{% note warning simple %}
由于 hexo 的渲染限制， 在段落开头使用 label 标签外挂会出现一些问题。例如：连续开头使用 label 标签外挂的段落无法换行

建议 **不要** 在段落开头使用 label 标签外挂
{% endnote %}

高亮所需的文字

```markdown
{% label text color %}
```

| 参数  | 解释                                                                             |
| ----- | -------------------------------------------------------------------------------- |
| text  | 文字                                                                             |
| color | 【可选】背景颜色，默认为 `default`<br/>default/blue/pink/red/purple/orange/green |

> Demo

```markdown
臣亮言：{% label 先帝 %}创业未半，而{% label 中道崩殂 blue %}。今天下三分，{% label 益州疲敝 pink %}，此诚{% label 危急存亡之秋 red %}也！然侍衞之臣，不懈于内；{% label 忠志之士 purple %}，忘身于外者，盖追先帝之殊遇，欲报之于陛下也。诚宜开张圣听，以光先帝遗德，恢弘志士之气；不宜妄自菲薄，引喻失义，以塞忠谏之路也。
宫中、府中，俱为一体；陟罚臧否，不宜异同。若有{% label 作奸 orange %}、{% label 犯科 green %}，及为忠善者，宜付有司，论其刑赏，以昭陛下平明之治；不宜偏私，使内外异法也。
```

臣亮言：{% label 先帝 %}创业未半，而{% label 中道崩殂 blue %}。今天下三分，{% label 益州疲敝 pink %}，此诚{% label 危急存亡之秋 red %}也！然侍衞之臣，不懈于内；{% label 忠志之士 purple %}，忘身于外者，盖追先帝之殊遇，欲报之于陛下也。诚宜开张圣听，以光先帝遗德，恢弘志士之气；不宜妄自菲薄，引喻失义，以塞忠谏之路也。
宫中、府中，俱为一体；陟罚臧否，不宜异同。若有{% label 作奸 orange %}、{% label 犯科 green %}，及为忠善者，宜付有司，论其刑赏，以昭陛下平明之治；不宜偏私，使内外异法也。

### timeline - 时间线

> 4.0.0 以上支持

```markdown
{% timeline title,color %}
<!-- timeline title -->
xxxxx
<!-- endtimeline -->
<!-- timeline title -->
xxxxx
<!-- endtimeline -->
{% endtimeline %}
```

| 参数  | 解释                                                                          |
| ----- | ----------------------------------------------------------------------------- |
| title | 标题/时间线                                                                   |
| color | timeline 颜色<br/>default(留空) / blue / pink / red / purple / orange / green |

> Demo

```markdown
{% timeline 2022 %}
<!-- timeline 01-02 -->
这是测试页面
<!-- endtimeline -->
{% endtimeline %}
```

{% timeline 2022 %}
<!-- timeline 01-02 -->
这是测试页面
<!-- endtimeline -->
{% endtimeline %}

```markdown
{% timeline 2022,blue %}
<!-- timeline 01-02 -->
这是测试页面
<!-- endtimeline -->
{% endtimeline %}
```

{% timeline 2022,blue %}
<!-- timeline 01-02 -->
这是测试页面
<!-- endtimeline -->
{% endtimeline %}

```markdown
{% timeline 2022,pink %}
<!-- timeline 01-02 -->
这是测试页面
<!-- endtimeline -->
{% endtimeline %}
```

{% timeline 2022,pink %}
<!-- timeline 01-02 -->
这是测试页面
<!-- endtimeline -->
{% endtimeline %}

### flink - 友情链接

> 4.1.0 支持

可在任何界面插入类似友情链接列表效果

内容格式与友情链接界面一样，支持 yml 格式

```markdown
{% flink %}
xxxxxx
{% endflink %}
```

> Demo

```markdown
{% flink %}
- class_name: 友情链接
  class_desc: 那些人，那些事
  link_list:
    - name: JerryC
      link: https://jerryc.me/
      avatar: https://jerryc.me/img/avatar.png
      descr: 今日事,今日毕
    - name: Hexo
      link: https://hexo.io/zh-tw/
      avatar: https://d33wubrfki0l68.cloudfront.net/6657ba50e702d84afb32fe846bed54fba1a77add/827ae/logo.svg
      descr: 快速、简单且强大的网志框架

- class_name: 网站
  class_desc: 值得推荐的网站
  link_list:
    - name: Youtube
      link: https://www.youtube.com/
      avatar: https://i.loli.net/2020/05/14/9ZkGg8v3azHJfM1.png
      descr: 视频网站
    - name: Weibo
      link: https://www.weibo.com/
      avatar: https://i.loli.net/2020/05/14/TLJBum386vcnI1P.png
      descr: 中国最大社交分享平台
    - name: Twitter
      link: https://twitter.com/
      avatar: https://i.loli.net/2020/05/14/5VyHPQqR6LWF39a.png
      descr: 社交分享平台
{% endflink %}
```

{% flink %}
- class_name: 友情链接
  class_desc: 那些人，那些事
  link_list:
    - name: JerryC
      link: https://jerryc.me/
      avatar: https://jerryc.me/img/avatar.png
      descr: 今日事,今日毕
    - name: Hexo
      link: https://hexo.io/zh-tw/
      avatar: https://d33wubrfki0l68.cloudfront.net/6657ba50e702d84afb32fe846bed54fba1a77add/827ae/logo.svg
      descr: 快速、简单且强大的网志框架

- class_name: 网站
  class_desc: 值得推荐的网站
  link_list:
    - name: Youtube
      link: https://www.youtube.com/
      avatar: https://i.loli.net/2020/05/14/9ZkGg8v3azHJfM1.png
      descr: 视频网站
    - name: Weibo
      link: https://www.weibo.com/
      avatar: https://i.loli.net/2020/05/14/TLJBum386vcnI1P.png
      descr: 中国最大社交分享平台
    - name: Twitter
      link: https://twitter.com/
      avatar: https://i.loli.net/2020/05/14/5VyHPQqR6LWF39a.png
      descr: 社交分享平台
{% endflink %}
  
  
