---
title: theme-hexo blog 博客的部署方式
date: 2024-04-16 02:01:33
updated:
tags: theme
categories:
keywords:
description:
top_img:
comments:
cover: /img/default_cover-3.png
toc:
toc_number:
toc_style_simple:
copyright:
copyright_author:
copyright_author_href:
copyright_url:
copyright_info:
mathjax:
katex:
aplayer:
highlight_shrink:
aside:
abcjs:
---

> todo-准备用这个文档来记录一下github page和gitee page的部署事项，目前github page已经成功绑定了自己的域名http://dreamstar.top/，也算是一个小小的进步吧。

# hexo blog的部署方式

hexo作为一种便捷的博客系统搭建方式，可以使用多种主题快速的搭建个人博客，满足文章的存档和个性化需求。这个文档的主要内容是来描述hexo blog 的部署方式，目前已包含了gitee和github的部署方式，并且已经可以实现和域名的绑定。

## 通用内容

要想建立一个属于自己的博客，首先需要先建立一个项目（并可使用git进行管理），在master的基础上，先建立一个dev分支（或者其他任意名字）来储存hexo的主要文件和主题信息，在本地建立hexo项目的详细的内容见[hexo的基本用法](../0.hello-world)。

在完成本地hexo项目之后，需要在hexo目录下的`_config.yml`文件中完成对`\# URL`和`\# Deployment`部分的设置。

`\# URL`：用于定义网页中默认的url和分支情况，主要需要注意以下两个参数。

- url: http://dreamstar.top/  - - 这个参数是你网页访问的网址，如果绑定了域名，就需要直接使用域名的链接。如果没有，就要使用https://mengfan1993.gitee.io/这样的链接，实测gitee中好像不受这个参数的影响。

- root:  - - 这个参数反映了你项目中分支的名称当你使用了默认值之外的分支名称时，就需要填写这个内容。

  > gitee：默认为pages
  >
  > github：默认为gh-pages

> 注意：github中必须严格的按照上述的规则进行填写，否则主题可能无法进行应用，可以在.deploy_git和生成的public中的index.html中看到这两个参数对静态页面产生的影响。

`\# Deployment`是用于填写一键部署上传方式和对应仓库位置的地方。以下是gitee和github同时部署的填写方式：

```
# Deployment
## Docs: https://hexo.io/docs/one-command-deployment
deploy:
  - type: git
    repo: git@gitee.com:gitee的账户名称/gitee的账户名称.git
    branch: pages
  - type: git
    repo: git@github.com:github的账户名称/github的账户名称.github.io.git
    branch: gh-pages
```

> 直接使用github的账户名称.github.io和gitee的账户名称来分别作为github和gitee的仓库名是最简单快捷的搭建方式，这样可以直接使用一级域名，便于后期域名的绑定

### 一键部署插件

Hexo中提供的一键部署方式是最简单且快捷的方式，使用方式如下：

1. 首先需要完成[hexo-deployer-git](https://github.com/hexojs/hexo-deployer-git)插件的安装。

```cmd
$ npm install hexo-deployer-git --save
```

2. 随后修改配置

```
deploy:
  type: git
  repo: <repository url> #https://bitbucket.org/JohnSmith/johnsmith.bitbucket.io
  branch: [branch]
  message: [message]
```

| 参数      | 描述                                                         | 默认                                                         |
| --------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| `repo`    | 库（Repository）地址                                         |                                                              |
| `branch`  | 分支名称                                                     | `gh-pages` (GitHub) `coding-pages` (Coding.net) `master` (others) |
| `message` | 自定义提交信息                                               | `Site updated: {{ now('YYYY-MM-DD HH:mm:ss') }}`)            |
| `token`   | 可选的令牌值，用于认证 repo。用 `$` 作为前缀从而从环境变量中读取令牌 |                                                              |

3. 生成站点文件并推送至远程库。执行 `hexo clean && hexo deploy`。

- 除非你使用令牌或 SSH 密钥认证，否则你会被提示提供目标仓库的用户名和密码。
- hexo-deployer-git 并不会存储你的用户名和密码. 请使用 [git-credential-cache](https://git-scm.com/docs/git-credential-cache) 来临时存储它们。

4. 登入 Github(或gitee、BitBucket、Gitlab)，请在库设置（Repository Settings）中将默认分支设置为`_config.yml`配置中的分支名称。稍等片刻，您的站点就会显示在您的Github Pages中。

> 当执行 `hexo deploy` 时，Hexo 会将 `public` 目录中的文件和目录推送至 `_config.yml` 中指定的远端仓库和分支中，并且**完全覆盖**该分支下的已有内容。
>
> 由于 Hexo 的部署默认使用分支 `master`，所以如果你同时正在使用 Git 管理你的站点目录，你应当注意你的部署分支应当不同于写作分支。
> 一个好的实践是将站点目录和 Pages 分别存放在两个不同的 Git 仓库中，可以有效避免相互覆盖。
> Hexo 在部署你的站点生成的文件时并不会更新你的站点目录。因此你应该手动提交并推送你的写作分支。

此外，如果您的 Github Pages 需要使用 CNAME 文件**自定义域名**，请将 CNAME 文件置于 `source` 目录下，只有这样 `hexo deploy` 才能将 CNAME 文件一并推送至部署分支。

## gitee部署hexo

gitee中使用Gitee Pages服务进行个人网页的部署，==需要首先进行个人身份认证==，随后就可以在某一个仓库中进行搭建。

如果遵循了通用内容的填写方式，gitee中的静态网页分支名称应该为pages，因此在`部署分支`的选项中，选择`pages`，并勾选强制使用HTTPS（==反正我勾了==）然后更新即可等到自己默认的博客地址，如 https://mengfan1993.gitee.io。

## github中部署hexo

github中项目`setting`的`Pages`中，首先在`Source`选项中，选择`Deploy from a branch`，随后在下面的分支选项中选择对应的`gh-pages`分支，`Save`即可完成对page的搭建。

## github中绑定域名/自定义域名

github中项目`setting`的`Pages`中，`Custom domain`负责验证并绑定对应的域名。因此，需要一个自己的域名。

以下以腾讯云作为示例：

1. 首先，需要购买一个属于自己的域名。（假装有配图）

2. 其次，点开域名管理界面中的`域名解析`。（这里不需要图）
3. 最后，选择`快速添加解析`，`域名映射`、填写github上默认的域名，如arrayofstar.github.io.

![image-20240416223042806](./images/theme-github-deploy/image-add_CNAME.png)

> 博客中的图片问题看来还是需要建立一个图床才行。

