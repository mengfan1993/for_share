# SPWLA_group-自动机器学习

> 作者：CSY、Dreamstar		校对：Dreamstar
>
>  参考链接：https://pycaret.gitbook.io/docs/

本教程涵盖了整个AutoML过程，包含数据获取、预处理、模型训练、超参数调优、模型预测、保存与部署，以备未来随时使用与查看。

以下命令是PyCaret中主流程的函数，这些命令是自然构造的，并且非常直观易记，例如：

```python
setup()  # 初始化
create_model()  # 创建模型
compare_models()  # 比较模型
tune_model()  # 微调模型
plot_model()  # 绘图
evaluate_model()  # 评估模型
predict_model()  # 模型预测
```

PyCaret的全局流程如下：

**Setup** ➡️ **Compare Models** ➡️ **Analyze Model** ➡️ **Prediction** ➡️ **Save Model**

![PyCaret-workflow](images/PyCaret/PyCaret-workflow.jpg)

## Pycaret简介

Pycaret是一个用于快速原型设计和自动化机器学习流程的Python开源库，旨在简化机器学习任务的常见工作流程，如数据预处理、特征工程、模型选择、调参和模型部署。

与其他开源机器学习库相比，PyCaret是一个替代的低代码库，可以用几行代码替换数百行代码。这使得实验速度和效率呈指数级增长。PyCaret本质上是围绕几个机器学习库和框架的Python包装器，如scikit-learn、XGBoost、LightGBM、CatBoost、spaCy、Optuna、Hyperopt、Ray等。

以下是Pycaret的一些主要功能和特点：

1. 自动化：Pycaret提供了一套自动化工具，可以自动处理数据预处理、特征选择、特征工程和模型选择等任务。它根据数据的类型自动选择合适的预处理方法，并提供了一系列自动化算法来选择最佳模型。
2. 多模型比较：Pycaret支持比较多个机器学习模型的性能。它为用户提供了一个简单的接口，可以快速训练和比较多个模型，并生成模型性能指标的可视化图表。
3. 模型调参：Pycaret提供了一套用于模型调参的工具。它可以自动搜索参数空间，并使用交叉验证方法选择最佳的参数组合。
4. 模型解释性：Pycaret提供了一些用于解释模型结果的工具，如特征重要性图表和SHAP值分析。这些工具可以帮助用户理解模型的预测能力和特征的影响程度。
5. 模型部署：Pycaret支持将训练好的模型导出为独立的Python代码，以便在生产环境中进行部署和集成。

总之，Pycaret是一个强大的机器学习库，提供了许多方便而强大的工具，可以帮助数据科学家和机器学习工程师快速构建和部署机器学习模型。无论是初学者还是经验丰富的专业人士，都可以通过使用Pycaret来加速机器学习任务的开发和迭代过程。

> 相关库的文件和其他相关资料：[https://pycaret.org/](https://link.zhihu.com/?target=https%3A//pycaret.org/) 。
>
> 需要安装pycaret的话，请运行以下命令：`!pip install pycaret`。

## Pycaret安装

PyCaret的默认安装不会自动安装所有可选的依赖项。根据使用情况，您可能对一个或多个附加功能感兴趣：

```
# install simple version
pip install pycaret`

# install analysis extras
pip install pycaret[analysis]

# models extras
pip install pycaret[models]

# install tuner extras
pip install pycaret[tuner]

# install mlops extras
pip install pycaret[mlops]

# install parallel extras
pip install pycaret[parallel]

# install test extras
pip install pycaret[test]

# install multiple extras together
pip install pycaret[analysis,models]

# install full version
pip install pycaret[full]
```

**对于google colab用户**：如果需要在google colab中运行此笔记本，请在笔记本顶部运行以下代码以显示交互式图像。

```text
from pycaret.utils import enable_colab
enable_colab()
```

## PyCaret模块划分

Pycaret模块根据我们要执行的任务进行划分，这些模块代表了不种类型的学习任务(有监督的或无监督的)。

**有监督ML(Supervised ML)**

- 分类(Classification)

PyCaret的分类模块是一个有监督的机器学习模块，用于将元素分组。

目标是预测离散和无序的分类类标签。一些常见的用例包括预测客户违约（是或否）、预测客户流失（客户将离开或留下）、发现的疾病（阳性或阴性）。

此模块可用于二分类或多分类问题。

- 回归(Regression)

PyCaret的回归模块是一个有监督的机器学习模块，用于估计因变量（通常被称为“结果变量”或“目标”）和一个或多个自变量（通常被称作“特征”、“预测因子”或“协变量”）之间的关系。

回归的目的是预测连续值，如预测销售额、预测数量、预测温度等。

**无监督ML(Unsupervised ML)**

- 聚类(Clustering)

PyCaret的聚类模块是一个无监督的机器学习模块，它执行对一组对象进行分组的任务，使同一组（也称为聚类）中的对象彼此之间比其他组中的对象更相似。

- 异常检测(Anomaly Detection)

PyCaret的异常检测模块用于识别数据中不符合正常模式的数据点(如罕见项、事件或观察结果)。通常，异常项目会转化为某种问题，如银行欺诈、结构缺陷、医疗诊断。

**时间序列(Time Series)**

PyCaret时间序列模块是使用机器学习和经典统计技术分析和预测时间序列数据的强大工具。该模块支持多种预测方法，如ARIMA、Prophet和LSTM。它还提供了各种功能来处理缺失值、时间序列分解和数据可视化。

![PyCaret-Cheat_sheet](images/PyCaret/PyCaret-Cheat_sheet.jpg)

# PyCaret模块

## 1. 获取数据

![1-1](images/PyCaret/PyCaret-workflow-1.jpg)

获取数据是进行数据分析的第一步，PyCaret可以通过两种途径进行数据的获取。

**(1) 尝试从GitHub“address”中的文件夹读取数据集**

```python
# 查看数据集的索引
from pycaret.datasets import get_data
all_datasets=get_data('index',address='https://gitee.com/IncubatorShokuhou/pycaret/raw/master/datasets/')
```

![image-20230627170416197](images/PyCaret/PyCaret-get_data-1.jpg)

```python
# 下载并查看数据集
bike_data= get_data('bike',address='https://gitee.com/IncubatorShokuhou/pycaret/raw/master/datasets/')  # 第45个
```

![image-20230627174416464](images/PyCaret/PyCaret-get_data-2.jpg)

**(2) 从本地文件夹读取数据集**

```python
# 使用pandas加载数据
import pandas as pd
df = pd.read_csv('aligned_well_01_1_strech_1.csv')
df
```

![image-20230627210945211](images/PyCaret/PyCaret-get_data-3.jpg)

## 2. Initialize-设置PyCaret环境-[setup](https://pycaret.gitbook.io/docs/get-started/functions/initialize#setting-up-environment)

![1-2](images/PyCaret/PyCaret-workflow-2.jpg)

`setup()`函数的作用是：初始化pycaret的实验环境，并创建转换管道，为建模和部署准备数据。

在pycaret中执行任何其他函数之前必须调用`setup()`。它需要两个必需的参数：数据(pandas dataframe-data)和目标(目标列的名称-target)。大部分配置可使用默认值自动完成，但有些参数可以手动设置。例如：

- 默认的分割比是7:3，但是可以用“train_size”来更改，默认0.7；
- K折叠交叉验证，默认设置为10；
- “session_id”是我们经典的“random_state”，即随机种子。

PyCaret 3.0中有两个API。您可以根据自己的喜好选择其中一个。功能和实验结果是一致的。

```python
# Functional API - 函数接口
from pycaret.classification import *
clf1 = setup(data = diabetes, target = 'Class variable', session_id = 123)
```

```python
# OOP API -  面向对象接口
from pycaret.classification import ClassificationExperiment
clf1 = ClassificationExperiment()
clf1.setup(data = diabetes, target = 'Class variable', session_id = 123)
```

设置环境：(以回归为例)

```python
from pycaret.regression import *
```

```python
model_setup = setup(data=df, # 训练数据集
                    target='RHOB_pred', # 预测目标
            ignore_features=['DEPT','RHOB', 'RHOB_dept_pred', 'NPHI_dept_pred', 'RD_dept_pred', 'wellnum','NPHI_pred','RD_pred'],  # 需要忽视的特征
            keep_features=['GR','NPHI','RD'],  # 选择特征
            session_id=42,  # 随机数种子
            train_size=0.8,  # 划分训练集测试集
            normalize=True,  # 标准化，默认为zscore，可以通过normalize_method设置[minmax,maxabs,robust]
            use_gpu=True,
            remove_outliers=True,  # 默认使用隔离森林删除数据中的异常值，可以通过outliers_method来设置删除异常值的方法
            fold=10
            )
```

![image-20230703155202884](images/PyCaret/PyCaret-setup-1.jpg)

以下是针对Pycaret中`setup()`模块输出的解释：

1. Session id（会话ID）：用于标识Pycaret会话的唯一标识符。
2. Target（目标）：指定机器学习任务中的目标变量（预测变量）。在分类问题中，它通常是类别标签；在回归问题中，它通常是连续的数值。
3. Target type（目标类型）：目标变量的类型，可以是分类（classification）或回归（regression）。
4. Original data shape（原始数据形状）：原始数据集的维度信息，指示数据集的行数和列数。
5. Transformed data shape（转换后的数据形状）：经过预处理和转换后的数据集的维度信息。
6. Transformed train set shape（转换后的训练集形状）：经过预处理和转换后的训练集的维度信息。
7. Transformed test set shape（转换后的测试集形状）：经过预处理和转换后的测试集的维度信息。
8. Ignore features（忽略特征）：需要在机器学习任务中忽略的特征（列）的数量。
9. Numeric features（数值特征）：数值型特征（列）的数量，用于指定数据集中的数值型变量。
10. Keep features（保留特征）：需要在机器学习任务中保留的特征（列）的数量。
11. Preprocess（预处理）：指定在训练模型之前进行的数据预处理步骤。
12. Imputation type（缺失值填补类型）：用于处理数据集中缺失值的方法类型。
13. Numeric imputation（数值型特征的填补方式）：用于填补数值型特征中缺失值的方法。
14. Categorical imputation（分类特征的填补方式）：用于填补分类特征中缺失值的方法。
15. Normalize（归一化）：指定是否对数据进行归一化处理。
16. Normalize method（归一化方法）：用于归一化数据的方法。
17. Fold Generator（折叠生成器）：用于生成交叉验证折叠的方法。
18. Fold Number（折叠数量）：指定交叉验证的折叠数量。
19. CPU Jobs（CPU任务数）：指定在Pycaret中执行的CPU任务的数量。
20. Use GPU（使用GPU）：指定是否使用GPU加速进行模型训练和推断。
21. Log Experiment（记录实验）：指定是否记录实验的详细信息和结果。
22. Experiment Name（实验名称）：用于标识实验的名称。
23. USI（用户定义的标识）：用于跟踪和标识特定实验的用户自定义标识符。

### 2.1 数据准备 - 缺失值处理(Missing Values)

由于各种原因，数据集中可能有缺失的值或空记录，通常为空白或`NaN`。大多数机器学习算法无法处理缺失或空白的值。删除缺失值的样本是有时使用的一种基本策略，但这会丢失可能有价值的数据和相关信息或模式。更好的策略是对缺失值进行填补。

**相关参数**

- **imputation_type: string, default = 'simple'**

  要使用的插补类型。它可以是`simple`，也可以是`iterative`。如果是`None`，则不进行缺失值的插补。

- **numeric_imputation: int, float, or string, default = ‘mean’**

  数值数据列的输入策略。当`imputation_type=iterative`时忽略。有以下选择：

  - drop: 丢弃包含缺失值的行
  - mean: 使用当前列的平均值
  - median: 使用当前列的中位数
  - mode: 使用当前列的高频值
  - knn: 使用K近邻的方法进行补充
  - int or float: 使用所提供的值进行补充

- **categorical_imputation: string, default = ‘mode’**

  类别数据列的推测策略。当`imputation_type=iterative`时忽略。有以下选择：

  - drop: 丢弃包含缺失值的行
  - mode: 使用当前列的高频值
  - str: 使用所提供的字符进行补充

- **iterative_imputation_iters: int, default = 5**

  迭代次数。当`imputation_type=simple`时忽略。

- **numeric_iterative_imputer: str or sklearn estimator, default = 'lightgbm'**

  用于对数值数据特征中缺失值进行迭代插补的回归器。如果无，则使用LGB分类器。当`imputation_type=simple`时忽略。'

- **categorical_iterative_imputer: str or sklearn estimator, default = 'lightgbm'**

  用于对分类特征中的缺失值进行迭代插补的回归器。如果无，则使用LGB分类器。当`imputation_type=simple`时忽略。

> 可用的方法包含：et、rf、catboost、lightgbm、knn、simple

**简单估算器与迭代估算器的比较**

![PyCaret-setup-Comparison of Simple imputer vs. Iterative imputer](images/PyCaret/PyCaret-setup-Comparison of Simple imputer vs. Iterative imputer.png)

### 2.2 数据准备 - 数据类型(Data Types)

数据集中的每个特征都有一个相关的数据类型，如数字、分类或日期时间。PyCaret的推理算法自动检测每个特征的数据类型。但是，有时PyCaret推断的数据类型是不正确的。确保数据类型正确非常重要，因为几个下游流程取决于特征的数据类型。一个例子可能是数据集中[缺失值](https://pycaret.gitbook.io/docs/get-started/preprocessing/data-preparation#missing-values)的数字和分类特征的填补方式不同。若要覆盖推断的数据类型，可以在设置函数中使用`numeric_features`、`categorical_features`和`date_features`参数。也可以使用`ignore_features`忽略某些特征以进行模型训练。

**相关参数**

- **numeric_features: list of string, default = None**

  如果推断出的数据类型不正确，则可以使用`numeric_features`来覆盖推断的数据类型。

- **categorical_features: list of string, default = None**

  如果推断出的数据类型不正确，则可以使用`categorical_features`来覆盖推断的数据类型。

- **date_features: list of string, default = None**

  如果数据的`Datetime`列在运行安装程序时未自动推断，则可以使用`date_features`强制数据类型。它可以处理多个日期列。建模中不使用与日期时间相关的功能。相反，在模型训练过程中，执行特征提取并忽略原始`Datetime`列。如果`Datetime`列包含时间戳，则还将提取与时间相关的特征。

- **create_date_columns: list of str, default = ["day", "month", "year"]**

  要根据日期功能创建的列。请注意，已创建的零差异特征（例如，仅包含日期的列中的小时数）将被忽略。允许的值是`pandas.Series.dt`的日期时间属性。特征的日期时间格式是根据第一个非NaN值自动推断的。

- **text_features: list of str, default = None**

  包含文本语料库的列名。如果“无”，则不选择任何文本功能。

- **text_features_method: str, default = 'tf-idf'**

  用于在数据集中嵌入文本功能的方法。在“bow”（Bag of Words - `CountVectorizer`）或“tf-idf”（TfidfVectorizer）之间进行选择。请注意，转换器的稀疏矩阵输出在内部转换为其完整阵列。这可能会导致大型文本嵌入的内存问题。

- **ignore_features: list of string, default = None**

  `ignore_features`可用于在模型训练期间忽略特征。它采用一个字符串列表，其中包含要忽略的列名。

- **keep_features: list of str, default = None**

  `keep_features`参数可以用于在预处理过程中始终保留特定的特征，即任何类型的特征选择都不会丢弃这些特征。它采用一个字符串列表，其中包含要保留的列名。

**示例 - 分类特征(Categorical Features)**

```
 # load dataset
 from pycaret.datasets import get_data
 hepatitis = get_data('hepatitis')
 
 # init setup
 from pycaret.classification import *
 clf1 = setup(data = hepatitis, target = 'Class', categorical_features = ['AGE'])
```

![image-Categorical Features before](images/PyCaret/image-Categorical Features before.png)

![image-Categorical Features-after](images/PyCaret/image-Categorical Features-after.png)

### 2.3 数据准备 - 独热编码(One-Hot Encoding)

数据集中的分类特征包含标签值（序数或标称值），而不是连续数。大多数机器学习算法不能直接处理分类特征，在训练模型之前必须将其转换为数值。最常见的分类编码类型是One-Hot编码（也称为伪编码），其中每个分类级别在包含二进制值（1或0）的数据集中成为一个单独的特征。

由于这是执行ML实验的必要步骤，PyCaret将使用一个独热编码(One-Hot Encoding)来转换数据集中的所有分类特征。这对于具有标称分类数据的特征是理想的，即数据无法排序。但在其他不同的场景中，必须使用其他编码方法。例如，当数据是有序的，即数据具有内在级别时，[Ordinal Encoding](https://pycaret.gitbook.io/docs/get-started/preprocessing/data-preparation#ordinal-encoding)必须使用。One Hot Encoding适用于使用设置函数中的`categorical_features`推断为分类或强制为分类的所有特征。

**相关参数**

- **max_encoding_ohe: int, default = 25** 

  使用OneHotEncoding对唯一值或更少的列进行编码。如果更多，则使用`encoding method`(编码方法)估计器。请注意，只有两个类的列总是按顺序编码的。设置为0以下可始终使用OneHotEncoding。

- **encoding_method: category-encoders estimator, default = None**

  `category-encoders`(类别编码器)估计器，用于对具有超过`max_encoding_ohe`唯一值的分类列进行编码。如果无，则默认使用`category_encoders.leave_one_out.LeaveOneOutEncoder`。

### 2.4 数据准备 - 序数编码(Ordinal Encoding) 

当数据集中的分类特征包含具有内在自然顺序的变量，如*Low*、*Medium*和*High*时，这些变量的编码必须与标称变量不同（如男性或女性，没有内在顺序）。这可以使用`setup`函数中的`ordinal_features`参数来实现，该参数接受一个字典，字典中的功能名称和级别按从低到高的递增顺序排列。

**相关参数**

- **ordinal_features: dictionary, default = None**

  当数据包含有序特征时，必须使用`ordinal_features`对其进行不同的编码。如果数据有一个值为`low`、`medium`、`high`的分类变量，并且已知low<medium<high，则可以将其作为`ordinal_features=｛'column_name'：[‘low’、‘medium’、‘high’]｝`传递。列表序列必须按从低到高的递增顺序排列。

**示例**

```
 # load dataset
 from pycaret.datasets import get_data
 employee = get_data('employee')
 
 # init setup
 from pycaret.classification import *
 clf1 = setup(data = employee, target = 'left', ordinal_features = {'salary' : ['low', 'medium', 'high']})
```

![image-Ordinal Encoding-before](images/PyCaret/image-Ordinal Encoding-before.png)

![image-Ordinal Encoding-after](images/PyCaret/image-Ordinal Encoding-after.png)

### 2.5 数据准备 - 预测目标失衡/类内不平均(Target Imbalance)

当训练数据集的目标类分布不均衡时，可以使用设置中的`fix_imbalance`参数进行修复。当设置为`True`时，SMOTE（合成少数过采样技术）将用作重新采样的默认方法。可以使用设置中的`fix_imbalance_method`更改重新采样的方法。

**相关参数**

- **fix_imbalance: bool, default = False**

  当设置为`True`时，将使用`fix_imbalance_method`中定义的算法对训练数据集进行重新采样。当`None`时，默认情况下使用SMOTE。

- **fix_imbalance_method: str or imblearn estimator, default = 'SMOTE'**

  用于执行类平衡的估计器。从imbnear估计器的名称或其自定义实例中进行选择。当`fix_imbalance=False`时忽略。

**示例**

```python
 # load dataset
 from pycaret.datasets import get_data
 credit = get_data('credit')
 
 # init setup
 from pycaret.classification import *
 clf1 = setup(data = credit, target = 'default', fix_imbalance = True)
```

![image-Target Imbalance-before and after](images/PyCaret/image-Target Imbalance-before and after.png)

### 2.6 数据准备 - 去除离群值(Remove Outliers)

PyCaret中的`remove_outliers`函数允许您在训练模型之前识别并删除数据集中的异常值。使用奇异值分解技术通过PCA线性降维来识别异常值。可以使用[setup](https://www.pycaret.org/setup)中的`remove_outliers`参数来实现。通过`outliers_threshold`参数控制异常值的比例。

**相关参数**

- **remove_outliers: bool, default = False**

  当设置为`True`时，将使用孤立森林(Isolation Forest)删除训练数据中的异常值。

- **outliers_method: str, default = 'iforest'**

  用于删除异常值的方法。`remove_outliers=False`时忽略。可能的值为：

  - **'iforest':** 使用sklearn的IsolationForest.
  - **'ee'**: 使用sklearn的EllipticEnvelope.
  - **'lof':** 使用sklearn的LocalOutlierFactor.

- **outliers_threshold: float, default = 0.05**

  要从数据集中删除的异常值的百分比。在`remove_outliers=False`时忽略。

**示例**

```python
 # load dataset
 from pycaret.datasets import get_data
 insurance = get_data('insurance')
 
 # init setup
 from pycaret.regression import *
 reg1 = setup(data = insurance, target = 'charges', remove_outliers = True)
```

![image-remove outliers-description](images/PyCaret/image-remove outliers-description.png)

![image-remove outliers-before and after](images/PyCaret/image-remove outliers-before and after.png)

### 2.7 数据规整与转换 - 规范化/归一化(Normalize)

规范化是一种经常作为机器学习数据准备的一部分应用的技术。规范化的目标是重新缩放数据集中数字列的值，而不会扭曲值范围的差异或丢失信息。有几种方法可用于规范化，默认情况下，PyCaret使用`zscore`。

**相关参数**

- **normalize: bool, default = False**

  当设置为`True`时，将使用`normalized_method`参数下定义的方法转换特征空间。

- **normalize_method: string, default = ‘zscore**’

  定义要使用的规范化方法。默认情况下，使用`zscore`。其可选项包括：

  - **`zscore`** 使用$z=(x-u)/s$对数据进行标准化。
  - **`minmax`** 单独缩放和平移每个特征，使其处于0–1的范围内。
  - **`maxabs`** 单独地缩放和平移每个特征，使得每个特征的最大绝对值将是1.0。它不会移动/集中数据，因此不会破坏任何稀疏性。
  - **`robust`** 根据四分位间距对每个特征进行缩放和平移。当数据集包含异常值时，鲁棒缩放器通常会给出更好的结果。

**示例**

```python
 # load dataset
 from pycaret.datasets import get_data
 pokemon = get_data('pokemon')
 
 # init setup
 from pycaret.classification import *
 clf1 = setup(data = pokemon, target = 'Legendary', normalize = True)
```

![image-Normalize-before](images/PyCaret/image-Normalize-before.png)

![image-Normalize-after](images/PyCaret/image-Normalize-after.png)

![image-Normalize-Effect of Normalization](images/PyCaret/image-Normalize-Effect of Normalization.png)

### 2.8 数据规整与转换 - 特征变换(Feature Transform)

虽然[规范化](https://pycaret.gitbook.io/docs/get-started/preprocessing/scale-and-transform#normalize)在新的限制范围内重新缩放数据以减少方差大小的影响，但特征变换是一种更激进的技术。变换改变分布的形状，使得变换后的数据可以由正态或近似正态分布表示。有两种方法可用于转换`yeo-johnson`和`quantile`。

**相关参数**

- **transformation: bool, default = False**

  当设置为True时，将应用强大的转换器以使数据更正态(normal )/更高斯(Gaussian)。这对于建模与异方差或其他需要正态性的情况有关的问题很有用。通过最大似然估计稳定方差和最小化偏度的最优参数。

- **transformation_method: string, default = ‘yeo-johnson’**

  定义转换的方法。默认情况下，转换方法设置为`yeo-johnson`。另一个可用的选项是分位数(`quantile`)转换。这两种转换都将特征集变换为遵循类高斯分布或正态分布。分位数变换器是非线性的，并且可能使以相同尺度测量的变量之间的线性相关性失真。

**示例**

```python
 # load dataset
 from pycaret.datasets import get_data
 pokemon = get_data('pokemon')
 
 # init setup
 from pycaret.classification import *
 clf1 = setup(data = pokemon, target = 'Legendary', transformation = True)
```

![image-Feature Transform-before](images/PyCaret/image-Feature Transform-before.png)

![image-Feature Transform-after](images/PyCaret/image-Feature Transform-after.png)

![image-Feature Transform-effect](images/PyCaret/image-Feature Transform-effect.png)

### 2.9 数据规整与转换 - 目标变换(Target Transform)

目标变换类似于特征变换，因为它将改变目标变量而不是特征的分布形状。此功能仅在`pycarte.regression`模块中可用。

**相关参数**

- **transform_target: bool, default = False**

  当设置为True时，将使用`transform_target_method`参数中定义的方法转换目标变量。目标变换与特征变换是分开应用的。

- **transform_target_method: string, default = ‘yeo-johnson’**

  定义转换的方法。默认情况下，转换方法设置为`yeo-johnson`。转换的另一个可用选项是分位数。`transform_target=False`时忽略。

**before**

![image-Target Transform-before](images/PyCaret/image-Target Transform-before.png)

**after**

![image-Target Transform-after](images/PyCaret/image-Target Transform-after.png)

### 2.10 特征工程 - 多项式特征(Polynomial Features)

在机器学习实验中，常假设因变量和自变量之间的关系是线性的；然而，情况并非总是如此。有时，因变量和自变量之间的关系更为复杂。创建新的多项式特征有时可能有助于捕捉这种关系，否则可能会被忽视。

**相关参数**

- **polynomial_features: bool, default = False**

  当设置为True时，将基于数据集中数字特征中存在的所有多项式组合创建新特征，达到`polynomial_degree`参数中定义的程度。

- **polynomial_degree: int, default = 2**

  多项式特征的次数。例如，如果输入样本是二维的，并且形式为[a，b]，则次数=2的多项式特征为：[1，a，b，a^2，ab，b^2]。

```python
# load dataset
from pycaret.datasets import get_data
juice = get_data('juice')

# init setup
from pycaret.classification import *
clf1 = setup(data = juice, target = 'Purchase', polynomial_features = True)
```

![image-Polynomial Features-before](images/PyCaret/image-Polynomial Features-before.png)

![image-Polynomial Features-after](images/PyCaret/image-Polynomial Features-after.png)

### 2.11 特征工程 - 分组特征(Group Features)

当数据集包含以某种方式相互关联的特征时，例如：以某些固定时间间隔记录的特征，则可以使用`group_features`参数从现有特征中创建一组此类特征的新统计特征，如均值、中值、方差和标准差。

**相关参数**

- **group_features: list or list of list, default = None**

  当数据集包含具有相关特征的特征时，group_features参数可用于统计特征提取。例如，如果数据集具有相互关联的数字特征（即“Col1”、“Col2”、“Col3”），则可以在`group_features`下传递包含列名的列表，以提取统计信息，如平均值、中值、模式和标准差。

- **group_names: list, default = None**

  传递group_features时，组的名称可以作为包含字符串的列表传递到`group_names`参数中。group_names列表的长度必须等于`group_features`的长度。当长度不匹配或名称未传递时，将按顺序命名新特征，如group_1、group_2等。

**示例**

```python
 # load dataset
 from pycaret.datasets import get_data
 credit = get_data('credit')
 
 # init setup
 from pycaret.classification import *
 clf1 = setup(data = credit, target = 'default', group_features = ['BILL_AMT1', 'BILL_AMT2', 'BILL_AMT3', 'BILL_AMT4', 'BILL_AMT5', 'BILL_AMT6'])
```

![image-Group Features-before](images/PyCaret/image-Group Features-before.png)

![image-Group Features-after](images/PyCaret/image-Group Features-after.png)

### 2.12 特征工程 - 分仓数值特征(Bin Numeric Features)

特征分仓(Feature binning)是一种使用预定义数量，将连续变量分段转化为分类属性值的方法。当连续特征的唯一值过多或极值很少超出预期范围时，它是有效的。这样的极值影响训练的模型，从而影响模型的预测精度。在PyCaret中，可以使用`bin_numeric_features`参数将连续的数字特征分为区间。PyCaret使用“sturges”规则来确定分仓的数量，并使用K-Means聚类将连续的数值特征转换为分类特征。

**相关参数**

- **bin_numeric_features: list, default = None**

  当传递数字特征列表时，使用K-Means将其转换为分类特征，其中每个bin中的值具有1D K-Means聚类的相同最近中心。集群的数量是根据“sturges”方法确定的。它只对高斯数据是最优的，并且低估了大型非高斯数据集的bin数量。

**示例**

```python
 # load dataset
 from pycaret.datasets import get_data
 income = get_data('income')
 
 # init setup
 from pycaret.classification import *
 clf1 = setup(data = income, target = 'income >50K', bin_numeric_features = ['age'])
```

![image-Bin Numeric Features-before](images/PyCaret/image-Bin Numeric Features-before.png)

![image-Bin Numeric Features-after](images/PyCaret/image-Bin Numeric Features-after.png)

### 2.13 特征工程 - 组合稀有分类(Combine Rare Levels)

> 将低频的分类值合并为一个分类属性

有时，数据集可以具有一个分类特征（或多个分类特征），该分类特征具有非常高的级别数（即高基数特征-high cardinality features）。如果将这些特征编码为数值，则得到的矩阵是稀疏矩阵。这不仅由于特征数量和数据集大小的多重增加而使实验变得缓慢，而且还会在实验中引入噪声。稀疏矩阵可以通过组合具有高基数的特征中的稀有级别来避免。这可以在PyCaret中使用`rare_to_value`参数来实现。

**相关参数**

- **rare_to_value: float or None, default=None**

  类别列中出现的类别的最小分数。如果一个类别的频率低于`rare_to_value × len（X）`，则将其替换为rare_value中的字符串。使用此参数可以在对列进行编码之前对稀有类别进行分组。如果“无”，则忽略此步骤。																																															

- **rare_value: str, default="rare"**

  用于替换稀有类别的值。当`rare_to_value`为None时忽略。

**示例**

```python
 # load dataset
 from pycaret.datasets import get_data
 income = get_data('income')
 
 # init setup
 from pycaret.classification import *
 clf1 = setup(data = income, target = 'income >50K', rare_to_value = 0.1)
```

![image-Combine Rare Levels-before](images/PyCaret/image-Combine Rare Levels-before.png)

![image-Combine Rare Levels-after](images/PyCaret/image-Combine Rare Levels-after.png)

![image-Combine Rare Levels-Effect](images/PyCaret/image-Combine Rare Levels-Effect.png)

### 2.14 特征选择 - Feature Selection

**特征重要性(Feature Importance)**是一个用于选择数据集中在预测目标变量方面贡献最大的特征的过程。使用选定的特征而不是所有特征可以降低过度拟合的风险，提高准确性，并减少训练时间。在PyCaret中，这可以使用`feature_selection`参数来实现。

**相关参数**

- **feature_selection: bool, default = False**

  当设置为True时，将基于由`feature_selection_estimator`确定的特征重要性分数来选择特征子集。

- **feature_selection_method: str, default = 'classic'**

  - **'univariate':** 使用 sklearn's SelectKBest.
  - **'classic':** 使用 sklearn's SelectFromModel.
  - **'sequential':** 使用 sklearn's SequentialFeatureSelector.

- **feature_selection_estimator: str or sklearn estimator, default = 'lightgbm'**

  用于确定特征重要性的分类器。拟合后的估计器应具有`feature_importances_`或`coef_`属性。如果无，则使用LGB分类器(LGBClassifier)。当`feature_selection_method=univariate`时，将忽略此参数。

- **n_features_to_select: int or float, default = 0.2**

  使用feature_selection可选择的最大功能数。如果＜1，则为起始特征的分数。请注意，此参数在计数时不考虑`ignore_features`或`keep_features`中的功能。

**示例**

```python
 # load dataset
 from pycaret.datasets import get_data
 diabetes = get_data('diabetes')
 
 # init setup
 from pycaret.regression import *
 clf1 = setup(data = diabetes, target = 'Class variable', feature_selection = True)
```

![image-Feature Selection-before](images/PyCaret/image-Feature Selection-before.png)

![image-Feature Selection-after](images/PyCaret/image-Feature Selection-after.png)

### 2.15 特征选择 - 删除多重共线性特征(Remove Multicollinearity)

**多重共线性-Multicollinearity**（也称为共线*collinearity*）是一种数据集中的一个特征变量与另一特征变量高度线性相关的现象。多重共线性增加了系数的方差，从而使线性模型的系数不稳定且有噪声。处理多重共线的一种方法是放弃彼此高度相关的两个特征中的一个。可以在PyCaret中使用`remove_multicollinelity`参数来实现。

**相关参数**

- **remove_multicollinearity: bool, default = False**

  当设置为True时，高于定义阈值的互相关性特征将被删除。对于每个组，它将删除除与`y`具有最高相关性的特征之外的所有特征。

- **multicollinearity_threshold: float, default = 0.9**

  确定相关特征的最小绝对Pearson相关性。默认值删除相等的列。当`remove_multicollinearity`不为True时忽略。

**示例**

```python
 # load dataset
 from pycaret.datasets import get_data
 concrete = get_data('concrete')
 
 # init setup
 from pycaret.regression import *
 reg1 = setup(data = concrete, target = 'strength', remove_multicollinearity = True, multicollinearity_threshold = 0.3)
```

![image-Remove Multicollinearity-before](images/PyCaret/image-Remove Multicollinearity-before.png)

![image-Remove Multicollinearity-after](images/PyCaret/image-Remove Multicollinearity-after.png)

### 2.16 特征选择 - 主成分分析(Principal Component Analysis) 

主成分分析（PCA）是一种在机器学习中用于降低数据维度的无监督技术。它通过识别一个子空间来压缩特征空间，该子空间捕获完整特征矩阵中的大部分信息。它将原始特征空间投影到较低维度。

**相关参数**

- **pca: bool, default = False**

  当设置为True时，将使用`pca_method`参数中定义的方法应用降维，将数据投影到较低维度的空间中。

- **pca_method: string, default = ‘linear’**

  应用PCA的方法。可能的值为：

  - **'linear':** 使用 Singular Value Decomposition。
  - **'kernel':** 使用 RBF kernel 进行降维。
  - **'incremental':** 与 'linear' 相似，但对大数据集更加有效率。

- **pca_components: int/float, default = 0.99**

  要保留的组件数。如果 pca_components 是一个浮点值，则将其视为信息保留的目标百分比。当 pca_components 是一个整数时，它被视为要保留的特征数。pca_components 必须严格小于数据集中原始特征的数量。

- **pca_components: int, float, str or None, default = None** 

  要保留的组件数。当`pca=False`时，将忽略此参数。

  - **If None:** 所有成分将会被保留。 
  - **If int:** 组件的绝对值. 
  - **If float:** 需要解释的方差大于`n_components`指定的百分比。该值应介于0和1之间（仅适用于pca_thethod=“线性”）。
  - **If 'mle':** Minka’s MLE is used to guess the dimension (ony for pca_method='linear').

**示例**

```python
 # load dataset
 from pycaret.datasets import get_data
 income = get_data('income')
 
 # init setup
 from pycaret.classification import *
 clf1 = setup(data = income, target = 'income >50K', pca = True, pca_components = 10)
```

![image-Principal Component Analysis-before](images/PyCaret/image-Principal Component Analysis-before.png)

![image-Principal Component Analysis-after](images/PyCaret/image-Principal Component Analysis-after.png)

### 2.17 特征选择 - 忽略低方差(Ignore Low Variance)

有时，一个数据集可能具有多个级别的分类特征(categorical feature)，其中这些级别的分布是偏斜的，一个级别可能主导其他级别。这意味着由这种特征提供的信息没有太多变化。对于ML模型，这种特征可能不会添加很多信息，因此在建模时可以忽略。这可以在PyCaret中使用`low_variance_threshold`参数来实现。

**主要参数**

- **low_variance_threshold: float or None, default = None**

  删除训练集方差低于所提供阈值的特征。如果为0，则保留方差为非零的所有特征，即删除所有样本中具有相同值的特征。如果None，跳过此转换步骤。

**示例**

```python
 # load dataset
 from pycaret.datasets import get_data
 mice = get_data('mice')
 
 # filter dataset
 mice = mice[mice['Genotype'] == 'Control']
 
 # init setup
 from pycaret.classification import *
 clf1 = setup(data = mice, target = 'class', low_variance_threshold = 0.1)
```

![image-Ignore Low Variance-before](images/PyCaret/image-Ignore Low Variance-before.png)

![image-Ignore Low Variance-after](images/PyCaret/image-Ignore Low Variance-after.png)

### 2.18 其他参数 - 实验日志(Experiment Logging)

PyCaret可以自动记录整个实验，包括设置参数(setup parameters)、模型超参数(model hyperparameters)、性能指标(performance metrics)和管道工件(pipeline artifacts)。默认设置用[MLflow](https://mlflow.org/)作日志记录后端，[wandb](https://wandb.ai/), [cometml](https://www.comet.com/site/), [dagshub](https://www.dagshub.com/) 也可用于后端。可以启用设置中的参数来自动跟踪所有度量、超参数和模型工件。

**相关参数**

- **log_experiment: bool or str or BaseLogger or list of str or BaseLogger, default = False**

  与记录器相对应的 PyCaret `BaseLogger`或 str（`mlflow`、`wandb`、`comet_ml `或 `dagshub` 之一）的（列表），用于确定要使用哪些实验记录器。设置为True将仅使用MLFlow。

- **experiment_name: str, default = None**

  日志记录实验的名称。当 `log_experiment=False` 时忽略。

- **experiment_custom_tags: dict, default = None**

  标签名称的字典：字符串$\rightarrow$值：（字符串，但如果不是，将被字符串化）传递给mlflow.set_tags，为实验添加新的自定义标签。

- **log_plots: bool or list, default = False**

  当设置为True时，某些绘图将自动记录在MLFlow服务器中。要更改要记录的打印类型，请传递包含打印IDs的列表。请参阅`plot_model`的文档。当`log_experiment=False`时忽略。

- **log_profile: bool, default = False**

  当设置为True时，数据配置文件将作为html文件记录在MLflow服务器上。
  `log_experiment=False`时忽略

- **log_data: bool, default = False**

  当设置为True时，训练和测试数据集将记录为CSV文件。

**示例**

```python
# load dataset
from pycaret.datasets import get_data
data = get_data('diabetes')

# init setup
from pycaret.classification import *
clf1 = setup(data, target = 'Class variable', log_experiment = True, experiment_name = 'diabetes1')

# model training
best_model = compare_models() 
```

若要初始化MLflow服务器，必须在笔记本中或命令行中运行以下命令。服务器初始化后，您可以在上跟踪您的实验https://localhost:5000.

```cmd
# init server
!mlflow ui
```

![image-Experiment Logging-MLflow](images/PyCaret/image-Experiment Logging-MLflow.png)

**配置MLflow跟踪服务器**

当没有配置后端时，数据本地存储在提供的文件中（如果为空，则为./mlrun）。要配置后端，请在执行设置函数之前使用mlflow.set_tracking_uri。

- 一个空字符串或本地文件路径，前缀为file:/。数据本地存储在提供的文件中（如果为空，则为./mlrun）。

- 像这样的HTTP URI https://my-tracking-server:5000.

- Databricks工作区，以字符串“databricks”提供，或者，若要使用Databricks CLI 配置文件，则以“databricks://\<profileName\>”提供。

```python
# set tracking uri 
import mlflow 
mlflow.set_tracking_uri('file:/c:/users/mlflow-server')

# load dataset
from pycaret.datasets import get_data
data = get_data('diabetes')

# init setup
from pycaret.classification import *
clf1 = setup(data, target = 'Class variable', log_experiment = True, experiment_name = 'diabetes1')
```

**PyCaret on Databricks**

在Databricks上使用PyCaret时，设置中的`experiment_name`参数必须包括到存储的完整路径。请参阅以下使用Databricks时如何记录实验的示例：

```python
# load dataset
from pycaret.datasets import get_data
data = get_data('diabetes')

# init setup
from pycaret.classification import *
clf1 = setup(data, target = 'Class variable', log_experiment = True, experiment_name = '/Users/username@domain.com/experiment-name-here')
```

### 2.19 其他参数 - 模型选择(Model Selection)

setup中的以下参数，可用于模型选择的过程。这些与数据预处理无关，但可能会影响模型选择过程。

**相关参数**

- **train_size: float, default = 0.7**

  用于训练和验证的数据集的比例。

- **test_data: dataframe-like or None, default = None**

  如果不是`None`，则将`test_data`用作保留集(hold-out set)，并忽略train_size参数。data和test_data中的列必须完全匹配。

  > test_data的添加会大幅度影响模型选择过程中的精度。
  >
  > 基于目前已有的实验的猜测：
  >
  > - 现象1：设定test_data之后的性能指标明显变低，但是和最终预测的结果吻合的比较好。
  > - 现象2：test_data在这里由称为保留集，如果只是针对test_data来训练，不应该进行交叉验证。
  > - 猜测：经过查询，保留集这个概念应该和交叉验证有很大的关系，因为每次交叉验证都会取一部分数据作为验证集。所以可能需要在训练过程中禁用交叉验证。

- **data_split_shuffle: bool, default = True**

  设置为`False`时，可防止在`train_test_split`过程中打乱行。

- **data_split_stratify: bool or list, default = True**

  控制`train_test_split`过程中的分层。当设置为`True`时，它将按目标列进行分层。若要对任何其他列进行分层，请传递列名列表。当`data_split_shuffle`为`False`时忽略。

- **fold_strategy: str or scikit-learn CV generator object, default = ‘stratifiedkfold’**

- **fold:** int, default = 10

- **fold_shuffle:** bool, default = False

- **fold_groups:** str or array-like, with shape (n_samples,), default = None

### 2.20 其他参数

设置中的以下参数可用于控制其他实验设置，例如使用GPU进行训练或设置实验的详细程度。它们不会以任何方式影响数据。

**具体参数：**

- **n_jobs: int, default = -1**

  并行运行的作业数（对于支持并行处理的函数）-1表示使用所有处理器。要在单处理器上运行所有功能，请设置n_jobs=无

- **use_gpu: bool or str, default = False**

  当设置为`True`时，支持GPU的算法将使用GPU进行训练，如果不可用，则使用CPU。当设置为`force`时，它将仅使用启用GPU的算法，并在不可用时引发异常。当为`False`时，所有算法都只使用CPU进行训练。目前启用GPU的算法如下：

  - Extreme Gradient Boosting, requires no further installation
  - CatBoost Classifier, requires no further installation (GPU training is only enabled when data > 50,000 rows)
  - Light Gradient Boosting Machine, requires GPU installation [Tutorial](https://lightgbm.readthedocs.io/en/latest/GPU-Tutorial.html) 
  - Logistic Regression, Ridge Classifier, Random Forest, K Neighbors Classifier, Support Vector Machine, requires cuML >= 0.15 [cuML](https://github.com/rapidsai/cuml)

- **session_id: int, default = None**

  控制实验的随机性。它相当于scikit learn中的`random_state`。当`None`时，将生成一个伪随机数。这可以保证后续整个实验的再现性。

- **verbose: bool, default = True**

  设置为`False`时，不打印信息网格。

- **profile: bool, default = False**

  当设置为`True`时，将显示交互式EDA报告。

- **profile_kwargs: dict, default = {} (empty dict)**

  传递给用于创建EDA报告的`ProfileReport`方法的参数字典。如果`profile`为`False`，则忽略。

- **custom_pipeline: list of (str, transformer), dict or Pipeline, default = None**

  额外的自定义转换器。如果通过，则在所有内置转换器之后，它们最后应用于管道。

- **custom_pipeline_position: int, default = -1** 

  自定义管道在整个预处理管道中的位置。默认值最后添加自定义管道。

- **preprocess: bool, default = True**

  设置为`False`时，除了在`custom_pipeline`参数中传递的`train_test_split`和自定义转换外，不应用其他任何转换。当预处理设置为`False`时，数据必须先准备好再进行建模（没有缺失值、没有日期、分类数据编码）。

- **system_log: bool or str or logging.Logger, default = True**

  是否保存系统日志文件（作为logs.log）。如果输入的是字符串，将其用作日志文件的路径。如果输入已经是记录器对象，会使用该对象。

- **memory: str, bool or Memory, default=True**

  用于缓存管道的已配置好的转换器。

  - If False：则不执行缓存。
  - If True：则使用默认的临时目录。
  - If str：缓存目录的路径。

## 3. Train-比较模型-[compare_models](https://pycaret.gitbook.io/docs/get-started/functions/train#compare_models)

![PyCaret-workflow-3](images/PyCaret/PyCaret-workflow-3.jpg)

在PyCaret的 `setup()`完成后，建议将所有模型进行比较以评估性能(除非你确切知道需要什么类型的模型，通常情况下并非如此)，该函数使用交叉验证来训练和评估模型库中所有可用估计器的性能。该函数的输出是一个具有平均交叉验证分数的评分网格。

> 可以使用`get_Metrics`函数访问CV期间评估的度量。
>
> 也可以使用`add_metric`和`remove_metric`函数添加或删除自定义度量。

对于回归预测的任务，输出将打印一个分数网格，该网格显示模型、平均绝对误差（MAE）、均方误差（MSE）、确定系数（R^2^）、均方根误差（RMSE）、平均绝对百分比误差（MAPE）以及训练时间（TT）。

```python
best_model = compare_models()
```

![compare_models-1](images/PyCaret/image-compare_models-1.png)

`compare_models()`函数的作用是：一次比较多个模型。这是使用PyCaret的最大优点之一。在一行中，你可以看到许多模型之间的预测精度比较表。两个简单的单词(甚至不是一行代码)已经使用N倍交叉验证训练和评估了超过15个模型。

### 3.1 N-Fold 交叉验证

> ==todo== - 可以重写或细化这里
>
> 相关参考：
>
> https://zhuanlan.zhihu.com/p/471342052
>
> https://www.zhihu.com/tardis/bd/art/130875911?source_id=1001
>
> https://blog.csdn.net/encoding_utf_8/article/details/103225187

为更好的理解PyCaret是如何对模型进行比较，有必要对N-fold交叉验证的概念进行理解。

计算有多少数据应该划分到测试集中是一个微妙的问题。如果训练集太小，算法可能没有足够的数据来有效地学习。另一方面，如果测试集太小，准确度、精确度、召回率和F1分数可能会有很大的变化。

你可能很幸运，也可能很不幸！一般来说，将70%的数据放在训练集中，30%的数据放在测试集中是一个很好的起点。有时数据集太小了，70/30也会产生很大的差异。

一种解决方法是执行N折交叉验证。这里的中心思想是，将整个过程进行N次，然后平均精度。例如，在10折交叉验证中，将测试集的前10%的数据，并计算准确度、精确度、召回率和F1分数。然后，将使交叉验证建立第二个10%的数据，再次计算这些统计数据。最终，这个过程就进行10次，每次测试集都会有不同的数据，随后平均所有的准确度作为参考标准。

> 图注：验证集(这里是黄色)被认为是案例中的测试集

<img src="images/PyCaret/image-compare_models-Kfold.png" alt="compare_models-Kfold" style="zoom: 67%;" />

> 在K-最近邻算法中，当增加或减少K时，精确度会变化（未知）。一旦对模型的性能感到满意，就应该输入验证集以得到最终的模型(此时的模型相当于使用的训练集和验证集所有的数据，泛化性将只能使用测试集进行测试)。
>
> 整体上，交叉验证的工作原理与验证集/测试集体系非常相似，只是在构建或优化模型时有所区别。统一的是，通过找到一种精度指标，更好地了解算法在现实世界中的性能。 

### 3.2 改变排序标准 - Change the sort order

打印的表格可以突出显示了最高性能指标，仅供比较之用。默认表使用“R^2^”(从最高到最低)排序，也可以通过传递参数来更改。如`compare_models(sort = 'TT')`将根据TT对模型进行排序，效果如下：

```python
best_model = compare_models(sort = 'TT')
```

![Change the sort order](images/PyCaret/image-compare_models-Change the sort order.png)

### 3.3 仅比较部分模型 - Compare only a few models(include/exclude)

使用`include`参数，可以仅比较您选择的几个模型。

```python
best_model = compare_models(include = ['lr', 'dt', 'lightgbm'])
```

或者使用`exclude`参数，将比较除exclude参数中传递的模型之外的所有模型。

```python
best_model = compare_models(exclude = ['lr', 'dt', 'lightgbm'])
```

### 3.4 返回多个模型结果 - Return more than one model(n_select)

默认情况下，`compare_models()`只返回性能最高的模型，但如果您愿意，您可以获得前N个模型的列表。

```python
list_best_model = compare_models(n_select = 3)
top1_model, top2_model, top3_model = compare_models(n_select = 3)
```

### 3.5 设定预算时间 - Set the budget time(budget_time )

```python
best = compare_models(budget_time = 0.5)  # 超过0.5s的模型不再参与模型比较
```

### 3.6 设置概率阈值 - Set the probability threshold

执行==二进制分类==时，可以更改硬标签的概率阈值或截止值。默认情况下，所有分类器都使用0.5作为默认阈值。

```python
best = compare_models(probability_threshold = 0.25)
```

> 请注意，这个参数主用于[分类任务](https://pycaret.gitbook.io/docs/get-started/modules)中。
>
> 除了AUC之外的所有指标现在都不同了。AUC不会改变，因为它不取决于硬标签，其他一切都取决于现在使用probability_threshold=0.25获得的硬标签。

### 3.7 禁用交叉验证 - Disable cross-validation

如果你不想使用交叉验证来评估模型，而只是训练它们并查看测试/保持集上的指标，你可以将`cross_validation=False`。

```python
best = compare_models(cross_validation=False)
```

> 仅在[分类](https://pycaret.gitbook.io/docs/get-started/modules)和[回归](https://pycaret.gitbook.io/docs/get-started/modules)任务中有效。

### 3.8 查看模型参数 - Model parameter

```python
print(best_model)
```

![image-20230703204801697](images/PyCaret/image-compare_models-show_model_para.png)

### 3.9 集群上的分布式训练 - Distributed training on a cluster

要在大型数据集上进行扩展，可以使用名为parallel的参数在分布式模式下的集群上运行`compare_models`函数。它利用[Fugue](https://github.com/fugue-project/fugue/)抽象层在Spark或Dask集群上运行`compare_models`。

```python
# load dataset
from pycaret.datasets import get_data
diabetes = get_data('diabetes')

# init setup
from pycaret.classification import *
clf1 = setup(data = diabetes, target = 'Class variable', n_jobs = 1)

# create pyspark session
from pyspark.sql import SparkSession
spark = SparkSession.builder.getOrCreate()

# import parallel back-end
from pycaret.parallel import FugueBackend

# compare models
best = compare_models(parallel = FugueBackend(spark))
```

> 请注意，为了使用本地Spark进行测试，我们需要在设置中设置n_jobs=1，因为有些模型已经尝试使用所有可用的内核，而并行运行这些模型可能会导致资源争用导致的死锁。

对于Dask，我们可以在FugueBackend中指定“Dask”，它将提取可用的Dask客户端。

```python
# load dataset
from pycaret.datasets import get_data
diabetes = get_data('diabetes')

# init setup
from pycaret.classification import *
clf1 = setup(data = diabetes, target = 'Class variable', n_jobs = 1)

# import parallel back-end
from pycaret.parallel import FugueBackend

# compare models
best = compare_models(parallel = FugueBackend("dask"))
```

有关分布式执行的完整示例和其他功能，请查看[这个示例](https://github.com/pycaret/pycaret/blob/master/examples/PyCaret%202%20Fugue%20Integration.ipynb)。此示例还显示了如何实时获取排行榜。在分布式设置中，这涉及到设置RPCClient，但Fugue简化了这一点。

## 4. Train-创建模型-[create_model](https://pycaret.gitbook.io/docs/get-started/functions/train#create_model)

![workflow-4](images/PyCaret/PyCaret-workflow-4.jpg)

`create_model()`是PyCaret中最细粒度的函数，通常是PyCaret大多数功能的基础。正如它的名字所示，该函数使用交叉验证来训练和评估给定估计器的性能。此函数的输出是一个按倍数显示CV分数的评分网格。可以使用`get_Metrics`函数访问CV期间评估的度量。可以使用`add_metric`和`remove_metric`函数添加或删除自定义度量。所有可用的模型都可以使用模型功能访问。

对于回归预测任务，输出将打印一个计分表，按 Fold 显示MAE、MSE、RMSE、R2、RMSLE、MAPE。

```python
rf=create_model('rf')
```

![image-20230706114155919](images/PyCaret/image-create_model-1.png)

```python
rf=create_model('rf',return_train_score=True)
```

![image-20230709220925757](images/PyCaret/image-create_model-2.png)

当设置`return_train_score=True`时，交叉验证的结果将包括在训练集上计算得到的性能指标。这意味着除了在验证集上评估模型性能之外，还会提供模型在每个交叉验证折叠中在训练集上的性能指标。

当设置`return_train_score=False`时，默认行为是只返回在验证集上计算得到的性能指标，不包括训练集上的性能指标。

所有模型的平均分数与`compare_models()`上打印的分数匹配。这是因为`compare_models()`分数网格中打印的指标是所有折的平均分数。

还可以在每个模型的每个`print()`中看到用于构建它们的超参数。这是非常重要的，因为它是改进它们的基础。在下面的例子中，你可以看到Random Forest Regressor的参数。

![image-20230706114558448](images/PyCaret/image-create_model-show_model_para.png)

### 4.1 查看可用的模型 - Model library

对于预测回归任务，PyCaret模型库中有26个回归器可用。要查看所有分类器的列表，可查看文档或使用`models()`函数进行查看。

![image-compare_models-show_model](images/PyCaret/image-compare_models-show_model.png)

### 4.2 改变模型训练参数

**更改交叉验证(fold)参数 - Changing the fold param** 

```python
lr = create_model('lr', fold = 5)
```

**改变模型中的自定义参数 - Models with custom param**  

如果你更改模型中的超参数设置，可以使用下面的方式。以决策树为例，改变

```
# train decision tree
dt = create_model('dt', max_depth = 5)
```

![image-Train-model with custom param](images/PyCaret/image-Train-model with custom param.png)

### 4.3 访问评分网络 - Access the scoring grid

您在`create_model`之后看到的性能指标/评分网格仅显示，不会返回。因此，如果您想以`pandas.DataFrame`的形式访问该网格。可以在`create_model`之后立刻使用`pull`命令。

```python
# train decision tree
dt = create_model('dt', max_depth = 5)

# access the scoring grid
dt_results = pull()
print(dt_results)
```

![image-Train-access the scoring grid](images/PyCaret/image-Train-access the scoring grid.png)

### 4.4 禁用交叉验证 - Disable cross-validation

如果你不想使用交叉验证来评估模型，而只是训练它们并查看测试/保持集上的指标，你可以将`cross_validation`设置为False。

```python
lr = create_model('lr', cross_validation = False)
```

![image-train-disable cross_validation](images/PyCaret/image-train-disable cross_validation.png)

这些是测试/保持集上的指标。这就是为什么在原始输出中只看到一行，而不是12行。禁用交叉验证时，模型只在整个训练数据集上训练一次，并使用测试/保持集进行评分。

> 注意：此功能仅在“分类”和“回归”模块中可用。

### 4.5 返回训练得分 - Return train score

默认的评分网格按倍数显示验证集的性能指标。如果您还想按倍数查看训练集上的性能指标，以检查拟合过度/拟合不足，可以使用`return_train_score`参数。

```python
lr = create_model('lr', return_train_score = True)
```

![image_train_Return train score](images/PyCaret/image_train_Return train score.png)

### 4.6 设置概率阈值 - Set the probability threshold

执行二进制分类时，可以更改硬标签的概率阈值或截止值。默认情况下，所有分类器都使用0.5作为默认阈值。

```python
lr = create_model('lr', probability_threshold = 0.25)
```

### 4.7 循环训练模型 - Train models in a loop

您可以在循环中使用`create_model`函数来训练多个模型，甚至是具有不同配置的同一模型，并比较它们的结果。

```python
# train models in a loop
lgbs  = [create_model('lightgbm', learning_rate = i) for i in np.arange(0.1,1,0.1)]
```

> 注意，此时返回的是模型的列表

![image_Train_Train models in a loop](images/PyCaret/image_Train_Train models in a loop.png)

如果你想和大多数情况下一样跟踪指标，可以这样做。

```python
import numpy as np
import pandas as pd

# load dataset 
from pycaret.datasets import get_data 
diabetes = get_data('diabetes') 

# init setup
from pycaret.classification import * 
clf1 = setup(data = diabetes, target = 'Class variable')

# start a loop
models = []
results = []

for i in np.arange(0.1,1,0.1):
    model = create_model('lightgbm', learning_rate = i)
    model_results = pull().loc[['Mean']]
    models.append(model)
    results.append(model_results)
    
results = pd.concat(results, axis=0)
results.index = np.arange(0.1,1,0.1)
results.plot()
```

![image_Train_Output from results.plot()](images/PyCaret/image_Train_Output from results.plot().png)

### 4.8 训练自定义模型 - Train custom models

您可以使用自己的自定义模型进行训练，也可以使用不属于pycaret的其他库中的模型。只要他们的API与`sklearn`保持一致，它就会轻而易举地工作。

```python
# install gplearn library
# pip install gplearn

# load dataset 
from pycaret.datasets import get_data 
diabetes = get_data('diabetes') 

# init setup
from pycaret.classification import * 
clf1 = setup(data = diabetes, target = 'Class variable')

# import custom model
from gplearn.genetic import SymbolicClassifier
sc = SymbolicClassifier()

# train custom model
sc_trained = create_model(sc)
```

```python
type(sc_trained)
# >>> gplearn.genetic.SymbolicClassifier

print(sc_trained)
```

![image_Train_Train custom models](images/PyCaret/image_Train_Train custom models.png)

### 4.9 编写自己的模型 - Write your own models

您也可以使用`fit`和`predict`函数编写自己的类。PyCaret将与之兼容。下面是一个简单的例子：

```python
# load dataset 
from pycaret.datasets import get_data 
insurance= get_data('insurance') 

# init setup
from pycaret.regression import * 
reg1 = setup(data = insurance, target = 'charges')

# create custom estimator
import numpy as np
from sklearn.base import BaseEstimator
class MyOwnModel(BaseEstimator):
    
    def __init__(self):
        self.mean = 0
        
    def fit(self, X, y):
        self.mean = y.mean()
        return self
    
    def predict(self, X):
        return np.array(X.shape[0]*[self.mean])
        
# create an instance
my_own_model = MyOwnModel()

# train model
my_model_trained = create_model(my_own_model)
```

![image_Train_Write your own models](images/PyCaret/image_Train_Write your own models.png)

## 5. Optimize-调整模型-[tune_model](https://pycaret.gitbook.io/docs/get-started/functions/optimize#tune_model)

![PyCaret-workflow-](images/PyCaret/PyCaret-workflow-5.jpg)

在使用`create_model()`函数创建模型时，使用的是默认的超参数。使用`tune_model()`函数可以自动对超参数进行调整。该函数将在预定义搜索空间中的使用随机网格搜索自动调整模型的超参数，即超参数调优。

要自定义搜索网格，可以在tune_model函数中传递`custom_grid`参数。

### 5.1 使用方式及精度提升

**首先是原始模型精度**

![tune_model-1](images/PyCaret/image-tune_model-1.png)

**随后使用`tune_model()`调整模型**

```
tuned_knn=tune_model(knn)
```

![image-tune_model-2](images/PyCaret/image-tune_model-2.png)

==可以看出R2从0.34提高到了0.38==

![image-20230706155220504](images/PyCaret/image-tune_model-3.png)

==打印出来的模型参数也有变化==

> 注意：并不是所有模型使用tune_model后都会提高。（这里只是knn提高了，像rf/et/xgboost这些试了后并没有提高）

### 5.2 增加迭代 - Increasing the iteration

迭代次数取决于可用的时间和资源。由`n_iter`定义。默认设置为10。增加迭代次数有可能提升调优后的模型性能。

```
# tune model
```

**10次和50次迭代的比较**

**n_iter = 10**

![image-tune_model-Increasing_the_iteration-10](images/PyCaret/image-tune_model-Increasing_the_iteration-10.png)

**n_iter = 50**

![image-tune_model-Increasing_the_iteration-50](images/PyCaret/image-tune_model-Increasing_the_iteration-50.png)

### 5.3 选择度量方式 - Choosing the metric

当您调整模型的超参数时，您必须知道要针对哪个度量进行优化。可以在`optimize`参数下定义。默认情况下，对于分类任务，其设置为“`Accuracy `- 准确性”，对于回归，将其设置为“`R2`”。

```python
tuned_dt = tune_model(dt, optimize = 'MAE')
```

![tune_model-Choosing_the_metric](images/PyCaret/image-tune_model-Choosing_the_metric.png)

### 5.4 传递自定义网格 - Passing custom grid

PyCaret已经为库中的所有模型定义了超参数的调整网格。但是，如果您愿意，可以通过使用`custom_grid`参数传递自定义网格来定义自己的搜索空间。

```python
# load dataset
from pycaret.datasets import get_data 
boston = get_data('boston') 

# init setup
from pycaret.regression import * 
reg1 = setup(boston, target = 'medv')

# train model
dt = create_model('dt')

# define search space
params = {"max_depth": np.random.randint(1, (len(boston.columns)*.85),20),
          "max_features": np.random.randint(1, len(boston.columns),20),
          "min_samples_leaf": [2,3,4,5,6]}
          
# tune model
tuned_dt = tune_model(dt, custom_grid = params)
```

### 5.5 更改搜索算法 - Changing the search algorithm

PyCaret与许多不同的库无缝集成，用于超参数调整。这使您可以访问许多不同类型的搜索算法，包括随机、贝叶斯、optuna、TPE等。所有这些都只需更改一个参数。默认情况下，PyCaret使用sklearn中的`RandomGridSearch`，您可以通过使用`tune_mode`函数中的`search_library`和`search_algorithm`参数来更改它。

```python
# tune model sklearn
tune_model(dt)

# tune model optuna
tune_model(dt, search_library = 'optuna')

# tune model scikit-optimize
tune_model(dt, search_library = 'scikit-optimize')

# tune model tune-sklearn
tune_model(dt, search_library = 'tune-sklearn', search_algorithm = 'hyperopt')
```

- 搜索算法库 - search_library: str, default = 'scikit-learn'

  - 'scikit-learn'：default, https://github.com/scikit-learn/scikit-learn

  - 'scikit-optimize'：https://scikit-optimize.github.io/stable/

    ``pip install scikit-optimize``

  -  'tune-sklearn'：https://github.com/ray-project/tune-sklearn

    ``pip install tune-sklearn ray[tune]``

  -  'optuna'：https://optuna.org/

    ``pip install optuna``

- 搜索算法选择 -  search_algorithm: str, default = None

  > 搜索算法取决于“search_library”参数。某些搜索算法需要安装额外的库。
  > If None，将使用搜索库特定的默认算法。
  - 'scikit-learn' :
    - 'random' : 随机网络搜索 - random grid search (default)
    - 'grid' : 网络搜索 - grid search
  - 'scikit-optimize':
    - 'bayesian' : 贝叶斯搜索 - Bayesian search (default)
  - 'tune-sklearn' possible values:
    - 'random' : 随机网络搜索 - random grid search (default)
    - 'grid' : 网络搜索 - grid search
    - 'bayesian' : ``pip install scikit-optimize``
    -  ``'hyperopt' : ``pip install hyperopt
    -  ``'optuna' : ``pip install optuna
    -  ``'bohb' : ``pip install hpbandster ConfigSpace
  - 'optuna' possible values:
    - 'random' : 随机搜索 - randomized search
    - 'tpe' : 树结构Parzen估计搜索 - Tree-structured Parzen Estimator search (default)

### 5.6 访问微调器 - Access the tuner

默认情况下，PyCaret的`tune_mode`函数只返回优化器选择的最佳模型。有时您可能需要访问tuner对象，因为它可能包含重要的属性，您可以使用`return_tuner`参数。

```python
# tune model and return tuner
tuned_model, tuner = tune_model(dt, return_tuner=True)
```

```python
type(tuned_model), type(tuner)
```

![Access_the_tuner-1](images/PyCaret/image-tune_model-Access_the_tuner-1.png)

```python
print(tuner)
```

![Access_the_tuner-2](images/PyCaret/image-tune_model-Access_the_tuner-2.png)

### 5.7 自动选择更好的模型 - Automatically choose better

通常情况下，`tune_model`不会提高模型性能。事实上，它最终可能会使性能比具有默认超参数的模型更差。如果您没有在Notebook中积极进行实验，而是使用了运行`create_model`-->`tune_mode`或`compare_models`-->`tune_mode`工作流的python脚本，则这可能会产生问题。要解决这个问题，可以使用`choose_better`。当设置为`True`时，它将始终返回性能更好的模型，这意味着如果超参数调整不能提高性能，它将返回输入模型。

```python
# tune model
dt = tune_model(dt, choose_better = True)
```

> 注意：choose_better不会影响屏幕上显示的评分网格。评分网格将始终显示优化器选择的最佳模型的性能，不管输出性能是否小于输入性能。



### 5.8 集成模型 - ensemble_model()

`ensemble_model()`函数在Pycaret中用于对多个模型进行集成，以期望提高模型的性能和泛化能力。模型集成是一种常见的机器学习技术，通过结合多个模型的预测结果来生成最终的预测。此函数返回一个表，该表具有k倍的通用评估指标的交叉验证分数以及训练后的模型对象。使用的评估指标是： 分类（准确性，AUC，召回率，精度，F1，Kappa，MCC）回归（MAE，MSE，RMSE，R2，RMSLE，MAPE）。

可以使用`ensemble_model()`函数中的fold参数定义折叠次数。默认情况下，折叠倍数设置为10，所有指标均四舍五入到4位小数，可以使用round参数进行更改。有两种可用于模型集成的方法，通过`method`参数设置。这两种方法都需要对数据进行重新采样并拟合多个估计量，因此可以使用`n_estimators`参数来控制估计量的数量。默认情况下，`n_estimators`设置为10。

> 注意：该函数仅在pycaret.classification和pycaret.regression模块中可用。

#### Bagging - 装袋算法

Bagging，也称为Bootstrap aggregating，引导聚集算法，是一种机器学习集成元算法，旨在提高统计分类和回归中使用的机器学习算法的稳定性和准确性。 它还可以减少差异并有助于避免过度拟合。 尽管它通常应用于决策树方法，但可以与任何类型的方法一起使用。 套袋算法是模型平均方法的特例。

![image-Optimize-ensemble_model-Bagging](images/PyCaret/image-Optimize-ensemble_model-Bagging.png)

#### Boosting - 提升算法

Boosting是一种集成元算法，主要用于减少监督学习中的偏见和差异。 提升属于机器学习算法家族，可将弱学习者转化为强学习者。 弱学习者被定义为仅与真实分类略相关的分类器（它可以比随机猜测更好地标记示例）。 相反，学习能力强的分类器是与真实分类任意相关的分类器。

![image-Optimize-ensemble_model-Bagging_and_Boosting](images/PyCaret/image-Optimize-ensemble_model-Bagging_and_Boosting.png)

```python
# ensemble with bagging
ensemble_model(rf, method = 'Bagging')
```

![ensemble_model-1](images/PyCaret/image-other-ensemble_model-1.png)

ensemble_model中非常有用的其他一些参数包括：

- choose_better：自动选择更好的模型 - [Automatically choose better](https://pycaret.gitbook.io/docs/get-started/functions/optimize#automatically-choose-better-1)
- n_estimators：增加估算量 - [Increasing the estimators](https://pycaret.gitbook.io/docs/get-started/functions/optimize#increasing-the-estimators)
- groups
- fit_kwargs
- return_train_score

可以查看函数的文档字符串以了解更多信息

```python
help(ensemble_model)
```

### 5.9 混合模型 - blend_models()

在Pycaret中，`blend_models()`函数用于将多个已训练的模型进行集成（blending）。它采用一种加权平均的方法，结合多个模型的预测结果来生成最终的预测。

`blend_models()`函数的主要功能如下：

1. 模型集成：`blend_models()`函数接收多个已训练的模型对象作为输入，并将它们进行集成。这些模型可以是通过`compare_models()`或`create_model()`等函数训练和创建的。
2. 加权平均：`blend_models()`函数使用加权平均的方法，根据每个模型的预测性能和权重来生成最终的预测结果。权重可以手动指定，也可以使用默认的均匀权重。
3. 集成模型：`blend_models()`函数返回一个集成的模型对象，该模型对象包含了多个模型的组合。这个集成模型可以像单个模型一样进行预测操作。

```python
best_mae_models_top3 = compare_models(sort = 'MAE', n_select = 3)
# blend top 3 models
blend_models(best_mae_models_top3)
```

![blend_models-1](images/PyCaret/image-other-blend_models-1.png)

通过使用`blend_models()`函数，可以将多个模型的预测结果结合起来，以期望提高模型的性能和鲁棒性。

在blend_models中，您可能会发现其他一些非常有用的参数包括：

- choose_better - [Automatically choose better]()
- weights - [Changing the weights](https://pycaret.gitbook.io/docs/get-started/functions/optimize#changing-the-weights)
- optimize
- fit_kwargs
- return_train_score

可以查看函数的文档字符串以了解更多信息

```python
help(blend_models)
```

#### （1）动态输入估计器 - [Dynamic input estimators](https://pycaret.gitbook.io/docs/get-started/functions/optimize#dynamic-input-estimators)

您还可以使用`compare_models`函数自动生成输入估计量的列表。这样做的好处是，您根本不需要更改脚本。每次使用前N个模型作为输入列表时。

```python
# load dataset
from pycaret.datasets import get_data
diabetes = get_data('diabetes')

# init setup
from pycaret.classification import *
clf1 = setup(data = diabetes, target = 'Class variable')

# blend models
blender = blend_models(compare_models(n_select = 3))
```

注意这里发生的事情。我们将`compare_models(n_select = 3)`作为输入传递给`blend_models`。内部发生的情况是，首先执行`compare_modes`函数，然后将前3个模型作为输入传递到`blend_models`函数。

```python
print(blender)
```

![image-Optimize-blend_models-print(blender)](images/PyCaret/image-Optimize-blend_models-print(blender).png)

#### （2）混合方法 - [Changing the method](https://pycaret.gitbook.io/docs/get-started/functions/optimize#changing-the-method)

> 注意：方法参数仅在“分类”模块中可用。

当`method = 'soft'`时，它基于预测概率之和的argmax来预测类标签，推荐用于校准良好的分类器集合。

```python
# load dataset
from pycaret.datasets import get_data
diabetes = get_data('diabetes')

# init setup
from pycaret.classification import *
clf1 = setup(data = diabetes, target = 'Class variable')

# train a few models
lr = create_model('lr')
dt = create_model('dt')
knn = create_model('knn')

# blend models
blender_soft = blend_models([lr,dt,knn], method = 'soft')
```

当`method = 'hard'`时，它使用输入模型的预测（硬标签），而不是概率。

```python
# load dataset
from pycaret.datasets import get_data
diabetes = get_data('diabetes')

# init setup
from pycaret.classification import *
clf1 = setup(data = diabetes, target = 'Class variable')

# train a few models
lr = create_model('lr')
dt = create_model('dt')
knn = create_model('knn')

# blend models
blender_hard = blend_models([lr,dt,knn], method = 'hard')
```

默认方法设置为`auto`，这意味着它将优先尝试使用`soft`，如果不支持，则返回`hard`，当您的一个输入模型不支持`predict_proba`属性时，可能会发生这种情况。

#### （3）改变权重 - Changing the weights

默认情况下，在混合所有输入模型时，它们的权重都是相等的，但您可以明确地将权重传递给每个输入模型。

```python
# 代码片段 - blend models
blender_weighted = blend_models([lr,dt,knn], weights = [0.5,0.2,0.3])
```

您也可以使用tune_mode来调整优化器的权重。

```python
# 代码片段 - blend models
blender_weighted = blend_models([lr,dt,knn], weights = [0.5,0.2,0.3])

# 代码片段 - tune blender
tuned_blender = tune_model(blender_weighted)
```

```python
print(tuned_blender)
```

![image-Optimize-blend_models-Change_weights_print(tuned_blender)](images/PyCaret/image-Optimize-blend_models-Change_weights_print(tuned_blender).png)

### 5.10 堆叠模型 - stack_models()

在Pycaret中，`stack_models()`函数用于将多个已训练的模型进行堆叠（stacking）。堆叠是一种模型集成的技术，它通过将多个基础模型的预测结果作为输入，训练一个元模型来生成最终的预测。

`stack_models()`函数的主要功能如下：

1. 模型堆叠：`stack_models()`函数接收多个已训练的模型对象作为输入，并将它们进行堆叠。这些模型可以是通过`compare_models()`或`create_model()`等函数训练和创建的。
2. 元模型训练：`stack_models()`函数会使用一种堆叠的方法，在已有的模型预测结果的基础上，训练一个元模型。这个元模型可以是线性回归、随机森林等机器学习算法，用于组合基础模型的预测结果。
3. 特征选择：`stack_models()`函数还会自动进行特征选择，选择在堆叠中最具有预测能力的特征子集，以提高模型的性能。
4. 集成模型：`stack_models()`函数返回一个堆叠的模型对象，该模型对象包含了多个基础模型和一个元模型的组合。这个堆叠模型可以像单个模型一样进行预测操作。

```python
# stack models
stack_models(best_mae_models_top3)
```

![stack_models-1](images/PyCaret/image-other-stack_models-1.png)

通过使用`stack_models()`函数，可以将多个模型的预测结果结合起来，并使用元模型对它们进行进一步的组合和调整，以期望提高模型的性能和泛化能力。

stack_models中非常有用的其他一些参数包括：

- method - [Changing the method](https://pycaret.gitbook.io/docs/get-started/functions/optimize#changing-the-method-1)
- choose_better - 
- meta_model - [Changing the meta-model](https://pycaret.gitbook.io/docs/get-started/functions/optimize#changing-the-meta-model)
- restack - [Restacking](https://pycaret.gitbook.io/docs/get-started/functions/optimize#restacking)
- optimize - [optimize_threshold](https://pycaret.gitbook.io/docs/get-started/functions/optimize#optimize_threshold)
- return_train_score

可以查看函数的文档字符串以了解更多信息

```python
help(stack_models)
```

在Pycaret中，`stack_models()`和`blend_models()`是用于模型集成的两个不同函数，它们具有以下区别：

1. 集成方式：`stack_models()`函数使用堆叠（stacking）的方式进行模型集成，而`blend_models()`函数使用混合（blending）的方式进行模型集成。

2. 模型组合：`stack_models()`函数将多个基础模型的预测结果作为输入，训练一个元模型来生成最终的预测。它通过在已有的模型预测结果的基础上进行进一步的组合和调整，以提高模型的性能。

   `blend_models()`函数直接将多个训练好的模型进行集成，使用加权平均的方法（soft voting）或简单平均的方法（hard voting）生成最终的预测结果。

3. 数据使用：`stack_models()`函数使用训练集和验证集进行堆叠，它将基础模型在验证集上的预测结果作为元特征（meta-feature）输入给元模型进行训练。

   `blend_models()`函数使用训练集进行模型训练，然后使用训练好的模型直接对新数据进行预测，不需要额外的验证集。

4. 结果解释：`stack_models()`函数提供了对模型性能的更详细解释，因为它基于基础模型在验证集上的预测结果进行元模型的训练和组合。

   `blend_models()`函数提供了更简单的集成结果，它只是对多个模型的预测结果进行平均或投票，不需要进一步解释模型的性能。

选择使用`stack_models()`还是`blend_models()`取决于数据集和任务的特点以及对模型集成的需求。如果希望通过更复杂的堆叠方式获得更好的性能提升，并且愿意使用额外的验证集进行训练，可以选择`stack_models()`。如果希望使用简单而直接的模型集成方法，并且不需要额外的验证集，可以选择`blend_models()`。

在实践中，可以尝试使用这两个函数并比较它们的性能，以选择最适合特定问题的模型集成方法。

**堆叠和混合区别：**

堆叠（Stacking）和混合（Blending）是两种常见的模型集成技术，用于将多个模型的预测结果进行组合以获得更好的性能。它们有以下区别：

1. 数据使用：
   - 堆叠：堆叠使用训练集和验证集进行模型集成。首先，基础模型在训练集上进行训练，并在验证集上生成预测结果。然后，这些预测结果被作为元特征输入给元模型（也称为次级模型或元学习器），进一步训练元模型来生成最终的预测结果。
   - 混合：混合使用训练集进行模型训练，并直接在新数据上生成预测结果。多个训练好的模型的预测结果通过加权平均（软投票）或简单平均（硬投票）的方式进行组合，生成最终的预测结果。
2. 结果解释：
   - 堆叠：堆叠提供了对模型性能的更详细解释。由于堆叠包括了基础模型的预测结果作为元特征，通过元模型的训练和组合，可以进一步改进模型的性能。堆叠可以捕捉到不同模型之间的差异和互补性。
   - 混合：混合提供了更简单的集成结果。它只是将多个模型的预测结果进行平均或投票，而不需要进一步解释模型的性能。
3. 训练过程：
   - 堆叠：堆叠需要两个步骤，首先是基础模型的训练，然后是元模型的训练。需要在训练集和验证集上进行多次模型训练和预测。
   - 混合：混合只需要进行一次基础模型的训练，并在训练集上生成预测结果。然后，可以直接在新数据上使用训练好的模型进行预测。
4. 集成结果：
   - 堆叠：堆叠使用基础模型的预测结果作为元特征，并通过元模型对这些特征进行进一步的训练和组合。最终的集成结果来自元模型的预测结果。
   - 混合：混合直接将多个模型的预测结果进行组合，生成最终的集成预测结果。

### 5.11 校准模型 - calibrate_model

该函数使用等渗回归或逻辑回归(isotonic or logistic regression)来校准给定模型的概率。此函数的输出是一个按倍数显示CV分数的评分网格。可以使用`get_Metrics`函数访问CV期间评估的度量。可以使用`add_metric`和`remove_metric`函数添加或删除自定义度量。

```python
# 示例
# load dataset
from pycaret.datasets import get_data
diabetes = get_data('diabetes')

# init setup
from pycaret.classification import *
clf1 = setup(data = diabetes, target = 'Class variable')

# train a model
dt = create_model('dt')

# calibrate model
calibrated_dt = calibrate_model(dt)
```

```
print(calibrated_dt)
```

![image-Optimize-blend_models-Change_weights_print(tuned_blender)](images/PyCaret/image-Optimize-blend_models-Change_weights_print(tuned_blender)-1714019259721.png)

> [Before and after calibration](https://pycaret.gitbook.io/docs/get-started/functions/optimize#before-and-after-calibration)

## 6. Analyze-模型分析与绘制-[plot_model](https://pycaret.gitbook.io/docs/get-started/functions/analyze#plot_model)

![PyCaret-workflow-6](images/PyCaret/PyCaret-workflow-6.jpg)

`plot_model()`函数是Pycaret库中用于绘制模型相关图表的函数。它可以根据提供的参数绘制不同类型的图表来帮助分析和可视化模型的性能和结果。

下面是`plot_model()`函数中的`plot`参数的可选值和对应的图表类型：

- `'pipeline'`：预处理流程图
- `'residuals_interactive'`：交互式残差图
- `'residuals'`：残差图
- `'error'`：预测误差图
- `'cooks'`：Cook's距离图
- `'rfe'`：递归特征选择图
- `'learning'`：学习曲线
- `'vc'`：验证曲线
- `'manifold'`：流形学习图
- `'feature'`：特征重要性图
- `'feature_all'`：所有特征的特征重要性图
- `'parameter'`：模型超参数图
- `'tree'`：决策树图

通过选择适当的`plot`参数值，您可以生成所需类型的图表来更好地理解和评估模型的性能和特征重要性。

```python
plot_model(best_model,plot='residuals_interactive')
```

![plot-residuals_interactive](images/PyCaret/image-plot-residuals_interactive-1.png)

```python
plot_model(best_model,plot='residuals')
```

![plot-residuals](images/PyCaret/image-plot-residuals.png)

```python
plot_model(rf,plot='feature')
```

![plot-feature](images/PyCaret/image-plot-feature.png)

### 6.1 plot绘图参数调整

#### （1）更改绘图比例 - **Change the scale**

图形的分辨率比例可以通过`scale`参数进行更改。

```python
# load dataset
from pycaret.datasets import get_data
diabetes = get_data('diabetes')

# init setup
from pycaret.classification import *
clf1 = setup(data = diabetes, target = 'Class variable')

# creating a model
lr = create_model('lr')

# plot model
plot_model(lr, plot = 'auc', scale = 3)
```

![image-Analyze-plot-change_the_scale](images/PyCaret/image-Analyze-plot-change_the_scale.png)

#### （2）保存绘图 - Save the plot

可以使用`save`参数将打印另存为png文件。

```python
# 代码片段 - plot model
plot_model(lr, plot = 'auc', save = True)
```

![image-Analyze-plot-save_the_plot](images/PyCaret/image-Analyze-plot-save_the_plot.png)

#### （3）自定义绘图 - Customize the plot

PyCaret在大多数绘图中使用[Yellowbrick](https://www.scikit-yb.org/en/latest/)。任何Yellowbrink可视化工具可接受的参数都可以作为`plot_kwargs`参数传递。

```python
# load dataset
from pycaret.datasets import get_data
diabetes = get_data('diabetes')

# init setup
from pycaret.classification import *
clf1 = setup(data = diabetes, target = 'Class variable')

# creating a model
lr = create_model('lr')

# plot model
plot_model(lr, plot = 'confusion_matrix', plot_kwargs = {'percent' : True})
```

![image-Analyze-plot-Customize_the_plot](images/PyCaret/image-Analyze-plot-Customize_the_plot.png)

> tab blog

#### （4）使用训练集数据 - Use train data

如果要评估列车数据上的模型图，可以在`plot_mode`函数中传递`use_train_data=True`。

```python
# plot model
plot_model(lr, plot = 'auc', use_train_data = True)
```

> tab blog - Plot on train data vs. hold-out data

#### （5）可用的绘图类型 - **[Examples by module](https://pycaret.gitbook.io/docs/get-started/functions/analyze#examples-by-module)**

> 暂不描述

### 6.2 汇总-模型评估 - [evaluate_model](https://pycaret.gitbook.io/docs/get-started/functions/analyze#evaluate_model)

![PyCaret-workflow-7](images/PyCaret/PyCaret-workflow-7.jpg)

分析模型性能的另一种方法是使用`evaluate_model()`函数，该函数显示给定模型的所有可用图形的用户界面。在内部它使用`plot_model()`函数。

```python
evaluate_model(best_model)
```

![evaluate_model-1](images/PyCaret/image-evaluate_model-1.png)


## 7. Analyze-解释模型-[interpret_model](https://pycaret.gitbook.io/docs/get-started/functions/analyze#interpret_model)

### 7.1 可解释性模型 - interpret_model

> ==todo== 该部分的内容还有待进一步完善

在Pycaret的`interpret_model()`函数中，可以使用不同的绘图属性（plot_type）来生成不同类型的解释图表。基于[SHAP（SHapley加法解释）](https://shap.readthedocs.io/en/latest/)实现，以下是常用的解释图表类型和对应的绘图属性：

1. 'summary'：基于SHAP生成模型的摘要图表。该图表显示了每个特征对于预测的贡献程度，以及特征的重要性排序。
2. 'correlation'：基于SHAP生成相关性图（Correlation）。该图表显示了特征与模型预测之间的关系，可以帮助理解特征如何影响预测结果。
3. 'reason'：基于SHAP生成力图（Reason Plot）。力图展示了单个样本的特征对预测结果的影响，可以帮助解释单个样本的预测原因。
4. 'pdp'：生成部分依赖图（Partial Dependence Plot）。部分依赖图显示了特定特征在其他特征固定的情况下，对预测结果的影响。

```python
# interpret model
interpret_model(xgboost, plot = 'pdp', feature = 'Age (years)')
```

5. 'msa'：执行Morris敏感性分析（Morris Sensitivity Analysis），用于评估模型中各个特征对输出的敏感性。

6. 'pfi'：执行排列特征重要性（Permutation Feature Importance）分析，用于确定特征对模型预测的重要性排序。

这些解释图表可以帮助理解模型的特征重要性、特征对预测的影响、特征之间的相关性等，从而提供对模型的解释和理解。根据需要，可以选择适合问题的解释图表类型，并通过调整参数来定制图表的输出。

> 前三个函数只支持这些模型: catboost, xgboost, lightgbm, rf, et, dt.

```python
rf=create_model('rf')
interpret_model(rf, plot = 'summary')
```

![interpret_model-summary-rf](images/PyCaret/image-interpret_model-summary-rf.png)

```python
interpret_model(rf, plot = 'correlation')
```

![interpret_model-correlation](images/PyCaret/image-interpret_model-correlation.png)

```python
# load dataset
from pycaret.datasets import get_data
diabetes = get_data('diabetes')

# init setup
from pycaret.classification import *
clf1 = setup(data = diabetes, target = 'Class variable')

# creating a model
xgboost = create_model('xgboost')

# interpret model
interpret_model(xgboost, plot = 'reason')
```

![image-Analyze-interpret_model-reason_plot](images/PyCaret/image-Analyze-interpret_model-reason_plot.png)

当您在不通过测试数据的特定索引的情况下生成原因图时，将获得显示的交互式图，并能够选择x轴和y轴。只有当您使用Jupyter Notebook或同等环境时，这才可能实现。如果要查看特定观测的此图，则必须传递观测参数中的索引。

```python
interpret_model(rf, plot = 'reason', observation = 1)
```

![interpret_model-reason](images/PyCaret/image-interpret_model-reason.png)

#### （1）保存绘图 - Save the plot

可以使用`save`参数将打印另存为`png`文件。

```python
# interpret model
interpret_model(xgboost, save = True)
```

#### （2）使用训练集数据 - Use train data

默认情况下，所有绘图都是在测试数据集上生成的。如果要使用训练数据集（不推荐）生成绘图，可以使用`use_train_data`参数。

```python
# interpret model
interpret_model(xgboost, use_train_data = True)
```

### 7.2 公平性检验 - check_fairness

公平概念化有多种方法。`check_fairness`函数遵循被称为群体公平的方法，该方法询问：群体中哪些个体更易收到经验伤害的风险。`check_fairness`提供不同组（也称为子群体）之间的公平性相关度量。

```python
# load dataset
from pycaret.datasets import get_data
income = get_data('income')

# init setup
from pycaret.classification import *
exp_name = setup(data = income,  target = 'income >50K')

# train model
lr = create_model('lr')

# check model fairness
lr_fairness = check_fairness(lr, sensitive_features = ['sex', 'race'])
```

![image-Analyze-check_fairness](images/PyCaret/image-Analyze-check_fairness.png)

## 8. Deploy-模型预测-[predict_model](https://pycaret.gitbook.io/docs/get-started/functions/deploy#predict_model)

![PyCaret-workflow-9](images/PyCaret/PyCaret-workflow-9.png)

在最终确定模型之前，建议通过预测测试和查看评估指标来执行最终检查。如果你查看信息表，你将看到20%的数据被分离为测试集样本。

我们在上面看到的所有评估指标大部分都是基于训练集(80%)的交叉验证结果。现在，使用存储在tuned_rf变量中的最终训练模型，我们根据测试样本进行预测，并评估指标，看它们是否与CV结果有实质性差异

```python
predict_model(et)
```

![predict_model-1](images/PyCaret/image-predict_model-1.png)

![predict_model-2](images/PyCaret/image-predict_model-2.png)

可以看出示例数据中与之前训练的结果差异，测试集的MAE为0.0337，训练集的MAE为0.0348，其他精度的差距也没有显著区别。如果测试集和训练集的结果之间存在较大差异，这通常表示过拟合，但也可能是由于其他几个因素造成的，需要进一步调查。

在新的数据上进行预测

```python
# predict on new data
new_data = diabetes.copy()
new_data.drop('Class variable', axis = 1, inplace = True)
new_pred_data = predict_model(xgboost, data = new_data)
```

## 9. Deploy-确定最终模型-[finalize_model](https://pycaret.gitbook.io/docs/get-started/functions/deploy#finalize_model)

![PyCaret-workflow-8](images/PyCaret/PyCaret-workflow-8.png)

确定最终模型是实验的最后一步。PyCaret中的正常机器学习工作流从`setup()`开始，然后使用`compare_models()`对所有模型进行比较，并预先选择一些候选模型(基于感兴趣的度量)，以执行各种建模技术，如超参数拟合、装配、堆叠等。

`finalize_model()`函数使模型拟合完整的数据集，包括测试样本(在本例中为20%)。此函数的目的是在将模型部署到生产环境之前，对模型进行完整的数据集训练。我们可以在`predict_model()`之后或之前执行此方法。我们要在这之后执行。

> ==注意==：使用`finalize_model()`完成模型后，整个数据集(包括测试集)将用于训练。因此，在`finalize_model()`之后使用模型对测试集进行预测，打印的信息网格将产生误导，因为它试图对用于建模的相同数据进行预测。（先使用`finalize_model()`后再使用`predict_model()`会造成数据过拟合，因为`finalize_model()`将所有数据包括测试集和训练集都拿去训练了。）

```python
final = finalize_model(best_model)
```

![finalize_model](images/PyCaret/image-finalize_model.png)

使用`finalize_model()`函数对模型进行最终化之后，通常会将其用于新数据的预测。

然而，如果在对模型进行最终化之后再次使用`predict_model()`函数，可能会导致潜在的问题。这是因为`finalize_model()`函数在内部对模型进行了最后的拟合和调优，以便在整个训练集上获得最佳性能。因此，如果再次使用`predict_model()`函数，实际上会再次对已经最终化的模型进行拟合，这可能会导致过拟合或无效的结果。

## 10. Deploy-保存/加载模型-save/load_model

![workflow-10](images/PyCaret/PyCaret-workflow-10.png)

> [在云端部署模型 - deploy_model](https://pycaret.gitbook.io/docs/get-started/functions/deploy#deploy_model): AWS、GCP、Azure

### 10.1 保存模型 - save_model

在Pycaret中，`save_model()`函数用于将训练和最终化的模型保存到磁盘上，以便以后在不同的环境中使用、加载和部署模型。

`save_model()`函数的主要功能如下：

1. 模型保存：`save_model()`函数接收训练和最终化的模型对象，并将其保存到磁盘上的指定位置。模型可以保存为特定格式的文件，以便在以后的任务中加载和使用。
2. 模型文件格式：Pycaret支持将模型保存为不同的文件格式，如pickle文件（.pkl）、Joblib文件（.joblib）等。这些文件格式是常见的机器学习模型保存和加载的标准格式。

```python
save_model(final, 'pycaret_model1')
```

![save_model](images/PyCaret/image-save_model.png)

这个模型对象是一个`Pipeline`对象，其中包含了一系列的数据预处理步骤和最终的模型。下面是对该模型中部分对象的解释：

1. `numerical_imputer`（数值型特征填补）：使用`SimpleImputer`进行数值型特征（'GR', 'NPHI', 'RD'）的缺失值填补。
2. `categorical_imputer`（分类特征填补）：使用`SimpleImputer`进行分类特征的缺失值填补，策略为使用最频繁的值进行填补。
3. `normalize`（归一化）：使用`StandardScaler`进行数据的归一化处理。
4. `actual_estimator`（最终的估计器）：使用`ExtraTreesRegressor`作为最终的回归模型，使用了全部可用的CPU核心（`n_jobs=-1`），并设定了随机种子为42。

整个模型对象被保存为一个pickle文件（.pkl），文件名为 `'pycaret_model1.pkl'`。

### 10.2 加载模型 - load_model

要在同一环境或其他环境中加载在将来某个日期保存的模型，我们将使用PyCaret的`load_model()`函数，然后轻松地将保存的模型应用到新的未查看的数据中以进行预测

```python
ml_model=load_model('pycaret_model1')
```

![load_model](images/PyCaret/image-load_model.png)

一旦模型加载到环境中，就可以使用相同的predict_model()函数来预测任何新数据。接下来，我们应用加载模型来预测新数据

```python
df2 = pd.read_csv('aligned_well_02_1_strech_1.csv')
prediction=predict_model(ml_model, data=df2)
prediction
```

![predict_model](images/PyCaret/image-predict_model.png)

### 10.3 保存实验设置 - save_experiment

`save_experiment`函数将实验保存到pickle文件中。该实验是使用cloudpickle来处理lambda函数来保存的。数据或测试数据不与实验一起保存，使用`load_experiment`加载时需要再次指定。

```python
# load dataset
from pycaret.datasets import get_data
diabetes = get_data('diabetes')

# init setup
from pycaret.classification import *
clf1 = setup(data = diabetes, target = 'Class variable')

# save experiment
save_experiment('my_saved_experiment1')
```

### 10.4 读取实验设置 - load_experiment

load_experiment函数从路径或文件加载实验。数据（和test_data）不与实验一起保存，需要在加载时再次指定。

```python
# load data
data = get_data('diabetes')

# load experiment function
from pycaret.classification import load_experiment
clf2 = load_experiment('my_saved_experiment1', data = data)
```

## 11. 其他参数与设置

> todo 还有一些内容和参数没有添加

### 11.1 pull() - 返回最后打印的得分网格

返回最后打印的得分网格。在任何训练函数之后使用`pull`函数将得分网格存储在`panda.DataFrame`。

```python
# loading dataset
from pycaret.datasets import get_data
data = get_data('diabetes')

# init setup
from pycaret.classification import *
clf1 = setup(data, target = 'Class variable')

# compare models
best_model = compare_models()

# get the scoring grid
results = pull()
```

*输出的图片，略*

```python
type(results)
# >>> pandas.core.frame.DataFrame
```

### 11.2 get_config() - 获取配置信息

在Pycaret中，`get_config()`函数用于获取当前Pycaret会话的配置信息。它返回一个字典，其中包含Pycaret当前会话的各种配置选项和参数。

`get_config()`函数的主要功能如下：

1. 获取配置信息：`get_config()`函数可以获取Pycaret当前会话的各种配置信息，包括数据预处理选项、模型训练选项、特征工程选项、交叉验证选项等。
2. 配置参数：返回的字典中的键值对表示不同的配置参数。可以通过访问相应的键来获取特定的配置参数值。

```python
get_config()
```

![get_config](images/PyCaret/image-other-get_config.png)

- `USI`：用户自定义标识符，用于跟踪和标识特定实验的用户自定义标识符。
- `X`：原始特征（输入变量）。
- `X_test`：测试集中的特征。
- `X_test_transformed`：经过预处理和转换后的测试集特征。
- `X_train`：训练集中的特征。
- `X_train_transformed`：经过预处理和转换后的训练集特征。
- `X_transformed`：经过预处理和转换后的特征（整个数据集）。
- `data`：原始数据集。
- `exp_id`：实验的唯一标识符。
- `exp_name_log`：用于记录实验的名称。
- `fold_generator`：用于生成交叉验证折叠的方法。
- `log_plots_param`：用于配置记录图表的参数。
- `n_jobs_param`：配置Pycaret中任务执行的CPU核心数量。
- `pipeline`：预处理和模型训练的管道。
- `seed`：随机种子，用于控制随机性。
- `target_param`：目标变量（预测变量）。
- `test`：测试集。
- `test_transformed`：经过预处理和转换后的测试集。
- `train`：训练集。
- `train_transformed`：经过预处理和转换后的训练集。
- `y`：原始目标变量（预测变量）。
- `y_test`：测试集中的目标变量。
- `y_test_transformed`：经过预处理和转换后的测试集目标变量。
- `y_train`：训练集中的目标变量。
- `y_train_transformed`：经过预处理和转换后的训练集目标变量。

```python
config =get_config('X_test_transformed')
config
```

![get_config-2](images/PyCaret/image-other-get_config-2.png)

在上面的示例中，首先使用`setup()`函数初始化分类任务，并根据需要进行了其他的Pycaret配置。然后，通过`get_config()`函数获取当前Pycaret会话的配置信息，并将其存储在`config`变量中。

```python
# lets check the X_train_transformed to see effect of params passed
get_config('X_train_transformed')['GR'].hist()
```

![get_config-3](images/PyCaret/image-other-get_config-3.png)

使用`set_config`可以设置全局参数

```python
# reset environment seed
set_config('seed', 999)
```

### 11.3 get_leaderboard() - 获取排行榜

在Pycaret中，`get_leaderboard()`函数用于获取模型的排行榜，按照性能指标对训练的多个模型进行排序并显示。它提供了一个简洁的方式来比较和评估不同模型的性能。

模型排行榜显示了不同模型在选择的性能指标上的得分。可以根据排行榜的结果来选择最佳模型或进一步分析模型的性能。

```python
# get leaderboard
lb = get_leaderboard()
lb
```

<img src="images/PyCaret/image-other-get_leaderboard.png" alt="get_leaderboard" style="zoom:80%;" />

get_leaderboard中非常有用的其他一些参数包括：

- finalize_models
- fit_kwargs
- model_only
- groups

可以查看函数的文档字符串以了解更多信息

```python
help(get_leaderboard)
```

可以通过该方式获得训练的工作流

```python
# check leaderboard
lb = get_leaderboard()

# select top model
lb.iloc[0]['Model']
```

### 11.4 automl()

此函数根据优化参数返回当前设置中所有训练模型中的最佳模型。可以使用get_Metrics函数访问评估的度量。

```python
# find best model based on CV metrics
automl()
```

![automl](images/PyCaret/image-other-automl.png)

### 11.5 dashboard()

ExplainerDashboard是一个用于解释机器学习模型的Python库和工具，它提供了一个交互式仪表板，用于可视化和解释模型的预测结果、特征重要性和模型行为。

ExplainerDashboard 的主要功能包括：

1. 模型解释：ExplainerDashboard 可以帮助用户解释和理解机器学习模型的预测结果。它提供了多种图表和可视化工具，用于分析特征重要性、预测误差、决策边界等方面。
2. 特征重要性：ExplainerDashboard 可以显示模型中各个特征的重要性和影响程度。通过可视化工具，用户可以了解每个特征对于模型预测的贡献程度，并比较不同特征之间的重要性。
3. 预测解释：ExplainerDashboard 可以帮助用户解释和理解单个样本的预测结果。用户可以通过仪表板中的工具，深入分析每个特征对于某个样本的影响，并探索样本预测的原因和解释。
4. 模型比较：ExplainerDashboard 可以同时显示多个模型的解释结果，帮助用户比较和对比不同模型的性能和解释。用户可以在仪表板中选择不同的模型，并观察它们的预测结果和特征重要性。

ExplainerDashboard 是一个灵活且易于使用的工具，可应用于各种机器学习任务和模型类型。它提供了可视化和交互式的解释功能，使用户能够更好地理解和解释模型的行为和预测结果。通过使用 ExplainerDashboard，用户可以增加对模型的信任和理解，并从中获取有价值的见解。

目前的版本提供了5个tab，包括

- **特征重要性**
- **模型预测结果总结**
- **单个样本解释（局部）**
- **what **  **if**  **模拟**
- **特征依赖分析（全局）**
- **特征交互分析**
- **树模型（针对个别模型，随机森林等）**

需要先安装：pip install explainerdashboard

```python
# dashboard function
dashboard(bm) #可以是混合的模型也可以是单个模型
```

<img src="images/PyCaret/image-other-dashboard.png" alt="dashboard" />

<img src="images/PyCaret/image-other-dashboard-2.png" alt="dashboard-2" style="zoom:80%;" />

### 11.6 deep_check()

深度检查（DeepChecks）是一个用于模型质量评估的技术和方法。它旨在帮助检测和识别机器学习模型中的问题、缺陷和偏差。通过进行全面的模型检查，可以增加对模型的可靠性、鲁棒性和可解释性的信心。

需要先安装：pip install deepchecks

```python
# deep check function
deep_check(bm)
```

![deep_check](images/PyCaret/image-other-deep_check.png)

### 11.7 eda()

在 Pycaret 中，`eda()` 函数用于进行数据探索性数据分析（Exploratory Data Analysis，简称 EDA）。该函数可以帮助用户快速了解和分析数据集的特征、分布、关联性等信息，以支持后续的数据预处理和建模工作。

`eda()` 函数将生成一个交互式的数据探索仪表板，其中包含各种图表和可视化工具。这些图表包括数据的概览、特征分布、相关性矩阵、变量重要性等。通过这些图表，用户可以快速了解数据的特征和统计信息，并探索特征之间的关系。

通过使用 `eda()` 函数，可以更好地理解数据集的特点、发现异常值或缺失数据、探索特征之间的关联性，并为后续的数据预处理和建模工作提供指导。

需要先安装：pip install autoviz

```python
# eda function
eda()
```

![eda](images/PyCaret/image-other-eda.png)

### 11.8 create_app()

在 Pycaret 中，`create_app()` 是一个用于创建交互式 Web 应用程序的函数。它可以将 Pycaret 模型和功能转换为一个具有用户界面的在线应用程序，使用户能够轻松地使用模型进行预测和分析。

`create_app()` 函数将基于提供的模型和数据集创建一个 Web 应用程序，其中用户可以进行实时的模型预测和分析。该应用程序将在本地主机上启动一个 Web 服务器，并提供一个网页界面，用户可以在其中输入数据并获得模型的预测结果。

通过使用 `create_app()` 函数，可以将训练的模型快速转化为一个可用的在线应用程序，以便用户能够轻松地使用模型进行预测和分析，而无需编写额外的代码或构建独立的应用程序。

```python
# create gradio app
create_app(bm)
```

![create_app](images/PyCaret/image-other-create_app.png)

## 12.参考链接

- 官网文档链接1：https://pycaret.gitbook.io/docs/

- 官网文档链接2（快速入门）：https://pycaret.gitbook.io/docs/get-started/quickstart

- 官网文档链接3（回归类问题API）：https://pycaret.readthedocs.io/en/latest/api/regression.html

- 官网文档链接4（回归类问题详细教程）：https://nbviewer.org/github/pycaret/pycaret/blob/master/tutorials/Tutorial%20-%20Regression.ipynb

- 视频教程：https://www.youtube.com/@pycaret7041/videos

- 知乎-低代码机器学习库--Pycaret：https://zhuanlan.zhihu.com/p/412621015

- 知乎-PyCaret：机器学习模型开发变得简单：https://zhuanlan.zhihu.com/p/516308006

- 知乎-抢调包侠饭碗了！推荐一款全自动的机器学习建模神器PyCaret：https://zhuanlan.zhihu.com/p/139587526