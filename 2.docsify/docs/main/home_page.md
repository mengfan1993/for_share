# 更新日志

## Deeplearning

### Deeplearning-Fastai
20230629-初始化fastai中的内容，这个是一个关于pytorch的库和TensorFlow中Keras是比较相似的，目的是为了简化深度学习中库的使用方式。这个库来源于tsai，即一个专门针对于时间序列的第三方库，因为tsai中引用了fastai中的内容来建立模型，所以或许需要大概了解一下fastai库的使用和它的一些周边产品。

## Data Visualization-overview 
20231023-整理一下数据可视化总述的相关内容

### Visualization-matplotlib
20231027-新增了关于matplotlib的相关笔记内容

### Visualization-seaborn
20230823-备份一下之前翻译的关于seaborn的官网相关笔记，完成的2/13的内容。
20231027-更新了seaborn笔记的文件名方式。
20240402-修改文档的名称，并新增关于“Data structures”的文件，暂未添加内容。

### Visualization-plotly
20230705-初始化plotly的内容，这是一个基于的js的python库，主要用于绘制可交互的图像，可以在streamlit中进行使用。
因为内容主要来源于官网中，所以大部分的内容采用英文的形式。

### Visualization-pyqt
20240801-新增了关于pycharm软件中，如何设置外部工具的文档；

## Qt - pyqt & pyside
20230725-对Qt的相关内容进行初始化和准备，后面也许很用得上这里的内容。整理了第一部分、内容包含:
(1)状态栏、菜单栏和工具栏，(2)布局形式

## SPWLA_interest_group
20230926-新增SPWLA兴趣小组的内容，期望逐渐丰富代码库中的细节。

**YYY**

**JHL**

**CSY**
### 自动机器学习
20240419-新增关于PyCaret的文档并将官网教程的部分内容进行汉化。
20240308-新增自动机器学习在回归任务中英文的文章和部分截图。