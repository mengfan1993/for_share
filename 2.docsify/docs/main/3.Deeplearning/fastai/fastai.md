# fastai

fastai和TensorFlow的Keras类似，是一种简化pytroch神经网络训练流程的一种框架。

## Installing - 安装方式

```
conda install -c nvidia fastai anaconda
conda install -c nvidia fastai anaconda
pip install fastai
```

如果你计划自己开发fastai，或者想走在最前沿，你可以使用可编辑安装（如果你这样做，你也应该使用fastcore的可编辑安装）。首先安装PyTorch，然后

```
git clone https://github.com/fastai/fastai
pip install -e "fastai[dev]"
```

## Learning fastai - 学习方式

最佳的学习方式是阅读这个[书籍](https://www.amazon.com/Deep-Learning-Coders-fastai-PyTorch/dp/1492045527), 然后完成这个 [免费的课程](https://course.fast.ai/).

[Quick Start](https://docs.fast.ai/quick_start.html)中包含了图像分类、图像分割、文本情感、推荐系统和结构化数据的模型。

## About fastai - 关于fastai

fastai是一个深度学习库，可以为工程人员提供可以快速应用深度学习领域先进模型的高级组件，也可以为研究人员提供能够自由搭配并构建新方法的低级组件。它旨在易用性、灵活性或性能方面具有一定的优势。它通过采用精心分层的体系结构，以解耦的抽象表达了许多深度学习和数据处理技术的共同底层模式。这些抽象可以通过利用底层Python语言的动态性和PyTorch库的灵活性来简洁而清晰地表达。fastai包括：

- 一个新的Python类型调度系统，以及一个用于张量的语义类型层次结构
- GPU优化的计算机视觉库，可在纯Python中扩展
- 一个优化器，将现代优化器的常见功能重构为两个基本部分，允许优化算法在4-5行代码中实现
- 一种新颖的双向回调系统，可以访问数据、模型或优化器的任何部分，并在训练期间的任何时候更改它
- 一种新的数据块API
- 其他更多 and much more...

fastai有两个设计目标：平易近人(to be approachable)、快速高效(rapidly productive)，同时还具有高度的可破解性和可配置性。它建立在提供可组合构建块的较低级别API的层次结构之上。这样，想要重写部分高级API或添加特定行为以满足其需求的用户就不必学习如何使用最低级别的API。

![Layered API](images/layered.png)