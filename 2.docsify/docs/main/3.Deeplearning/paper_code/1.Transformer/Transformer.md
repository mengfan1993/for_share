# Transformer素材收集

> 作者：Dreamstar & Soga
>
> 信息来源：
>
> - [《动手学深度学习》：10.7. Transformer](https://zh.d2l.ai/chapter_attention-mechanisms/transformer.html)
> - [The Annotated Transformer](http://nlp.seas.harvard.edu/annotated-transformer/)
> - 

[TOC]

## 模型背景

自注意力，有时也称为内部注意力，是一种将单个序列的不同位置联系起来以计算序列表示的注意力机制。目前，自注意力已成功应用于各种任务之中，包括阅读理解、抽象概括、文本推理和独立于学习任务的句子表征。基于循环注意力机制的端到端记忆网络，已经被证明和基于序列而设计的循环循环神经网络一样，可以在简单的多种自然语言建模任务中表现良好。

Transformer是第一个完全依赖自注意力机制来计算其输入到输出之间也映射关系的的转换模型(transduction model)，其特点是不使用基于序列对齐而设计的循环神经网络，如RNN^[引用]^，或者基于局部序列汇聚的卷积神经网络，如TCN^[引用]^。

随后的部分将对模型的结构和使用场景进行介绍。

## 模型结构

> 参考材料：
>
> 1. http://nlp.seas.harvard.edu/annotated-transformer/#prelims
> 2. https://zh.d2l.ai/chapter_attention-mechanisms/transformer.html

止于 2024/10/31 11:35 ，先完成代码的部分，然后再考虑如何完整这部分的写作内容，后面尽量使用markdown的格式来完成。



## 数据准备 - 以中英文翻译为例

> 参考材料：
>
> [^1]: [教你用PyTorch玩转Transformer英译中翻译模型！](https://zhuanlan.zhihu.com/p/347061440)
> [^2]: [动手学机器学习 - 自然语言处理：预训练](https://zh.d2l.ai/chapter_natural-language-processing-pretraining/index.html)
> [^3]: [NLP 中语言表示 (向量化) 的基本原理和历史演变综述](https://blog.csdn.net/u010280923/article/details/130555437)
> [^4]: [动手学机器学习 - 14.6.2. 字节对编码（Byte Pair Encoding）](https://zh.d2l.ai/chapter_natural-language-processing-pretraining/subword-embedding.html)
> [^5]: soga-词向量转换.doc
> [^6]: [动手学机器学习 - 8.2. 文本预处理](https://zh.d2l.ai/chapter_recurrent-neural-networks/text-preprocessing.html)
> [^7]: Brown, P. F., Cocke, J., Della Pietra, S. A., Della Pietra, V. J.,  Jelinek, F., Lafferty, J., … Roossin, P. S. (1990). A statistical  approach to machine translation. *Computational linguistics*, *16*(2), 79–85. 
> [^8]: Brown, P. F., Cocke, J., Della Pietra, S. A., Della Pietra, V. J.,  Jelinek, F., Mercer, R. L., & Roossin, P. (1988). A statistical  approach to language translation. *Coling Budapest 1988 Volume 1: International Conference on Computational Linguistics*.
> [^9]: [动手学深度学习 - 9.5. 机器翻译与数据集](https://zh.d2l.ai/chapter_recurrent-modern/machine-translation-and-dataset.html)

### 机器翻译简介

> 作者：dreamstar

机器翻译（Machine Translation）是一种使用计算机实现两种自然语言翻译转换的过程。在使用神经网络进行端到端学习兴起之前，统计学方法在机器翻译的领域占据主导地位[^7][^8]。因为*统计机器翻译*（statistical machine translation）涉及了 翻译模型和语言模型等组成部分的统计分析， 因此基于神经网络的方法通常被称为 *神经机器翻译*（neural machine translation）， 用于将两种翻译模型区分开来。[^9]

与单一的语言数据不同，机器翻译的数据集是由源语言和目标语言的文本序列对组成的。因此，神经网络翻译模型实际上反映了两种语言之间的映射关系，**这与测井曲线重构中不同测井曲线之间的关系映射如出一辙，具有很高的参考和模仿价值。**

以”英译中“的数据集中（数据集来自[WMT 2018 Chinese-English track](https://link.zhihu.com/?target=http%3A//statmt.org/wmt18/translation-task.html) (Only NEWS Area)，具体的下载来源于[github仓库](https://github.com/hemingkx/ChineseNMT)，下载后文件夹./data/json中的内容即为训练集(train.json)、验证集(dev.json)和测试集(test.json)。[^1]），数据集中的没遗憾都是由分隔符(如逗号)分隔的文本序列对，序列对由英文文本序列和翻译后的中文文本序列组成。每个文本序列都是一个句子，或者多个句子。此时，英语为源语言(source language, src)，中文为目标语言(target language, tag)。

**词元化：**与普通自然语言处理中的字符级词元不同，机器翻译中常用单词级词元。

**词表：**由于机器翻译数据集由语言对组成， 因此我们可以分别为源语言和目标语言构建两个词表。 使用单词级词元化时，词表大小将明显大于使用字符级词元化时的词表大小。 为了缓解这一问题，这里我们将出现次数少于2次的低频率词元 视为相同的未知（“<unk>”）词元。 除此之外，我们还指定了额外的特定词元， 例如在小批量时用于将序列填充到相同长度的填充词元（“<pad>”）， 以及序列的开始词元（“<bos>”）和结束词元（“<eos>”）。 这些特殊词元在自然语言处理任务中比较常用。[^9]



为对象进行研究，使用基础的Transformer模型，简述基于神经网络机器翻译（Neural Machine Translation，NMT）的实现流程。



在NMT发展初期，RNN、LSTM、GRU等被广泛用作编码器和解码器的网络结构。2017年，**Transformer**^[引用]^横空出世。它不但在翻译效果上大幅超越了基于RNN的神经网络，还通过训练的并行化实现了训练效率的提升。目前，业界机器翻译主流框架广泛采用Transformer。因此，**我们在本文中也具体分享Transformer在翻译任务中的使用，教你从零开始玩转Transformer机器翻译模型**！[^1]

### 数据处理 - 文本预处理

> 作者：dreamstar
>

数据集来自[WMT 2018 Chinese-English track](https://link.zhihu.com/?target=http%3A//statmt.org/wmt18/translation-task.html) (Only NEWS Area)，具体的下载来源于[github仓库](https://github.com/hemingkx/ChineseNMT)，下载后文件夹./data/json中的内容即为训练集(train.json)、验证集(dev.json)和测试集(test.json)。[^1]

原始数据的基本形式是英文到中文的句子对(sentence pair)。

```json
[
  ['Some analysts argue that the negative effects of such an outcome would only last for “months.”', 
   '某些分析家认为军事行动的负面效果只会持续短短“几个月”。'],
  ['The Fed apparently could not stomach the sell-off in global financial markets in January and February, which was driven largely by concerns about further tightening.', 
  '美联储显然无法消化1月和2月的全球金融市场抛售，而这一抛售潮主要是因为对美联储进一步紧缩的担忧导致的。']
]
```

解析文本的常见预处理步骤通常包括：[^6]

1. 将文本作为字符串加载到内存中，即读取数据集或语料库。
2. 将字符串拆分为词元（如单词和字符），即将文本词元化。
3. 建立一个词表，将拆分的词元映射到数字索引，即根据词元生成词表。
4. 将文本转换为数字索引序列，方便模型操作，即使用词表将文本映射为数字索引。

#### 读取数据集/语料库

数据集通常由要处理的文本数据所构成，对于自然语言的处理任务，数据集亦可以称之为语料库。但是对于时间序列的数据集，数据集中可能还含有数值型数据，此时，语料库需要进行提取来获得。例如，在测井数据集中，各种类型的电测曲线，由数值型的浮点数数据表示。对于文本型的字符串数据，主要来源于地层、录井、试油等资料，其语言构成通常为词汇，因此语料库相对简单，词汇量并不是很大。比常规的自然语言处理语料库小了很多，但其加入依旧对数据集的模型表示有很大的帮助。（其难点在于不同数据源的多通道处理）

在自然语言处理任务中，语料库代表了所要处理的文档，小的语料库可以包含30000多个单词，如H.G.Well的书籍[The Time Machine](http://www.gutenberg.org/ebooks/35)，而现实中更大的文档集合可能包含了数十亿个单词。[^6]

#### 词元化 - Tokenization

词元(Token)是文本中最细粒度的“原子”，认为是最小的不可分割单元。对于一个文本序列，认为每一个时间步对应了一个词元，但词元的形成源于对文本表示方法的选择（**在后面的文本表示中细说**）。例如，可以将“Baby needs a new pair of shoes”表示为7个单词的序列，但这样所有单词的集合将构成一个数万或是数十万个庞大的词汇表。又或者，使用更小的词汇表，例如只有256个不同的ASCII字符编码，但这样文本转化后的序列将会很长。

#### 词表 - Vocabulary

然而，即使将文本转换为不同的词元，这些词元依旧是字符串的类型。神经网络模型需要数值型数据的输入，因此需要构建一个字典，通常称之为词表(vocabulary)。这个词表存储了词元(字符串类型)到索引(整数型)的映射。常见的词表构建方式如下：

1. 首先，将训练集中所有文本型数据合并在一起，对它们的唯一词元进行统计，得到的统计结果称之为语料(corpus)。
2. 随后，应用不同的分词策略，根据每个唯一词元的出现频率，为其分配一个数字索引。出现频率很少的词元通常会被移除，用于降低复杂性。并且，对于语料库中不存在或已删除的任何词元，将会映射到一个特定的未知词元“<unk>”。
3. 最后，为了能够在模型中的增加一个特定的功能与操作，还会选择性的增加一个列表，保存一些特殊的词元，例如：填充词元("<pad>")，序列开始词元("<bos>")，序列结束词元("<eos>")。

#### 小结

- 文本数据是数据集中常见的一种字符串数据表示形式；
- 为了能够让模型处理文本数据，需要进行预处理，通常会将文本拆分为词元，随后构建词表形成词元字符串到数字索引的映射，最后将本文数据转换为词元索引供模型后续的操作。
- **备忘：虽然这里对文本预处理的方法进行了简单的整理，但是对于英文版本中对于[词汇频率](https://d2l.ai/chapter_recurrent-neural-networks/text-sequence.html#exploratory-language-statistics)的分析，这里并没有进行完整的体现，因此后续论文的正文中，对所使用的的数据集进行详细的词汇分析。**



### 自然语言处理中的文本序列表示 - 词嵌入/分词

> 作者：soga & dreamstar
>
> 参考资料：
>
> [^2]:[动手学机器学习 - 自然语言处理：预训练](https://zh.d2l.ai/chapter_natural-language-processing-pretraining/index.html)
>
> [^3]:[NLP 中语言表示 (向量化) 的基本原理和历史演变综述](https://blog.csdn.net/u010280923/article/details/130555437)
> [^4]: [动手学机器学习 - 14.6.2. 字节对编码（Byte Pair Encoding）](https://zh.d2l.ai/chapter_natural-language-processing-pretraining/subword-embedding.html)
> [^5]: soga-词向量转换.doc
> [^6]: [动手学机器学习 - 8.2. 文本预处理](https://zh.d2l.ai/chapter_recurrent-neural-networks/text-preprocessing.html)
>
> [^7]: 

**自然语言处理**（Natural Language Processing，NLP）是指研究使用自然语言的计算机和人类之间的交互[^2]。语言表示是该领域中的一项核心任务，旨在将人类语言转化为计算机可理解和处理的形式。语言表示的基本原理和历史演变是理解和应用 NLP 技术的基石。随着人工智能和深度学习的迅猛发展，语言表示也经历了一系列的演进和改进。从早期符号化的离散表示方法到如今基于深度学习的分散式表示 (Distributed Representations) 方法，语言表示都在 NLP 任务中扮演着至关重要的角色[^3]。

目前，自监督学习（self-supervised learning） 已被广泛用于预训练文本表示^[引用]^， 例如通过使用周围文本的其它部分来预测文本的隐藏部分。 通过这种方式，模型可以通过有监督地从海量文本数据中学习，而不需要昂贵的标签标注！[^2]

当将每个单词或子词视为单个词元时， 可以在大型语料库上使用word2vec、GloVe或子词嵌入模型预先训练每个词元的词元。 经过预训练后，每个词元的表示可以是一个向量。 但是，无论上下文是什么，它都保持不变。 例如，“bank”（可以译作银行或者河岸）的向量表示在 “go to the bank to deposit some money”（去银行存点钱） 和“go to the bank to sit down”（去河岸坐下来）中是相同的。 因此，许多较新的预训练模型使相同词元的表示适应于不同的上下文， 其中包括基于Transformer编码器的更深的自监督模型BERT。[^2]





计算机只能处理数字，因此几乎在任何的文本分析和文本模型实现过程中，第一步都是将文本转换成数字，这一步称之为分词(tokenization)。在很多的文本预处理步骤中，分词都是非常重要的一步。[^5]

分词就是将句子、段落、文章这种长文本，分解为以字词为单位的数据结构，方便后续的处理分析工作, 是自然语言处理的重要步骤。通过把文本内容处理为最小基本单元即词元 (token)用于后续的处理。其基本思想是构建一个词表通过词表一一映射进行分词。按照分词粒度可以将分词方法大致分为三大类，分别是词粒度、字符粒度和子词粒度。词粒度以词为单位进行分词，有助于保留词语之间的关联性和上下文信息，但模型无法捕捉同一词的不同形态且只能使用词表中的词来进行处理，限制了模型的语言理解能力；字符粒度以字符为单位进行分词，适用于不同语言可以处理任何字符，具有通用性，但语义信息不明确无法直接表达词的语义，由于文本被拆分为字符导致计算成本和时间增加，处理效率低；在很多情况下，既不希望将文本切分成单独的词（太大），也不想将其切分成单个字符（太小），而是希望得到介于词和字符之间的子词单元。这就引入了子词（subword）粒度的分词方法。

子词粒度方法依据词频大小进行分词，不会对高频的词进行拆分，而仅仅是对一些低频的词进行拆分，能够解决传统方法无法很好的处理未知或罕见的词的缺陷并且可以有效地平衡词汇表大小和文本序列的长度。常用的子词粒度方法有WordPiece和Byte-Pair Encoding (BPE)。









## 素材

> 素材进行随意的复制，不考虑格式和大纲的框架

