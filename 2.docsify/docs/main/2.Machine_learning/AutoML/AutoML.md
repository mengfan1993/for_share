# AutoML - 自动机器学习

> 作者： Dreamstar 
>
> 注：内容尚未完善

当前，机器学习技术已深植根于我们的日常生活中。然而，由于需要高知识储备和多密集测试来提升学习性能，人类必须深度参与机器学习的各个流程。为了使机器学习技术更容易应用，并减少对经验丰富的人类专家的需求，自动化机器学习 (AutoML) 已成为工业和学术感兴趣的热门话题。

# 1. 引言

AutoML 旨在减少数据科学家的需求，并使领域专家能够在不需要统计和 ML 知识的情况下自动构建 ML 应用程序[^1]。

由于没有算法可以在同等重要的所有可能学习问题上取得良好的性能（根据 No Free Lunch 定理 [^3] [^4]），因此机器学习应用的各个方面，例如特征工程、模型选择和算法选择（图 1），需要仔细配置。因此，人类专家非常参与机器学习应用。由于这些专家很少见，机器学习的成功付出了巨大的代价。

如果我们可以使人摆脱这些机器学习应用的过程，我们就可以更快速的部署跨组织的机器学习解决方案，并有效的验证和（基准）测试部署方案，使专家更多的关注应用和业务相关的问题。这将使机器学习更容易被使用，形成具有显著影响的全新能力和定制水平。

作为机器学习的一个子课题，自动机器学习已经在的很多重要领域中有成功的应用。

自动模型选择，如Auto-sklearn[^5]，Auto-WEKA[^6],等。

网络结构搜索，如Google‘s Cloud

自动特征工程，如DSM，ExploreKit，FeatureHub，FeatureLabs

表1 自动机器学习方法在工业和学术界中的应用

![Table_1-Examples of AutoML approaches in industry and academic](images/AutoML/Table_1-Examples of AutoML approaches in industry and academic.png)

AutoML 系统需要一个灵活的管道配置空间，并由搜索该空间内的有效方法驱动。此外，在评估不同的管道时，它们依赖于模型选择和预算分配策略。此外，为了加快搜索过程，在其他数据集上获得的信息可用于启动或指导搜索过程（即元学习）。最后，还可以在事后集成步骤中结合在搜索阶段训练的模型。

## 下文框架

- 自动机器学习面临的问题；
- 自动机器学习的框架和定义；
- 总结已存在的自动机器学习应用；
  - 问题设置
  - 相关技术

# 2. 定义及框架

自动机器学习(AutoML)的理念来源于自动化和机器学习两个学科的交叉融合，其定义如下[^2]：

对于机器学习，是指一个程序通过对经验E在某任务T上获得了评估性能P的改善，则可以说，关于T和P，该程序对E进行了学习[^7]。即如何通过“计算手段”，用“以往经验”改善“算法模型”自身的“性能指标”。

对于自动化，是指没有人或较少人的参与，使用各种控制方法使系统达到目标的过程[^8]。

自动机器学习，其目标为：有限计算预算下，全部或部分取代人类，使机器学习工作流满足性能目标的自动化配置方法。

定义[^37]：一个计算方法试图原子化（全部或部分）学习配置，使用在任务$T$的一些子类经验$E$，通过优化性能度量$P$进行重组。

在经典机器学习中，人们通过操作特征工程、模型选择和算法选择来大量参与配置学习工具，导致机器学习的实践应用需要大量人力和知识基础。在AutoML中，这些都可以全部或部分由计算机程序完成。

自动机器学习的目标：

- 高预测性能：在不同输入数据和不同学习任务下，具有良好的泛化性能；
- 低人工参与：可以自动完成机器学习工具的配置；
- 高计算效率：程序可以在有效计算资源下返回合理的输出；

常规的机器学习流程下，当学习问题被定义后，需要找到一些学习工具来解决它，这些工具包含了对特征、模型和优化的处理。为了获得良好的学习性能，我们将尝试凭借对数据和工具的经验直觉来设配置参数。然后，根据训练及测试性能的的反馈，调整配置并希望性能有所提升，这种测错的过程直到满足预期的表现才会停止。

对于自动机器学习，会有一个控制工具替代人们来找到学习工具最合适的配置，这个控制工具包含两个基本要素：优化器和评估器。

- 评估器：其职责是使用优化器提供的配置来衡量学习工具的性能，并给优化器一个反馈。通常要求高效且准确。
- 优化器：其职责是更新或生成学习工具的配置。通常优化器的搜索空间由机器学习的目标所决定，并期望更新的配置能够在评估器上取得更好的性能。首先，是否会利用评估器的反馈取决于优化器的类型，其次，搜索空间结构希望能够简单且紧凑，从而可以使用更有效的优化方法。

对于自动机器学习方法的分类：

根据要解决的问题来分，可分为特征工程、模型选择、优化算法选择以及网络结构搜索。

根据具体的技术类型来分，可分为网格搜索和随机搜索[^9]、强化学习[^10]、自动微分[^11]、元学习[^12]、迁移学习[^13]

Auto-sklearn 1.0 中的定义方式[^30]：

- (AutoML problem) 对于$i=1,...,n+m$，设$x_i\in \mathbb{R}^d$表示特征向量，$y_i\in Y$表示对应的目标值，给定取自同一数据分布$P(D)$的下的训练集$D_{train}=\{(x_1,y_1),...,(x_n,y_n)\}$和测试集$D_{test}=\{(x_{n+1},y_{n+1}),...,(x_{n+m},y_{n+m})\}$，以及有限计算预算$b$和度量标准$\mathcal{L}(·,·)$，自动机器学习是自动化的提供测试集的预测$\hat{y} _{n+1},...,\hat{y} _{n+m}$，使目标损失函数${1\over m} \sum_{j=1}^{m}\mathcal{L}(\hat{y} _{n+j},{y}_{n+j})$最小化。
- 自动机器学习中的两个问题：(1)没有单一机器学习方法可以在所有的数据集上取得最佳的成绩；(2)一些机器学习方法(例如，非线性SVMs)非常依赖超参数优化；超参数优化已使用贝叶斯优化有效的解决[^31]，并成为了AutoML系统的核心组件，而这两个问题可以考虑为单一地、结构化的联合优化问题，即联合算法选择和超参优化问题。
- (CASH-Combined Algorithm Selection and Hyperparameter)设$\mathcal{A}=\{A^{(1)},...,A^{(R)}\}$为一系列算法的集合，每个算法$A^{(j)}$的超参数具有一个选择域$\Lambda^{(j)}$。随后，训练集$D_{train}={(x_1,y_1),...,(x_n,y_n)}$被分为K折交叉验证集${D_{valid}^{(1)},...,D_{valid}^{(K)}}$和训练集${D_{train}^{(1)},...,D_{train}^{(K)}}$，并满足$i=1,...,K$，$D_{train}^{(i)}=D_{train}\setminus D_{valid}^{(i)}$成立。最后，用$\mathcal{L}(A_\lambda^{(j)},D_{train}^{(i)},D_{valid}^{(i)})$表示用超参数$\lambda$在$D_{train}^{(i)}$上训练，算法$A^{(j)}$在$D_{valid}^{(i)}$上的损失，联合优化问题就是找到一个联合算法和超参设置来最小化损失函数：$$A^*,\lambda_*\in argmin_{A^{(i)}\in \mathcal{A},\lambda\in\Lambda^{(j)}}{1\over K}\sum_{i=1}^K\mathcal{L}(A_\lambda^{(j)},D_{train}^{(i)},D_{valid}^{(i)})$$
- 该 CASH 问题首先由 Thornton 等人解决[^32]，它们在 AUTO-WEKA 系统中使用机器学习框架 WEKA[^33]和基于树的贝叶斯优化方法[^34][^35]。简而言之，贝叶斯优化[^36]拟合概率模型来捕获超参数设置与其测量性能之间的关系；然后，它使用该模型来选择最有希望的超参数设置（跟踪探索空间的新部分与已知良好区域的开发），评估超参数设置，用结果更新模型，并迭代。



Auto-sklearn 2.0 中的定义[^29]：设$P(D)$是数据集的分布，从中采样的单个数据集的分布为$P_d=P_d(x,y)$。在自动机器学习中，我们考虑生成一种训练管道$\mathcal{M}_\lambda:x\rightarrow y$，超参数$\lambda\in\Lambda$，自动从分布$P_d$生成样本，以最小化预期的泛化误差：

$$GE(\mathcal{M}_\lambda)=\mathbb{E}_{(x,y)~P_d}[\mathcal{L}(\mathcal{M}_\lambda(x),y)]$$

由于数据集只能通过一组$n$个独立的观察值$D_d={(x_1,y_1),...,(x_n,y_n)}~P_d$观测到，因此只能凭借经验逼近样本数据的泛化误差：

$$\widehat{GE}(\mathcal{M}_\lambda,D_d)=\frac{1}{n}\sum_{i=1}^{n}\mathcal{L}(\mathcal{M}_\lambda(x),y)$$

在实践中，可以访问两个不相交的有限样本，即训练集和测试集（$D_{train}$和$D_{test}$）。在搜索到最佳的ML管道时，只能访问$D_{train}$，并且，$D_{test}$只在最终的性能估计时使用一次。因此AutoML系统使用它来自动搜索最佳的$\mathcal{M}_\lambda*$：

$$\mathcal{M}_\lambda*\in argmin_{\lambda\in\Lambda}\widehat{GE}(\mathcal{M}_\lambda,D_{train})$$

同时估计$GE$，例如，通过K折交叉验证：

$$\widehat{GE}_{CV}(\mathcal{M}_\lambda,D_{train})={1\over K}\sum_{k=1}^K\widehat{GE}(\mathcal{M}_\lambda^{D_{train} ^{(train,k)}},D_{train}^{^{(val,k)}})$$

式中，$\mathcal{M}_\lambda^{D_{train} ^{(train,k)}}$表示$\mathcal{M}_\lambda$在拆分的第$k$折$D_{train} ^{(train,k)}\subset D_{train}$中进行训练，然后在第$k$折$D_{train}^{^{(val,k)}}=D_{train} \setminus D_{train} ^{(train,k)}$，假设通过$\lambda$，AutoML系统可以选择算法及其超参数设置，定义$\widehat{GE}_{CV}$等效于CASH（Combined Algorithm Selection and Hyperparameter optimization）问题。然而，无论使用何种优化算法，AutoML系统都不太可能找到确切的最佳$\lambda*$。但作为替代，AutoML系统将返回搜索过程中训练的最佳ML管道，用$\mathcal{M}_\lambda$表示，并使用$\lambda*$得到超参数设置。

# 3. 问题设置

AutoML方法不一定涵盖完整的机器学习过程，但可以关注过程中的某些部分，对于一个自动机器学习问题，需要明确以下几个问题：

1. 我们要关注学习过程中的哪个流程？
2. 哪些学习工具可以被设计并使用？
3. 对应的学习配置有哪些？

通过回答上述的三个问题，我们可以相对具体的明确AutoML方法的搜索空间，如下表所示。

<table>
	<tr>
        <th colspan="2">学习过程</th>
        <th>学习工具</th>
        <th>搜索空间</th>
        <th>示例</th>
    </tr>
    <tr>
        <td colspan="2" rowspan="2">特征工程</td>
        <td rowspan="2">分类器/回归器</td>
        <td>特征集合</td>
        <td>ExploreKit</td>
    </tr>
    <tr>
        <td>特征增强方法及其超参数</td>
        <td></td>
    </tr>
    <tr>
        <td colspan="2">模型选择</td>
        <td>分类器/回归器</td>
        <td>分类器及对应的超参数</td>
        <td>Auto-WEKA、</td>
    </tr>
    <tr>
        <td colspan="2">优化算法选择</td>
        <td>分类器/回归器</td>
        <td>优化算法及对应的超参数</td>
        <td>ASlib</td>
    </tr>
    <tr>
        <td rowspan="2">全范围</td>
        <td>常规的</td>
        <td>分类器/回归器/</td>
        <td>特征、模型或算法中搜索空间的并集</td>
        <td>Auto-WEKA、</td>
    </tr>
    <tr>
        <td>神经网络结构搜索(NAS)</td>
        <td>神经网络</td>
        <td>网络结构</td>
        <td>强化学习、SMASH、PathNet、Peephole</td>
    </tr>
</table>


## 3.1 特征工程

特征工程中的第一个的问题，特征集合，很大程度上取决于应用场景和人类专业知识，目前没有统一的原则或方法来从数据集中创建特征。AutoML在这个方向上的进展相对有限。

### 3.1.1 特征增强方法 - Feature Enhancing Methods

很多情况下，数据中的原始特征可能并没有那么充足，如维度过高、样本在特征空间中不易分辨。因此可以考虑对特征进行预处理来提升学习性能，但是通常需要人工辅助进行：

- 降维：当特征具有很大冗余或特征维度过高时，降维可以通过减少所考虑的随机变量数量来获取一组潜在的变量作为新的特征。这个技术过程可分为特征选择和特征映射。
  - 特征选择试图从原始特征中选择特征子集，常用的方法有贪婪搜索(greed search)、套索(lasso)
  - 特征映射试图将原始特征转换为新的低维空间，常用的方法有PCA[^14]、LDA[^15]、自编码器[^16]

- 特征生成：从原始特征中构造新的特征。因为针对于特征本身及之间的探索和交互，也有可能显著的提高学习性能。通常这个交互可以是基于一定经验的(物理属性)或随机的，例如特征之间的乘法，也可以是仅针对特征自身的，例如标准化、归一化。
- 特征编码：根据从数据中学习的映射字典对原始特征进行重新解释，使原始特征空间中无法区分的训练样本变得可以分离。例如稀疏编码[^17]\(及其卷积变体[^18])和局部线性编码[^19]。此外，基于核函数的方法也可以被认为是特征编码，其函数充当映射字典的角色，不过核函数必然与支持向量机(SVM)一起使用，可人工设计基函数，非数据驱动。

### 3.1.2 搜索空间

特征增强工具的搜索空间有两类：一个是工具的超参数，另一个是要生成和选择的特征。

## 3.2 模型选择

一旦训练特征确定，就需要寻找合适的模型来进行训练和预测。模型选择包含两个主要成分，需要合适的模型和设置合适的超参数。对于自动机器学习，主要的任务就是自动的选择模型和对应的超参数，使之获取最佳的学习性能。

### 3.2.1 模型工具 - Classification Tools

现阶段，文献中已经提供了很多模型类型，如决策树、线性模型、核机器以及最近流行的深度神经网络，每个分类器对于特定数据和情况下都有自己的优势和劣势。在经典机器学习第三库中，scikit-learn库中有很多开箱即用的模型，如下表所示。

<table>
    <tr>
        <th></th>
        <th colspan="3">超参数的数量</th>
    </tr>
    <tr>
        <th></th>
        <th>合计</th>
        <th>离散值</th>
        <th>连续值</th>
    </tr>
    <tr>
    	<td>AdaBoost</th>
        <td>4</th>
        <td>1</th>
        <td>3</th>
    </tr>
    <tr>
    	<td>Bernoulli naive Bayes</th>
        <td>2</th>
        <td>1</th>
        <td>1</th>
    </tr>
    <tr>
    	<td>decision tree</th>
        <td>4</th>
        <td>1</th>
        <td>3</th>
    </tr>
    <tr>
    	<td>gradient boosting</th>
        <td>6</th>
        <td>0</th>
        <td>6</th>
    </tr>
    <tr>
    	<td>kNN</th>
        <td>3</th>
        <td>2</th>
        <td>1</th>
    </tr>
    <tr>
    	<td>linear SVM</th>
        <td>4</th>
        <td>2</th>
        <td>2</th>
    </tr>
    <tr>
    	<td>kernel SVM</th>
        <td>7</th>
        <td>2</th>
        <td>5</th>
    </tr>
    <tr>
    	<td>random forest</th>
        <td>5</th>
        <td>2</th>
        <td>3</th>
    </tr>
    <tr>
    	<td>logistic regression</th>
        <td>10</th>
        <td>4</th>
        <td>6</th>
    </tr>
</table>
> 《Efficient and robust automated machine learning》的table1有更详细的内容。

可以看出，不同的模型具有不同的超参数，在传统的机器学习方法中，模型和超参数的选择通常由人工主观经验并以在验证集试错的方式来进行判断。

### 3.2.2 搜索空间

在模型选择的背景下，候选模型及其对应超参数构成了自动机器学习的目标搜索空间，下图显示了一些模型的分层搜索结构。

![Figure_9-An illustration of the search space for model selection](images/AutoML/Figure_9-An illustration of the search space for model selection.png)

## 3.3 优化算法选择

最后一个，同时也是最消耗时间的步骤就是机器学习过程中的模型训练，也是优化算法涉及的领域。对于经典的机器学习算法，优化算法并不是核心，因为从预测性能上看，模型通常表现为凸函数，算法达到的性能理论上基本一致[^20]。因此，计算效率才是优化算法关心的重点。

随着机器学习模型从支持向量机到深度神经网络的发展，模型结构变得越来越复杂，优化算法不仅能够节约计算资源的开销，同时也对学习性能有一定的影响[^21][^22]，如神经网络中优化器。

因此，自动机器学习中，算法选择的目标是自动选择一个优化算法，保证效率和表现达到平衡。

### 3.3.1 优化算法

对于每种学习模型，可选的优化算法有很多，如下表，

<table>
    <tr>
        <th> </th>
        <th colspan="3" align="center">超参数数量</th>
    </tr>
    <tr>
        <th></th>
        <th align="center">合计</th>
        <th align="center">离散值</th>
        <th align="center">连续值</th>
    </tr>
    <tr>
        <td align="center">GD</th>
        <td align="center">0</th>
        <td align="center">0</th>
        <td align="center">0</th>
    </tr>
    <tr>
        <td align="center">L-BFGS</th>
        <td align="center">1</th>
        <td align="center">1</th>
        <td align="center">0</th>
    </tr>
    <tr>
        <td align="center">SGD</th>
        <td align="center">4</th>
        <td align="center">1</th>
        <td align="center">3</th>
    </tr>
</table>

- 梯度下降(GD)：不涉及额外参数，但是收敛速度慢，每次迭代复杂度较高，
- 有限内存的BFGS(L-BFGS:Limited-memory BFGS)[^23]：这个重要公式是由`Broyden`,`Fletcher`,`Goldfard`和`Shanno`于1970年提出的，所以简称为`BFGS`，L-BFGS需要选择存储梯度的长度(离散值)。
- 随机梯度下降(SGD)：需要选择小批量的大小(离散值)、步长(连续值)。

### 3.3.2 搜索空间

传统方法中，优化算法的选择及其超参数都是由人工根据对数据和模型的探索性分析和理解做出的。为了自动化这个过程，优化算法中的超参数应该由算法自动配置决定[^24][^25][^26][^27]，这类似于一种层次结构。

## 3.4 全流程 - Full Scope

内容略



# 4. 优化器技术

一旦确定了搜索空间，接下来就需要找到一个优化器来指导空间中的搜索，以达到自动化的效果。对于自动机器学习中的优化器，需要考虑以下三个问题：

- 优化器可以操作什么样的搜索空间？
- 优化器需要什么样的反馈？
- 在找到一个最佳的配置之前，优化器会需要生成或更新出多少配置？

前两个问题决定了优化器的选择，最后一个问题决定了优化器的效率。通常优化器技术可以分类四类：简单的搜索方法、基于样本的优化、梯度下降该和贪婪搜索。这些技术之间的比较概述下表所示。

<table>
    <tr>
        <th align="center">类型</th>
        <th align="center">方法</th>
        <th align="center">连续型</th>
        <th align="center">离散型</th>
        <th align="center">多步型</th>
        <th align="center">示例</th>
        <th align="center">示例中的反馈</th>
    </tr>
    <tr>
        <td colspan="2" align="center">简单搜索方法</td>
        <td align="center">√</td>
        <td align="center">√</td>
        <td align="center">×</td>
        <td align="center">[^28]Random search for hyper-parameter optimization</td>
        <td align="center">无</td>
    </tr>
    <tr>
        <td rowspan="3" align="center">基于样本的优化技术</td>
        <td align="center">进化算法-evolutionary algorithm</td>
        <td align="center">√</td>
        <td align="center">√</td>
        <td align="center">×</td>
        <td align="center">Genetic CNN</td>
        <td align="center">验证集上的表现</td>
    </tr>
    <tr>
        <td align="center">贝叶斯优化-Bayesian optimization</td>
		<td align="center">√</td>
        <td align="center">√</td>
        <td align="center">×</td>
        <td align="center">AutoWEKA: Combined selection and hyperparameter optimization of classification algorithms</td>
        <td align="center">验证集上的表现</td>
    </tr>
    <tr>
        <td align="center">强化学习-reinforcement learning</td>
        <td align="center">√</td>
        <td align="center">√</td>
        <td align="center">√</td>
        <td align="center">Neural architecture search with reinforcement learning</td>
        <td align="center">验证集上的表现(奖励)<br>一系列的配置(状态)</td>
    </tr>
    <tr>
        <td colspan="2" align="center">梯度下降-gradient descent</td>
		<td align="center">√</td>
        <td align="center">×</td>
        <td align="center">×</td>
        <td align="center">Gradient-based hyperparameter optimization through reversible learning</td>
        <td align="center">performance on validation set and gradients w.r.t hyper-parameters</td>
    </tr>
    <tr>
        <td colspan="2" align="center">贪婪搜索-greedy search</td>
        <td align="center">×</td>
        <td align="center">√</td>
        <td align="center">√</td>
        <td align="center">Progressive neural architecture search</td>
        <td align="center">验证集上的表现</td>
    </tr>
</table>

注：表格中包含的文献引用[^28]、

>  具体的优化器介绍：略

# 5. 评估器技术

一旦生成了候选配置，评估器需要对其性能进行衡量，并提供一定的反馈，这个过程通常会比较耗时，涉及到占据大量时间的模型训练，对于机器学习中的评估器，需要考虑以下三个问题：

- 是否能够提供快速的评估？
- 是否能够提供准确的评估？
- 需要提供哪些反馈？

前两个问题相对对立，因为过快的评估通常会导致结果的退化，即低精度高方差。而最后一个问题取决于优化器的选择，比如梯度下降方法需要梯度信息的反馈。











# 附录

## 相关库的表格整理

| 库名称                                                       | github项目启动时间                                           | star量(20240407数据)                                         | 备注                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [PyCaret](https://pycaret.org)                               | [20191124](https://github.com/pycaret/pycaret/commits/master/?since=2019-11-24&until=2019-11-24) | [8400+](https://github.com/pycaret/pycaret)                  | 未见官方相关文献                                             |
| [AutoGluon](https://auto.gluon.ai/)                          | [20190509](https://github.com/autogluon/autogluon/commit/a6a11d8ccb9d85a7776fadd0659456a9dc939d8e) | [7000+](https://github.com/autogluon/autogluon)              | 亚马逊，AutoGloon:AutoML用于图像、文本、时间序列和表格数据   |
| [H2O.ai](https://h2o.ai/)                                    | [20140304](https://github.com/h2oai/h2o-3/commit/079f9dae869c02c7f5ea47baeacedb6ab4238df5) | [6700+](https://github.com/h2oai/h2o-3)                      | [教程](https://docs.h2o.ai/h2o/latest-stable/h2o-docs/automl.html)，H2O是一个开源、分布式、快速和可扩展的机器学习平台：深度学习、梯度提升（GBM）和XGBoost、随机森林、广义线性建模（GLM with Elastic Net）、K-Means、PCA、广义加性模型（GAM）、RuleFit、支持向量机（SVM）、堆叠集成、自动机器学习（AutoML）等。可使用R语言或Python语言 |
| [Auto-Sklearn](https://automl.github.io/auto-sklearn)        | [20150702](https://github.com/automl/auto-sklearn/commits/master?since=2015-07-02&until=2015-07-02) | [7400+](https://github.com/automl/auto-sklearn/tree/master)  | 基于[scikit-learn](http://scikit-learn.org/)构建             |
| [TPOT](https://epistasislab.github.io/tpot/)                 | [20151104](https://github.com/EpistasisLab/tpot/commits/master/?since=2015-11-04&until=2015-11-04) | [9500+](https://github.com/EpistasisLab/tpot/)               | 是一种基于树的管道优化工具，已发展到[TPOT2 (*alpha*)](https://github.com/EpistasisLab/tpot2)。TPOT2使用遗传算法优化机器学习流程。 |
|                                                              |                                                              |                                                              |                                                              |
| Auto-Keras                                                   |                                                              |                                                              |                                                              |
| [Hyperopt-sklearn](http://hyperopt.github.io/hyperopt-sklearn/) | [20130220](https://github.com/hyperopt/hyperopt-sklearn/commits/master/?since=2013-02-20&until=2013-04-01) | [1500+](https://github.com/hyperopt/hyperopt-sklearn)        | **[hyperopt](https://github.com/hyperopt/hyperopt)**作为一个超参数优化的库，有[7100+](https://github.com/hyperopt/hyperopt)个star<br>Bergstra, J., Yamins, D., Cox, D. D. (2013) Making a Science of Model  Search: Hyperparameter Optimization in Hundreds of Dimensions for Vision Architectures. TProc. of the 30th International Conference on Machine  Learning (ICML 2013), June 2013, pp. I-115 to I-23. |
| [Google AutoML](https://cloud.google.com/automl)             | /                                                            | [1300+](https://github.com/GoogleCloudPlatform/vertex-ai-samples?tab=readme-ov-file)(示例库) | 又名Vertex AI，是Google Cloud中的一个服务，使用监管式学习任务实现，根据不同的机器学习子类别，使用不同的解决方法，未见github代码库，有[示例库](https://github.com/GoogleCloudPlatform/vertex-ai-samples?tab=readme-ov-file)。 |
| [FLAML](https://microsoft.github.io/FLAML/)                  | /                                                            | [3600+](https://github.com/microsoft/FLAML)                  | FLAML基于大型语言模型、机器学习模型等实现自动化工作流程，并优化了性能。其中有一个[AutoGen](https://microsoft.github.io/autogen/)[](https://microsoft.github.io/FLAML/docs/Getting-Started#new-autogen)十分流行(24.1k个star)，它利用多个代理来实现复杂的工作流。FLAML: A Fast and Lightweight AutoML Library |
| [Auto-WEKA](https://www.cs.ubc.ca/labs/algorithms/Projects/autoweka/) | [20130302](https://www.cs.ubc.ca/labs/algorithms/Projects/autoweka/) | [325](https://github.com/automl/autoweka)                    | Auto-WEKA是一个基于WEKA的用于算法选择和超参数优化的Java应用程序。而pyautoweka是Auto WEKA的python包装器。使用了贝叶斯优化技术，完全自动化了算法的超参选择。 |

> 可能主要注意的库：
>
> optuna、hpbandster ConfigSpace、
>
> 资源主要来源于两个网址：
>
> https://landscape.lfai.foundation/card-mode?category=machine-learning&grouping=category
>
> https://www.automl.org/automl-for-x/tabular-data/

## 相关库的文献

Auto-WEKA：

- THORNTON C, HUTTER F, HOOS HolgerH, et al. Auto-WEKA: Combined Selection and Hyperparameter Optimization of Classification Algorithms[J]. arXiv: Learning,arXiv: Learning, 2012.

TPOT：

- R. Olson, N. Bartley, R. Urbanowicz, and J. Moore. Evaluation of a Tree-based Pipeline Optimization Tool for Automating Data Science. In T. Friedrich, editor, Proceedings of the Genetic and Evolutionary Computation Conference (GECCO'16), pages 485–492. ACM, 2016a.

- R. Olson, R. Urbanowicz, P. Andrews, N. Lavender, L. Kidd, and J. Moore. Automating biomedical data science through tree-based pipeline optimization. In G. Squillero and P. Burelli, editors, Proceedings of the 19th European Conference on Applications of Evolutionary Computation (EvoApplications'16), pages 123–137. Springer, 2016b.

H2O AutoML：

- E. LeDell and S. Poirier. H2O AutoML: Scalable automatic machine learning. In K. Eggensperger, M. Feurer, C. Weill, M.Lindauer, F. Hutter, and J. Vanschoren, editors, ICML workshop on Automated Machine Learning (AutoML workshop 2020), 2020.




















# 特征工程

通常认为数据和特征决定了 ML 的上限，模型和算法只能近似这个限制。

特征工程旨在最大化从原始数据中提取特征，以提供算法和模型使用。包含以下三个主题：特征选择、特征提取和特征构建。

自动特征工程在某种程度上是这三个过程的动态组合。

## 特征选择 - Feature Selection

特征选择通过减少不相关或冗余的特征，基于原始特征集构建特征子集。倾向于简化模型，从而避免过度拟合并提高模型性能。整体流程如下：

![Figure_4](images/AutoML/The_iterative_process_of_feature_selection.png)



特征选择的搜索策略涉及三种类型的算法：完全搜索、启发式搜索和随机搜索。

子集评估方法可以分为三个不同的类别，

- 过滤法(filter method)：依据特征的散度和相关性进行过滤，常用的评分标准是方差、相关系数、卡方检验和互信息。、
- 包装法(wrapper method)：使用选定的特征子集对样本集进行分类，然后使用分类准确定作为衡量特征子集质量的标准。
- 嵌入法(embedded method)：将变量选择作为模型学习过程中的一部分进行。如正则化，决策树和深度学习。

## 特征构建 - Feature Construction

特征构建是从基本特征空间或原始数据构建新的特征，以增强模型的鲁棒性和可泛化性的过程，本质上是为了增加原始特征的代表性能力。通常该过程高度依赖人类专业知识，最常用的方法之一是预处理转换，如标准化、规范化或特征离散化。

对于不同的数据类型，数据转换的方式也不相同，例如布尔特征使用conjunctions, disjunctions 和negation，数值特征使用最大、最小、加法、减法、均值等操作，标量特征使用笛卡尔积，M-of-N等操作。







# 其他相关知识

## MLOps

> 参考链接：https://zhuanlan.zhihu.com/p/527768254

MLOps（Machine Learning  Operations）是一组用于数据科学家和运维人员之间协作和沟通的最佳实践。应用这些实践可以提高质量，简化管理流程，并在大规模生产环境中自动部署机器学习和深度学习模型，使模型与业务需求以及规则要求保持一致变得更加容易。

```
# html表格中的单元格合并
<td colspan="3">列合并3列</td>
<td rowspan="4">行合并三行</td>
```



# 参考文献

[^1]: M. Zoller, M. F. Huber, Benchmark and survey of automated machine learning frameworks, arXiv preprint arXiv:1904.12054.
[^2]: Q. Yao, M. Wang, Y. Chen, W. Dai, H. Yi-Qi, L. Yu-Feng, T. Wei-Wei, Y. Qiang, Y. Yang, Taking human out of learning applications: A survey on automated machine learning, arXiv preprint arXiv:1810.13306.

[^3]: D. H. Wolpert, "The lack of a priori distinctions between learning algorithms," Neural computation, vol. 8, no. 7, pp. 1341–1390, 1996.
[^4]: D. H. Wolpert and W. G. Macready, "No free lunch theorems for optimization," IEEE Transactions on Evolutionary Computation, vol. 1, no. 1, Apr. 1997.

[^ 5]: M. Feurer, A. Klein, K. Eggensperger, J. Springenberg, M. Blum, and F. Hutter, "Efficient and robust automated machine learning," in Advances in Neural Information Processing Systems, 2015, pp. 2962–2970.
[^6]: L. Kotthoff, C. Thornton, H. Hoos, F. Hutter, and K. LeytonBrown, "Auto-WEKA 2.0: Automatic model selection and hyperparameter optimization in WEKA," Journal of Machine Learning Research, vol. 18, no. 1, pp. 826–830, 2017.
[^7]: T. Mitchell, Machine Learning. Springer, 1997.
[^8]: PHELAN R M, JOHNSON S H. Automatic Control Systems[J/OL]. Journal of Dynamic Systems, Measurement, and Control, 1977, 99(3): 219-220. http://dx.doi.org/10.1115/1.3427108. DOI:10.1115/1.3427108.

[^9]: BERGSTRA J, BENGIO Y. Random search for hyper-parameter optimization[J]. Journal of Machine Learning Research,Journal of Machine Learning Research, 2012.
[^10]: ZOPH B, LE QuocV. Neural Architecture Search with Reinforcement Learning[J]. International Conference on Learning Representations,International Conference on Learning Representations, 2016.
[^11]: BAYDIN A, PEARLMUTTER BarakA, RADUL A, et al. Automatic differentiation in machine learning: a survey[J]. arXiv: Symbolic Computation,arXiv: Symbolic Computation, 2015.
[^12]: LEMKE C, BUDKA M, GABRYS B. Metalearning: a survey of trends and technologies[J/OL]. Artificial Intelligence Review, 2015: 117-130. http://dx.doi.org/10.1007/s10462-013-9406-y. DOI:10.1007/s10462-013-9406-y.
[^13]: PAN S J, YANG Q. A Survey on Transfer Learning[J/OL]. IEEE Transactions on Knowledge and Data Engineering, 2010: 1345-1359. http://dx.doi.org/10.1109/tkde.2009.191. DOI:10.1109/tkde.2009.191.

[^14]: PEARSON P, KARL K. LIII. On lines and planes of closest fit to systems of points in space[J]. Philosophical Magazine Series 1,Philosophical Magazine Series 1.
[^15]: FISHER R A. THE USE OF MULTIPLE MEASUREMENTS IN TAXONOMIC PROBLEMS[J/OL]. Annals of Eugenics: 179-188. http://dx.doi.org/10.1111/j.1469-1809.1936.tb02137.x. DOI:10.1111/j.1469-1809.1936.tb02137.x.
[^16]: VINCENT P, LAROCHELLE H, BENGIO Y, et al. Extracting and composing robust features with denoising autoencoders[C/OL]//Proceedings of the 25th international conference on Machine learning - ICML ’08, Helsinki, Finland. 2008. http://dx.doi.org/10.1145/1390156.1390294. DOI:10.1145/1390156.1390294.
[^17]: ELAD M, AHARON M. Image Denoising Via Sparse and Redundant Representations Over Learned Dictionaries[J/OL]. IEEE Transactions on Image Processing, 2006: 3736-3745. http://dx.doi.org/10.1109/tip.2006.881969. DOI:10.1109/tip.2006.881969.
[^18]: ZEILER M D, KRISHNAN D, TAYLOR G W, et al. Deconvolutional networks[C/OL]//2010 IEEE Computer Society Conference on Computer Vision and Pattern Recognition, San Francisco, CA, USA. 2010. http://dx.doi.org/10.1109/cvpr.2010.5539957. DOI:10.1109/cvpr.2010.5539957.
[^19]: YU K, ZHANG T, GONG Y. Nonlinear Learning using Local Coordinate Coding[J]. Neural Information Processing Systems,Neural Information Processing Systems, 2009.
[^20]: BOTTOU L, BOUSQUET O. The Tradeoffs of Large-Scale Learning[M/OL]//Optimization for Machine Learning. 2011: 351-368. http://dx.doi.org/10.7551/mitpress/8996.003.0015. DOI:10.7551/mitpress/8996.003.0015.
[^21]: BELLO I, ZOPH B, VASUDEVAN V, et al. Neural optimizer search with reinforcement learning[J]. International Conference on Machine Learning,International Conference on Machine Learning, 2017.
[^22]: NGIAM J, COATES A, LAHIRI A, et al. On optimization methods for deep learning[J]. International Conference on Machine Learning,International Conference on Machine Learning, 2011.

[^23]: LIUDONG C, NOCEDALJORGE N. On the limited memory BFGS method for large scale optimization[J]. Mathematical Programming,Mathematical Programming, 1989.
[^24]: MERZ C J. Dynamical Selection of Learning Algorithms[M/OL]//Learning from Data,Lecture Notes in Statistics. 1996: 281-290. http://dx.doi.org/10.1007/978-1-4612-2404-4_27. DOI:10.1007/978-1-4612-2404-4_27.
[^25]: KADIOGLU S, MALITSKY Y, SABHARWAL A, et al. Algorithm Selection and Scheduling[M/OL]//Principles and Practice of Constraint Programming – CP 2011,Lecture Notes in Computer Science. 2011: 454-469. http://dx.doi.org/10.1007/978-3-642-23786-7_35. DOI:10.1007/978-3-642-23786-7_35.
[^26]: HUTTER F, XU L, HOOS HolgerH, et al. Algorithm Runtime Prediction: Methods & Evaluation[J]. arXiv: Artificial Intelligence,arXiv: Artificial Intelligence, 2012.
[^27]: BISCHL B, KERSCHKE P, KOTTHOFF L, et al. ASlib: A Benchmark Library for Algorithm Selection[J]. arXiv: Artificial Intelligence,arXiv: Artificial Intelligence, 2015.
[^28]: J. Bergstra and Y. Bengio, "Random search for hyper-parameter optimization," Journal of Machine Learning Research, vol. 13, no. Feb, pp. 281–305, 2012.

[^29]: FEURER M, EGGENSPERGER K, FALKNER S, et al. Auto-Sklearn 2.0: Hands-free AutoML via Meta-Learning[J]. arXiv: Learning,arXiv: Learning, 2020.
[^30]: FEURER M, KLEIN A, EGGENSPERGER K, et al. Efficient and robust automated machine learning[J]. Neural Information Processing Systems,Neural Information Processing Systems, 2015.
[^31]: BROCHU E, CORA VladM, FREITAS N. A Tutorial on Bayesian Optimization of Expensive Cost Functions, with Application to Active User Modeling and Hierarchical Reinforcement Learning[J]. arXiv: Learning,arXiv: Learning, 2010.
[^32]: THORNTON C, HUTTER F, HOOS HolgerH, et al. Auto-WEKA: Combined Selection and Hyperparameter Optimization of Classification Algorithms[J]. arXiv: Learning,arXiv: Learning, 2012.
[^33]: HALL M, FRANK E, HOLMES G, et al. The WEKA data mining software[J/OL]. ACM SIGKDD Explorations Newsletter, 2009: 10-18. http://dx.doi.org/10.1145/1656274.1656278. DOI:10.1145/1656274.1656278.
[^34]: HUTTER F, HOOS H H, LEYTON-BROWN K. Sequential Model-Based Optimization for General Algorithm Configuration[M/OL]//Lecture Notes in Computer Science,Learning and Intelligent Optimization. 2011: 507-523. http://dx.doi.org/10.1007/978-3-642-25566-3_40. DOI:10.1007/978-3-642-25566-3_40.
[^35]: BERGSTRA J, BARDENET R, BENGIO Y, et al. Algorithms for Hyper-Parameter Optimization[J]. Le Centre pour la Communication Scientifique Directe - HAL - Inria,Le Centre pour la Communication Scientifique Directe - HAL - Inria, 2011.
[^36]: BROCHU E, CORA VladM, FREITAS N. A Tutorial on Bayesian Optimization of Expensive Cost Functions, with Application to Active User Modeling and Hierarchical Reinforcement Learning[J]. arXiv: Learning,arXiv: Learning, 2010.
[^37]: Zhenqian Shen, Yongqi Zhang, Lanning Wei, Huan Zhao, and Quanming Yao. 2018. Automated Machine Learning: From Principles to Practices. J. ACM 37, 4, Article 111 (August 2018), 34 pages.



