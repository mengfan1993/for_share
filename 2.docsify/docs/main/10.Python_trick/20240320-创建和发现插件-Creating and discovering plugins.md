# 创建和发现插件 - Creating and discovering plugins

> 作者：Dreamstar
>
> 注：内容尚未完善
>
> 文档内容来源：https://packaging.python.org/en/latest/guides/creating-and-discovering-plugins/

通常在创建Python应用程序或库时，您会希望能够通过插件提供自定义或额外功能。因为Python包可以单独分发，所以您的应用程序或库可能希望自动发现所有可用的插件。
进行插件自动发现有三种主要方法：

1. [使用命名约定 - Using naming convention](https://packaging.python.org/en/latest/guides/creating-and-discovering-plugins/#using-naming-convention).
2. [使用命名空间包 - Using namespace packages](https://packaging.python.org/en/latest/guides/creating-and-discovering-plugins/#using-namespace-packages).
3. [使用软件包元数据 - Using package metadata](https://packaging.python.org/en/latest/guides/creating-and-discovering-plugins/#using-package-metadata).

## 使用命名约定 - Using naming convention

如果应用程序的所有插件都遵循相同的命名约定，则可以使用[`pkgutil.iter_modules()`](https://docs.python.org/3/library/pkgutil.html#pkgutil.iter_modules) 来发现所有与命名约定匹配的顶级模块。例如，[Flask](https://pypi.org/project/Flask/) 使用命名约定flask_{plugin_name}。如果你想自动发现所有安装的Flask插件：

```python
import importlib
import pkgutil

discovered_plugins = {
    name: importlib.import_module(name)
    for finder, name, ispkg
    in pkgutil.iter_modules()
    if name.startswith('flask_')
}
```

如果你同时安装了[Flask-SQLAlchemy](https://pypi.org/project/Flask-SQLAlchemy/)和[Flask-Talisman](https://pypi.org/project/flask-talisman)插件，那么`discovered_plugins`将是：

```python
{
    'flask_sqlalchemy': <module: 'flask_sqlalchemy'>,
    'flask_talisman': <module: 'flask_talisman'>,
}
```

使用插件的命名约定还允许您查询Python包索引的[simple repository API](https://packaging.python.org/en/latest/specifications/simple-repository-api/#simple-repository-api)中符合命名约定的所有包。

## 使用命名空间包 - Using namespace packages

命名空间包([Namespace packages](https://packaging.python.org/en/latest/guides/packaging-namespace-packages/))可以用于提供插件放置位置的约定，还可以提供执行发现的方法。例如，如果您将myapp.plugins子包作为名称空间包，那么其他发行版([distributions](https://packaging.python.org/en/latest/glossary/#term-Distribution-Package))可以向该名称空间提供模块和包。安装后，您可以使用[`pkgutil.iter_modules()`](https://docs.python.org/3/library/pkgutil.html#pkgutil.iter_modules)来发现安装在该命名空间下的所有模块和包：

```python
import importlib
import pkgutil

import myapp.plugins

def iter_namespace(ns_pkg):
    # Specifying the second argument (prefix) to iter_modules makes the
    # returned name an absolute name instead of a relative one. This allows
    # import_module to work without having to do additional modification to
    # the name.
    return pkgutil.iter_modules(ns_pkg.__path__, ns_pkg.__name__ + ".")

discovered_plugins = {
    name: importlib.import_module(name)
    for finder, name, ispkg
    in iter_namespace(myapp.plugins)
}
```

指定`myapp.plugins.__path__` 到 [`iter_modules()`](https://docs.python.org/3/library/pkgutil.html#pkgutil.iter_modules) 导致它只在该命名空间下直接查找模块。例如，如果您安装了提供模块`myapp.plugins.a`和`myapp.plugins.b`的发行版，那么在这种情况下，`discovered_plugins`将是：

```python
{
    'a': <module: 'myapp.plugins.a'>,
    'b': <module: 'myapp.plugins.b'>,
}
```

此示例使用一个子包作为名称空间包(`myapp.plugins`)，但也可以为此使用顶级包(如`myapp_plugins`)。如何选择要使用的命名空间是一个偏好问题，但不建议将项目的主要顶层包(在本例中为`myapp`)作为插件的命名空间包，因为一个糟糕的插件可能会导致整个命名空间损坏，进而使您的项目不可移植。为了使“命名空间子包”方法发挥作用，插件包必须省略顶级包目录(本例中为myapp)的`__init__.py`，并在命名空间子包目录(`myapp/plugins`)中包含命名空间包样式`__init__.py`。这也意味着插件需要显式地将包列表传递给`setup()`的`packages`参数，而不是使用`setuptools.find_packages()`。

> Warning:
>
> 命名空间包是一个复杂的功能，有几种不同的创建方法。强烈建议您阅读 [Packaging namespace packages](https://packaging.python.org/en/latest/guides/packaging-namespace-packages/) 文档，并清楚地记录项目插件首选的方法。

## 使用软件包元数据 - Using package metadata

包可以具有入口点规范(Entry points specification](https://packaging.python.org/en/latest/specifications/entry-points/#entry-points))中描述的插件的元数据。通过指定它们，包会宣布它包含特定类型的插件。另一个支持这种插件的包可以使用元数据来发现该插件。

例如，如果您有一个名为`myapp-plugin-a`的包，并且它的`pyproject.toml`中包含以下内容：

```python
[project.entry-points.'myapp.plugins']
a = 'myapp_plugin_a'
```

然后，您可以使用`importlib.metadata.entry_points()`（或者对于Python 3.6-3.9，使用[backport](https://importlib-metadata.readthedocs.io/en/latest/) `importlib_metadata >= 3.6`）来发现并加载所有注册的入口点：

```python
import sys
if sys.version_info < (3, 10):
    from importlib_metadata import entry_points
else:
    from importlib.metadata import entry_points

discovered_plugins = entry_points(group='myapp.plugins')
```

在本例中，`discovered_plugins`将是`importlib.metadata.EntryPoint`的集合。入口点：

```python
(
    EntryPoint(name='a', value='myapp_plugin_a', group='myapp.plugins'),
    ...
)
```

现在，可以通过执行`discovered_plugins['a'].load()`导入您选择的模块。

> Note
>
> `setup.py`中的`entry_point`规范相当灵活，有很多选项。建议您通读关于入口点([entry points](https://setuptools.pypa.io/en/latest/userguide/entry_point.html))的整个章节。
>
> 由于此规范是标准库([standard library](https://docs.python.org/3/library/importlib.metadata.html))的一部分，所以除setuptools之外的大多数打包工具都支持定义入口点。

# 参考素材

[^1]: [Python Packaging User Guide](https://packaging.python.org/en/latest/#)

