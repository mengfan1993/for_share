# PyQt教程

PyQt5 是Digia的一套Qt5应用框架与python的结合，同时支持2.x和3.x。本教程使用的是3.x。Qt库由Riverbank Computing开发，是最强大的GUI库之一 ，官方网站：www.riverbankcomputing.co.uk/news

PyQt5是由一系列Python模块组成。超过620个类，6000函数和方法。能在诸如Unix、Windows和Mac OS等主流操作系统上运行。PyQt5有两种证书，GPL和商业证书。

> 这里是准备将平时收集到的Qt素材进行整理，目的是为了形成一个属于自己的Qt软件开发流程，框架设计如下(随学习深入随时改动)：
>
> 1. PyQt基础窗口素材
> 2. PyQt多线程处理
> 3. PyQt软件打包与分发

PyQt5类分为很多模块，主要模块有：

- QtCore ：核心的非GUI的功能。主要和时间、文件与文件夹、各种数据、流、URLs、mime类文件、进程与线程一起使用
- QtGui ：窗口系统、事件处理、2D图像、基本绘画、字体和文字类
- QtWidgets：一系列创建桌面应用的UI元素
- QtMultimedia：处理多媒体的内容和调用摄像头API的类
- QtBluetooth：查找和连接蓝牙的类
- QtNetwork：网络编程的类，这些工具能让TCP/IP和UDP开发变得更加方便和可靠。
- QtPositioning：定位的类，可以使用卫星、WiFi甚至文本
- Enginio：通过客户端进入和管理Qt Cloud的类
- QtWebSockets：WebSocket协议的类
- QtWebKit：一个基WebKit2的web浏览器
- QtWebKitWidgets：基于QtWidgets的WebKit1的类
- QtXml：处理xml的类，提供了SAX和DOM API的工具
- QtSvg：显示SVG内容的类，Scalable Vector Graphics (SVG)是一种是一种基于可扩展标记语言（XML），用于描述二维矢量图形的图形格式（这句话来自于维基百科）
- QtSql：处理数据库的工具
- QtTest：测试PyQt5应用的工具

## PyQt基础窗口素材

### 状态、菜单和工具栏

菜单是一组位于菜单栏的命令。工具栏是应用的一些常用工具按钮。状态栏显示一些状态信息，通常在应用的底部。

**主窗口**

`QMainWindow`提供了主窗口的功能，使用它能创建一些简单的状态栏、工具栏和菜单栏。具体内容放置于本小节最后。

**状态栏**

```python
#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
ZetCode PyQt5 tutorial 

This program creates a statusbar.

Author: Jan Bodnar
Website: zetcode.com 
Last edited: August 2017
"""

import sys
from PyQt5.QtWidgets import QMainWindow, QApplication


class Example(QMainWindow):

    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):               
        self.statusBar().showMessage('Ready')  # 状态栏

        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle('Statusbar')   
        self.show()


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
```

状态栏是由QMainWindow创建的。

```python
self.statusBar().showMessage('Ready')
```

调用`QtGui.QMainWindow`类的`statusBar()`方法，创建状态栏。第一次调用会创建一个状态栏，而再次调用会返回一个状态栏对象。`showMessage()`方法在状态栏上显示一条信息。程序预览如下：
<img src="images/spaces_-MTAMag8crV4t0EzzyKf_uploads_git-blob-113a904aea9516b01edddf1895b0aeb4233e234d_2-status.webp" alt="spaces_-MTAMag8crV4t0EzzyKf_uploads_git-blob-113a904aea9516b01edddf1895b0aeb4233e234d_2-status" style="zoom:50%;" />

**菜单栏**

菜单栏是非常常用的。是一组命令的集合（Mac OS下状态栏的显示不一样，为得到最相似的外观，我们可以增加一行语句`menubar.setNativeMenuBar(False)`)。

```python
#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
ZetCode PyQt5 tutorial 

This program creates a menubar. The
menubar has one menu with an exit action.

Author: Jan Bodnar
Website: zetcode.com 
Last edited: January 2017
"""

import sys
from PyQt5.QtWidgets import QMainWindow, QAction, qApp, QApplication
from PyQt5.QtGui import QIcon


class Example(QMainWindow):

    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):               

        exitAct = QAction(QIcon('exit.png'), '&Exit', self)  # 动作事件        
        exitAct.setShortcut('Ctrl+Q')  # 设置快捷键
        exitAct.setStatusTip('Exit application')  # 状态栏更新内容
        exitAct.triggered.connect(qApp.quit)  # 连接程序退出的功能

        self.statusBar()  # 状态栏

        menubar = self.menuBar()  # 菜单栏
        fileMenu = menubar.addMenu('&File')  # 一级菜单
        fileMenu.addAction(exitAct)  # 添加动作事件

        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('Simple menu')    
        self.show()


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
```

在上面的示例中，我们创建了只有一个命令的菜单栏，这个命令就是终止应用。同时也创建了一个状态栏。而且还能使用快捷键`Ctrl+Q`退出应用。

```python
exitAct = QAction(QIcon('exit.png'), '&Exit', self)        
exitAct.setShortcut('Ctrl+Q')
exitAct.setStatusTip('Exit application')
```

`QAction`是菜单栏、工具栏或者快捷键的动作的组合。上面三行中，前两行创建了一个图标、一个exit的标签和一个快捷键组合，都执行了一个动作；第三行，创建了一个状态栏，当鼠标悬停在菜单栏的时候，能显示当前状态。

```python
exitAct.triggered.connect(qApp.quit)
```

当执行这个指定的动作时，就触发了一个事件。这个事件跟`QApplication的quit()`行为相关联，所以这个动作就能终止这个应用。

```python
menubar = self.menuBar()
fileMenu = menubar.addMenu('&File')
fileMenu.addAction(exitAct)
```

`menuBar()`创建菜单栏。这里创建了一个菜单栏，并用`addMenu()`在上面添加了一个file菜单，用`addAction()`关联了点击退出应用的事件。

**子菜单**

子菜单是嵌套在菜单里面的二级或者三级等的菜单。

```python
#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
ZetCode PyQt5 tutorial

This program creates a submenu.
子菜单是嵌套在菜单里面的二级或者三级等的菜单。

Author: Jan Bodnar
Website: zetcode.com
Last edited: August 2017
"""

import sys
from PyQt5.QtWidgets import QMainWindow, QAction, QMenu, QApplication

class Example(QMainWindow):

    def __init__(self):
        super().__init__()

        self.initUI()


    def initUI(self):

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('File') # 一个在File菜单下面，一个在File的Import下面

        impMenu = QMenu('Import', self) # 使用QMenu创建一个子菜单
        impAct = QAction('Import mail', self)  # 定义动作事件
        impMenu.addAction(impAct) # 使用addAction()添加一个动作。

        newAct = QAction('New', self)

        fileMenu.addAction(newAct)
        fileMenu.addMenu(impMenu)

        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('Submenu')
        self.show()


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
```

这个例子里，有两个子菜单，一个在File菜单下面，一个在File的Import下面。

```python
impMenu = QMenu('Import', self)
```

使用`QMenu`创建一个新菜单。

```python
impAct = QAction('Import mail', self) 
impMenu.addAction(impAct)
```

使用`addAction()`添加一个动作。程序预览：

![子菜单](images/子菜单.webp)

**勾选菜单**

```python
#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
ZetCode PyQt5 tutorial

This program creates a checkable menu.

Author: Jan Bodnar
Website: zetcode.com
Last edited: August 2017
"""

import sys
from PyQt5.QtWidgets import QMainWindow, QAction, QApplication

class Example(QMainWindow):

    def __init__(self):
        super().__init__()

        self.initUI()


    def initUI(self):

        self.statusbar = self.statusBar() # 状态栏
        self.statusbar.showMessage('Ready') # 状态栏默认文字

        menubar = self.menuBar() # 菜单栏
        viewMenu = menubar.addMenu('View') # 添加菜单

        viewStatAct = QAction('View statusbar', self, checkable=True) # 用checkable选项创建一个能选中的菜单。
        viewStatAct.setStatusTip('View statusbar')  # 设置文字
        viewStatAct.setChecked(True)  # 默认设置为选中状态。
        viewStatAct.triggered.connect(self.toggleMenu) # 与 toggleMenu函数功能链接

        viewMenu.addAction(viewStatAct)

        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('Check menu')
        self.show()

    def toggleMenu(self, state):
        # 切换状态栏显示与否
        if state:
            self.statusbar.show()
        else:
            self.statusbar.hide()


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
```

本例创建了一个行为菜单。这个行为／动作能切换状态栏显示或者隐藏。

```python
viewStatAct = QAction('View statusbar', self, checkable=True)
```

用`checkable`选项创建一个能选中的菜单。

```python
viewStatAct.setChecked(True)
```

默认设置为选中状态。

```python
def toggleMenu(self, state):

    if state:
        self.statusbar.show()
    else:
        self.statusbar.hide()
```

依据选中状态切换状态栏的显示与否。 程序预览：

<img src="images/勾选菜单.webp" alt="勾选菜单" style="zoom:80%;" />

**右键菜单**

右键菜单也叫弹出框（！？），是在某些场合下显示的一组命令。例如，Opera浏览器里，网页上的右键菜单里会有刷新，返回或者查看页面源代码。如果在工具栏上右键，会得到一个不同的用来管理工具栏的菜单。

```python
#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
ZetCode PyQt5 tutorial

This program creates a context menu.

Author: Jan Bodnar
Website: zetcode.com
Last edited: August 2017
右键菜单也叫弹出框（！？），是在某些场合下显示的一组命令。
例如，Opera浏览器里，网页上的右键菜单里会有刷新，返回或者查看页面源代码。
如果在工具栏上右键，会得到一个不同的用来管理工具栏的菜单。
"""

import sys
from PyQt5.QtWidgets import QMainWindow, qApp, QMenu, QApplication

class Example(QMainWindow):

    def __init__(self):
        super().__init__()

        self.initUI()


    def initUI(self):

        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('Context menu')
        self.show()


    def contextMenuEvent(self, event):
        # 使用contextMenuEvent()方法实现这个菜单
   		cmenu = QMenu(self)

        newAct = cmenu.addAction("New")
        opnAct = cmenu.addAction("Open")
        quitAct = cmenu.addAction("Quit")
        action = cmenu.exec_(self.mapToGlobal(event.pos()))  

        if action == quitAct:
            qApp.quit()


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
```

使用`contextMenuEvent()`方法实现这个菜单。

```python
action = cmenu.exec_(self.mapToGlobal(event.pos()))
```

使用`exec_()`方法显示菜单。从鼠标右键事件对象中获得当前坐标。`mapToGlobal()`方法把当前组件的相对坐标转换为窗口（window）的绝对坐标。

```python
if action == quitAct:
    qApp.quit()
```

如果右键菜单里触发了事件，也就触发了退出事件，执行关闭菜单行为。程序预览：

<img src="images/右键菜单.webp" alt="右键菜单" style="zoom:80%;" />

**工具栏**

菜单栏包含了所有的命令，工具栏就是常用的命令的集合。 直接以按键的形式体现用户的交互行为。

```python
#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
ZetCode PyQt5 tutorial

This program creates a toolbar.
The toolbar has one action, which
terminates the application, if triggered.

Author: Jan Bodnar
Website: zetcode.com
Last edited: August 2017
菜单栏包含了所有的命令，工具栏就是常用的命令的集合。
"""

import sys
from PyQt5.QtWidgets import QMainWindow, QAction, qApp, QApplication
from PyQt5.QtGui import QIcon

class Example(QMainWindow):

    def __init__(self):
        super().__init__()

        self.initUI()


    def initUI(self):

        # exitAct = QAction(QIcon('exit24.png'), 'Exit', self)
        # 创建了一个工具栏。这个工具栏只有一个退出应用的动作。
        exitAct = QAction(QIcon('2.2.exit.png'), 'Exit', self) # 绑定了一个标签，一个图标
        exitAct.setShortcut('Ctrl+Q')
        exitAct.triggered.connect(qApp.quit) # 行为被触发的时候，会调用QtGui.QMainWindow的quit方法退出应用
        # addToolBar()创建工具栏，并用addAction()将动作对象添加到工具栏
        self.toolbar = self.addToolBar('Exit')
        self.toolbar.addAction(exitAct)

        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('Toolbar')
        self.show()


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
```

上面的例子中，我们创建了一个工具栏。这个工具栏只有一个退出应用的动作。

```python
exitAct = QAction(QIcon('exit24.png'), 'Exit', self)
exitAct.setShortcut('Ctrl+Q')
exitAct.triggered.connect(qApp.quit)
```

和上面的菜单栏差不多，这里使用了一个行为对象，这个对象绑定了一个标签，一个图标和一个快捷键。这些行为被触发的时候，会调用`QtGui.QMainWindow`的quit方法退出应用。

```python
self.toolbar = self.addToolBar('Exit')
self.toolbar.addAction(exitAct)
```

用`addToolBar()`创建工具栏，并用`addAction()`将动作对象添加到工具栏。程序预览如下：

<img src="images/工具栏.webp" alt="工具栏" style="zoom:80%;" />

**主窗口**

主窗口就是上面三种栏目的总称，现在我们把上面的三种栏在一个应用里展示出来。

> 首先要自己弄个小图标，命名为exit24.png

```python
#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
ZetCode PyQt5 tutorial

This program creates a skeleton of
a classic GUI application with a menubar,
toolbar, statusbar, and a central widget.

Author: Jan Bodnar
Website: zetcode.com
Last edited: August 2017

主窗口就是上面三种栏目的总称，现在我们把上面的三种栏在一个应用里展示出来。
"""

import sys
from PyQt5.QtWidgets import QMainWindow, QTextEdit, QAction, QApplication
from PyQt5.QtGui import QIcon


class Example(QMainWindow):

    def __init__(self):
        super().__init__()

        self.initUI()


    def initUI(self):
        # 文字编辑区
        textEdit = QTextEdit()
        self.setCentralWidget(textEdit)
        # 工具栏和菜单栏 - 动作事件
        # exitAct = QAction(QIcon('exit24.png'), 'Exit', self)
        exitAct = QAction(QIcon('2.2.exit.png'), 'Exit', self)
        exitAct.setShortcut('Ctrl+Q')
        exitAct.setStatusTip('Exit application')
        exitAct.triggered.connect(self.close)
        # 状态栏
        self.statusBar()
        # 菜单栏
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(exitAct)
        # 工具栏
        toolbar = self.addToolBar('Exit')
        toolbar.addAction(exitAct)

        self.setGeometry(300, 300, 350, 250)
        self.setWindowTitle('Main window')
        self.show()


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
```

### 布局管理

在一个GUI程序里，布局是一个很重要的方面。布局就是如何管理应用中的元素和窗口。有两种方式可以搞定：绝对定位和PyQt5的layout类

#### 绝对定位

每个程序都是以像素为单位区分元素的位置，衡量元素的大小。所以我们完全可以使用绝对定位搞定每个元素和窗口的位置。但是这也有局限性：

- 元素不会随着我们更改窗口的位置和大小而变化。
- 不能适用于不同的平台和不同分辨率的显示器
- 更改应用字体大小会破坏布局
- 如果我们决定重构这个应用，需要全部计算一下每个元素的位置和大小

下面这个就是绝对定位的应用

```python
#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
ZetCode PyQt5 tutorial 

This example shows three labels on a window
using absolute positioning. 

Author: Jan Bodnar
Website: zetcode.com 
Last edited: August 2017
"""

import sys
from PyQt5.QtWidgets import QWidget, QLabel, QApplication

class Example(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()


    def initUI(self):

        lbl1 = QLabel('Zetcode', self)
        lbl1.move(15, 10)

        lbl2 = QLabel('tutorials', self)
        lbl2.move(35, 40)

        lbl3 = QLabel('for programmers', self)
        lbl3.move(55, 70)        

        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle('Absolute')    
        self.show()


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
```

我们使用move()方法定位了每一个元素，使用x、y坐标。x、y坐标的原点是程序的左上角。

```python
lbl1 = QLabel('Zetcode', self)
lbl1.move(15, 10)
```

这个元素的左上角就在这个程序的左上角开始的(15, 10)的位置。程序展示如下：

<img src="images/绝对定位.webp" alt="绝对定位" style="zoom:80%;" />

#### 盒布局

使用盒布局能让程序具有更强的适应性。这个才是布局一个应用的更合适的方式。`QHBoxLayout`和`QVBoxLayout`是基本的布局类，分别是水平布局和垂直布局。

如果我们需要把两个按钮放在程序的右下角，创建这样的布局，我们只需要一个水平布局加一个垂直布局的盒子就可以了。再用弹性布局增加一点间隙。

```python
#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
ZetCode PyQt5 tutorial

In this example, we position two push
buttons in the bottom-right corner
of the window.

Author: Jan Bodnar
Website: zetcode.com
Last edited: August 2017
"""

import sys
from PyQt5.QtWidgets import (QWidget, QPushButton,
    QHBoxLayout, QVBoxLayout, QApplication)


class Example(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()


    def initUI(self):

        okButton = QPushButton("OK")  # 按钮
        cancelButton = QPushButton("Cancel")  # 按钮

        hbox = QHBoxLayout()  # 水平布局
        hbox.addStretch(1)  # 一块弹性空间
        hbox.addWidget(okButton)
        hbox.addWidget(cancelButton)

        vbox = QVBoxLayout()  # 垂直布局
        vbox.addStretch(1)  # 一块弹性空间
        vbox.addLayout(hbox)

        self.setLayout(vbox)

        self.setGeometry(300, 300, 300, 150)
        self.setWindowTitle('Buttons')
        self.show()


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
```

上面的例子完成了在应用的右下角放了两个按钮的需求。当改变窗口大小的时候，它们能依然保持在相对的位置。我们同时使用了`QHBoxLayout`和`QVBoxLayout`。

```python
okButton = QPushButton("OK")
cancelButton = QPushButton("Cancel")
```

创建两个按钮。

```python
hbox = QHBoxLayout()
hbox.addStretch(1)
hbox.addWidget(okButton)
hbox.addWidget(cancelButton)
```

创建一个水平布局，并增加弹性空间和两个按钮。stretch函数在两个按钮前面增加了一块弹性空间，它会将按钮挤到窗口的右边。

```python
vbox = QVBoxLayout()
vbox.addStretch(1)
vbox.addLayout(hbox)
```

为了布局需要，我们把这个水平布局放到了一个垂直布局盒里面。弹性元素会把水平布局挤到窗口的下边。

```python
self.setLayout(vbox)
```

最后，我们就得到了我们想要的布局。程序预览如下：

![盒布局](images/盒布局.webp)

#### 栅格布局

最常用的还是栅格布局了。这种布局是把窗口分为行和列。创建和使用栅格布局，需要使用QGridLayout模块。

```python
#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
ZetCode PyQt5 tutorial

In this example, we create a skeleton
of a calculator using a QGridLayout.

author: Jan Bodnar
website: zetcode.com
last edited: January 2015
栅格布局是把窗口分为行和列，使用QGridLayout模块。
"""

import sys
from PyQt5.QtWidgets import (QWidget, QGridLayout,
    QPushButton, QApplication)


class Example(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()


    def initUI(self):

        grid = QGridLayout()
        self.setLayout(grid)
        # 按钮的名称
        names = ['Cls', 'Bck', '', 'Close',
                 '7', '8', '9', '/',
                '4', '5', '6', '*',
                 '1', '2', '3', '-',
                '0', '.', '=', '+']
        # 按钮位置列表
        positions = [(i,j) for i in range(5) for j in range(4)]
        for position, name in zip(positions, names):
            if name == '':
                continue
            button = QPushButton(name)
            grid.addWidget(button, *position)

        self.move(300, 150)
        self.setWindowTitle('Calculator')
        self.show()


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
```

这个例子里，我们创建了栅格化的按钮。

```python
grid = QGridLayout()
self.setLayout(grid)
```

创建一个QGridLayout实例，并把它放到程序窗口里。

```python
names = ['Cls', 'Bck', '', 'Close',
        '7', '8', '9', '/',
        '4', '5', '6', '*',
        '1', '2', '3', '-',
        '0', '.', '=', '+']
```

这是我们将要使用的按钮的名称。

```python
positions = [(i,j) for i in range(5) for j in range(4)]
```

创建按钮位置列表。

```python
for position, name in zip(positions, names):

    if name == '':
        continue
    button = QPushButton(name)
    grid.addWidget(button, *position)
```

创建按钮，并使用`addWidget()`方法把按钮放到布局里面。程序预览如下：

<img src="images/栅格布局.webp" alt="栅格布局" style="zoom:67%;" />

#### 示例 - 制作提交反馈信息的布局

组件能跨列和跨行展示，这个例子里，我们就试试这个功能。

```python
#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
ZetCode PyQt5 tutorial

In this example, we create a more
complicated window layout using
the QGridLayout manager.

Author: Jan Bodnar
Website: zetcode.com
Last edited: August 2017
"""

import sys
from PyQt5.QtWidgets import (QWidget, QLabel, QLineEdit,
    QTextEdit, QGridLayout, QApplication)

class Example(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()


    def initUI(self):

        title = QLabel('Title')
        author = QLabel('Author')
        review = QLabel('Review')
        # 两个行编辑和一个文字编辑
        titleEdit = QLineEdit()
        authorEdit = QLineEdit()
        reviewEdit = QTextEdit()

        grid = QGridLayout()
        grid.setSpacing(10)

        grid.addWidget(title, 1, 0)
        grid.addWidget(titleEdit, 1, 1)  # 创建标签之间的空间

        grid.addWidget(author, 2, 0)
        grid.addWidget(authorEdit, 2, 1)  # 创建标签之间的空间

        grid.addWidget(review, 3, 0)
        grid.addWidget(reviewEdit, 3, 1, 5, 1)  # 创建标签之间的空间

        self.setLayout(grid)

        self.setGeometry(300, 300, 350, 300)
        self.setWindowTitle('Review')
        self.show()


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
```

我们创建了一个有三个标签的窗口。两个行编辑和一个文版编辑，这是用`QGridLayout`模块搞定的。

```python
grid = QGridLayout()
grid.setSpacing(10)
```

创建标签之间的空间。

```python
grid.addWidget(reviewEdit, 3, 1, 5, 1)
```

我们可以指定组件的跨行和跨列的大小。这里我们指定这个元素跨5行显示。程序预览如下：

![示例-制作提交反馈信息的布局](images/示例-制作提交反馈信息的布局.png)