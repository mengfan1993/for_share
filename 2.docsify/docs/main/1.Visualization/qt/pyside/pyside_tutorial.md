# Pyside教程

> 作者：Dreamstar
>
> 注：文章目前仍为草稿状态
>
> [B站视频](https://www.bilibili.com/video/BV1c84y1N7iL/?spm_id_from=333.999.0.0&vd_source=3aa88642179030efe4ce362bda4fea11)

## 环境搭建

### pyside6本体安装

```
pip install pyside6
```

### Zeal

一个文档查阅的工具

### QtDesigner

### Vscode插件配置

下载PYQT integration，根据实际情况配置pyrcc、pyuic和qtdesigner的路径。

## 基础框架

### 三种最基础的控件

QPushButton - 按钮

QLabel - 标签

QLineEdit - 输入框

## QtDesigner

## 控件的常用属性

### 全控件一览以及界面说明和界面设置

### 常用快捷键

ctrl+R：快速预览

### 界面和代码的转换

命令行转换：  pyside6-uic login.ui -o login.py

vscode插件转换：在插件中配置uic，然后直接在文件上右键选择"compile form"



QtDesigner中三个控件之间的区别：

- QMainWindow：有自带的布局，有菜单栏、状态栏。示例图如下。
- QWidget：普通的窗体类型，无自带的布局，可放入QMainWindow的主窗口中的内容区
- QDialog：可以将窗口置顶，一般是一个弹窗

![QMainWindow示例图](images/QMainWindow示例图.png)

## 信号与槽

通俗的描述 - 医生(槽-slot)与多个病人(信号-signal)

信号/槽/事件

信号处理

三大控件常用信号

项目：完善登录界面

项目：完善计算器界面

## 常用控件

QComboBox - 下拉框

```
cb = QComboBox()  # 定义下拉框
cb.addItems(['李华', '张三', '小王'])  # 定义内容

cb.currentTextChanged.connect(lambda: print(cb.currentText()))  # 文字改变时，执行函数
cb.currentIndexChanged.connect(lambda: print(cb.currentText()))  # 索引改变时，执行函数
```

QCheckBox - 复选框

```
cb = QCheckBox("是否被选中")  # 定义复选框

cb.stateChanged.connect(self.showState)  # 定义信号
```

项目：进制转换器





临时记录：https://www.bilibili.com/video/BV1c84y1N7iL/?p=14&spm_id_from=pageDriver&vd_source=3aa88642179030efe4ce362bda4fea11