
# 数据可视化 - Data Visualization

数据的可视化对帮助企业分析数据中趋势来说非常重要。以图表或图形的形式表示时的数据更容易被解读和解释。在数据科学和机器学习领域，原始数据被可视化以模拟趋势并得出有关数据的结论。
此外，可视化数据更有利于各方之间的沟通，并使数据的相关因素相互关联。可视化的数据使企业主、决策者和利益相关者更好地了解他们的数据，并帮助他们发展业务。[^1]

## Seaborn

Seaborn[^3]是一个Python可视化库，用于获得各种调色板中风格优美的图形，使图形更具吸引力。

```
pip install seaborn
```

## Plotly

Plotly[^2]是使用Python进行制作具有出版质量的交互式图形库。包括折线图(line plots)、散点图(scatter plots)、面积图(area charts)、条形图(bar charts)、误差条(error  bars)、箱型图(box  plots)、直方图(histograms)、热图(heatmaps)、子图(subplots)、多轴图(multiple-axes)、极坐标图(polar charts)和气泡图(bubble charts)的示例。

## Altair

altair是最常用的基于Vega的统计可视化python库之一。一旦我们定义了x和y的值，可视化过程就由Altair库处理。我们还可以限制应用程序中显示的图形的大小和颜色。首先，我们需要安装Altair库。要安装它，我们将使用以下命令：

```
pip install altair
```


[^1]: 《使用 Python 的 Streamlit 初学者指南》

[^2]: https://plotly.com/python/

[^3]: https://seaborn.pydata.org/tutorial.html