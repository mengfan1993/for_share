<!-- 网页侧边栏 -->
<!-- 注意：链接一定要用 / , 使用 \ 会导致图片显示错误-->
* [代办](main/todolist.md)
* 数据可视化
  - [Visualization-总览](main/1.Visualization/data_visualization.md)
  - [matplotlib-介绍](main/1.Visualization/matplotlib/matplotlib_tutorial-Introductory-zh.md)
  - [seaborn-介绍](main/1.Visualization/seaborn/1.seaborn_tutorial-introduction.md)
  - [seaborn-绘图函数](main/1.Visualization/seaborn/2.seaborn_tutorial-plotting_functions.md)
  - [seaborn-数据结构](main/1.Visualization/seaborn/3.seaborn_tutorial-Data_structures.md)
  - [plotly-基础](main/1.Visualization/plotly/1.plotly_fundamentals-en.md)
  - [plotly-基本图形](main/1.Visualization/plotly/2.plotly_basic_chart-en.md)
* 机器学习
  - [AutoML简介](main/2.Machine_learning/AutoML/AutoML.md)
  - [PyCaret](main/10.SPWLA_interest_group/CSY/PyCaret.md)
* 深度学习
  - [fastai](main/1.Deeplearning/fastai/fastai.md)
* 未分类
  - [Qt-pyqt](main/qt/pyqt/pyqt_tutorial.md)
  - [Qt-pyside](main/qt/pyside/pyside_tutorial.md)
  - [Hexo-theme](main/blog/hexo/theme-Butterfly.md)
